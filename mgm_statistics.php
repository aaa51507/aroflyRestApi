<?php header("X-Frame-Options: DENY");?>
<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale = 1.0, user-scalable = 0" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="app-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="apple-touch-fullscreen" content="yes" />
        <link rel='shortcut icon' href='template/images/favicon.ico' type='x-icon'>
        <title>會員統計 | healing_fruits</title>
        <meta name="description" content="What you see what you get Enjoy to Interactive with living objects">

        <link class="icon" href="../icon/logo.png" rel="apple-touch-icon-precomposed" />
        <link class="icon" href="../icon/logo.png" rel="SHORTCUT ICON" />
        
        <link rel="stylesheet" href="css/all_css.css">
        <link rel="stylesheet" href="css/global.css">
        
        <?php include( "js/all_js.php"); ?>
        <script src="js/jquery.pagination.js"></script>
        <script src="js/arod/management_account.jquery.pagination.js"></script>
        <script>
                $.global_date_statistics_column = "vip"; //table:date_statistics > col:vip
        </script>
        <!--script src="js/search.js"></script-->
        <script src="js/batch.js"></script>

        <!-- include HIGHCHARTS-->
<!--        <script src="js/highstock.js"></script>
        <script src="js/mgm_highchart_jack.js"></script>
        <script src="js/mgm_hichart_click_jack.js"></script>-->
        <!-- for arod edit function (點擊折線圖的點會呼叫的fun)-->

        <!-- datePicker -->
        <script src="js/jquery-ui.js"></script>

        <!-- jquery.dataTables -->
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="js/dataTablesPlugin.js"></script>

        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        
        <script>
                
                $(document).ready(function() {
                });

                
                function init() {
                        

                        var data = {
                                token: getCookie("scs_cookie")
                        };
                        var success_back = function(data) {
                                data = JSON.parse(data);
                                console.log(data);
                                var main_data, sub_data;
                                if (data.success) {
                                        //會員數統計
                        
                                        var tmp_data = data.data
                        
                                        $('#container_highcharts').highcharts({
                                            title: {
                                                text: '',
                                                x: -20 //center
                                            },
                                            subtitle: {
                                                text: '會員成長統計',
                                                x: -20
                                            },
                                            xAxis: {
                                                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                                                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                                            },
                                            yAxis: {
                                                title: {
                                                    text: '註冊會員數'
                                                },
                                                plotLines: [{
                                                    value: 0,
                                                    width: 1,
                                                    color: '#808080'
                                                }]
                                            },
                                            tooltip: {
                                                valueSuffix: '°C'
                                            },
                                            legend: {
                                                layout: 'vertical',
                                                align: 'right',
                                                verticalAlign: 'middle',
                                                borderWidth: 0
                                            },
                                            series: tmp_data
                                        });   
                                } else {
                                        show_remind( data.msg , "error");
                                }
                        };
                        var error_back = function(data) {
                                console.log(data);
                        };
                        $.Ajax("POST", "php/json_mgm_account_statistics.php?func=fn_read_account_highchart_line", data, "", success_back, error_back);


                        fn_read_account_statistics();
                }
                
                //會員居住城市統計、會員性別統計、會員年齡統計、會員職業統計
                function fn_read_account_statistics() {
                        var data = {
                                token: getCookie("scs_cookie")
                        };
                        var success_back = function(data) {
                                data = JSON.parse(data);
                                console.log(data);
                                var tmp_area, tmp_sex, tmp_age, tmp_job;
                                if (data.success) {
                                        //會員居住城市統計
                                        tmp_area = '<tr class="odd child-middle">' +
                                                        '<td>比例</td>' +
                                                        '<td>'+ data.data.area.north +'</td>' +
                                                        '<td>'+ data.data.area.central +'</td>' +
                                                        '<td>'+ data.data.area.south +'</td>' +
                                                        '<td>'+ data.data.area.east +'</td>' +
                                                        '<td>'+ data.data.area.none +'</td>' +
                                                '</tr>';
                                        $("#city").html(tmp_area);
                                        
                                        //會員性別統計
                                        tmp_sex = '<tr class="odd child-middle">' +
                                                        '<td>比例</td>' +
                                                        '<td>'+ data.data.sex.male +'</td>' +
                                                        '<td>'+ data.data.sex.female +'</td>' +
                                                        '<td>'+ data.data.sex.none +'</td>' +
                                                '</tr>';
                                        $("#sex").html(tmp_sex);
                                        
                                        //會員年齡統計
                                        tmp_age = '<tr class="odd child-middle">' +
                                                                '<td>15歲以下</td>' +
                                                                '<td>'+ data.data.age["15"] +'</td>' +
                                                                '<td>16~20歲</td>' +
                                                                '<td>'+ data.data.age["16_20"] +'</td>' +
                                                        '</tr>' +
                                                        '<tr class="even child-middle">' +
                                                                '<td>21~25歲</td>' +
                                                                '<td>'+ data.data.age["21_25"] +'</td>' +
                                                                '<td>26~30歲</td>' +
                                                                '<td>'+ data.data.age["26_30"] +'</td>' +
                                                        '</tr>' +
                                                        '<tr class="odd child-middle">' +
                                                                '<td>31~35歲</td>' +
                                                                '<td>'+ data.data.age["31_35"] +'</td>' +
                                                                '<td>36~40歲</td>' +
                                                                '<td>'+ data.data.age["36_40"] +'</td>' +
                                                        '</tr>' +
                                                        '<tr class="even child-middle">' +
                                                                '<td>41~45歲</td>' +
                                                                '<td>'+ data.data.age["41_45"] +'</td>' +
                                                                '<td>46~50歲</td>' +
                                                                '<td>'+ data.data.age["46_50"] +'</td>' +
                                                        '</tr>' +
                                                        '<tr class="odd child-middle">' +
                                                                '<td>51~55歲</td>' +
                                                                '<td>'+ data.data.age["51_55"] +'</td>' +
                                                                '<td>56~60歲</td>' +
                                                                '<td>'+ data.data.age["56_60"] +'</td>' +
                                                        '</tr>' +
                                                        '<tr class="even child-middle">' +
                                                                '<td>61~65歲</td>' +
                                                                '<td>'+ data.data.age["61_65"] +'</td>' +
                                                                '<td>66歲以上</td>' +
                                                                '<td>'+ data.data.age["66"] +'</td>' +
                                                        '</tr>';
                                        $("#age").html(tmp_age);
                                        
                                        //會員職業統計
                                        tmp_job = '<tr class="odd child-middle">' +
                                                            '<td>不公開</td>' +
                                                            '<td>'+ data.data.job.none +'</td>' +
                                                            '<td>農林漁牧業</td>' +
                                                            '<td>'+ data.data.job.farmer +'</td>' +
                                                    '</tr>' +
                                                    '<tr class="even child-middle">' +
                                                            '<td>製造業</td>' +
                                                            '<td>'+ data.data.job.manufacture +'</td>' +
                                                            '<td>營造業</td>' +
                                                            '<td>'+ data.data.job.build +'</td>' +
                                                    '</tr>' +
                                                    '<tr class="odd child-middle">' +
                                                            '<td>批發及零售業</td>' +
                                                            '<td>'+ data.data.job.sale +'</td>' +
                                                            '<td>運輸及倉儲業</td>' +
                                                            '<td>'+ data.data.job.transportation +'</td>' +
                                                    '</tr>' +
                                                    '<tr class="even child-middle">' +
                                                            '<td>住宿及餐飲業</td>' +
                                                            '<td>'+ data.data.job.hotel +'</td>' +
                                                            '<td>資訊及通訊傳播業</td>' +
                                                            '<td>'+ data.data.job.information +'</td>' +
                                                    '</tr>' +
                                                    '<tr class="odd child-middle">' +
                                                            '<td>金融及保險業</td>' +
                                                            '<td>'+ data.data.job.finance +'</td>' +
                                                            '<td>不動產業</td>' +
                                                            '<td>'+ data.data.job.estate +'</td>' +
                                                    '</tr>' +
                                                    '<tr class="even child-middle">' +
                                                            '<td>科學及技術服務業</td>' +
                                                            '<td>'+ data.data.job.tech +'</td>' +
                                                            '<td>公共行政及國防業</td>' +
                                                            '<td>'+ data.data.job.public_admin +'</td>' +
                                                    '</tr>' +
                                                    '<tr class="odd child-middle">' +
                                                            '<td>教育服務業</td>' +
                                                            '<td>'+ data.data.job.education +'</td>' +
                                                            '<td>醫療保險及社會工作服務業</td>' +
                                                            '<td>'+ data.data.job.medical +'</td>' +
                                                    '</tr>' +
                                                    '<tr class="even child-middle">' +
                                                            '<td>藝術、娛樂及休閒服務業</td>' +
                                                            '<td>'+ data.data.job.art +'</td>' +
                                                            '<td>其他</td>' +
                                                            '<td>'+ data.data.job.others +'</td>' +
                                                    '</tr>';
                                        $("#job").html(tmp_job);
                                        
                                        
                                        
                                                        
                                } else {
                                        show_remind("更改失敗");
                                }
                        };
                        var error_back = function(data) {
                                console.log(data);
                        };
                        $.Ajax("POST", "php/json_mgm_account_statistics.php?func=fn_read_account_statistics", data, "", success_back, error_back);
                }

        </script>
</head>

<body>
        <div id="all">

                <?php include( "html/loading.php"); ?>
                <?php include( "html/header.php"); ?>

                <div class="content">
                        <?php include( "html/sidebar_setting.php"); ?>

                        <div class="main-content container">                        
                                <div class="path">
                                        <a href="#">控制後台</a> > <a href="#">會員統計</a>
                                </div>

                                <div class="list">
                                        <div style="width: 49%; display: inline-block; margin-right: 1%; vertical-align: top;">
                                                <h2>會員成長統計</h2>
                                                <div id="container_highcharts" style="height: 300px; width: 100%; border: 1px solid;">圖表在這裡</div>
                                        </div>
                                        <div style="width:49%;display: inline-block;">

                                                <h2>會員居住城市統計</h2>
                                                <table class="display select dataTable" id="dynamic-table" style="width: 100%;">
                                                        <thead>
                                                                <tr>
                                                                        <th>地區</th>
                                                                        <th>北</th>
                                                                        <th>中</th>
                                                                        <th>南</th>
                                                                        <th>東</th>
                                                                        <th>不公開</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody id="city">
                                                                <tr class="odd child-middle">
                                                                        <td>比例</td>
                                                                        <td>0%</td>
                                                                        <td>0%</td>
                                                                        <td>0%</td>
                                                                        <td>0%</td>
                                                                        <td>0%</td>
                                                                </tr>
                                                        </tbody>
                                                </table>

                                                <h2>會員性別統計</h2>
                                                <table class="display select dataTable" id="dynamic-table" style="width: 100%;">
                                                        <thead>
                                                                <tr>
                                                                        <th>性別</th>
                                                                        <th>男</th>
                                                                        <th>女</th>
                                                                        <th>不公開</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody id="sex">
                                                                <tr class="odd child-middle">
                                                                        <td>比例</td>
                                                                        <td>0%</td>
                                                                        <td>0%</td>
                                                                        <td>0%</td>
                                                                </tr>
                                                        </tbody>
                                                </table>
                                        </div>
                                </div>

                                <div class="list">
                                        <div style="width:49%;display: inline-block;margin-right: 1%;">
                                                <h2>會員年齡統計</h2>
                                                <table class="display select dataTable" id="dynamic-table" style="width: 100%;">
                                                        <thead>
                                                                <tr>
                                                                        <th>年齡分佈</th>
                                                                        <th>比例</th>
                                                                        <th>年齡分佈</th>
                                                                        <th>比例</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody id="age">
                                                                <tr class="odd child-middle">
                                                                        <td>15歲以下</td>
                                                                        <td>0%</td>
                                                                        <td>16~20歲</td>
                                                                        <td>0%</td>
                                                                </tr>
                                                                <tr class="even child-middle">
                                                                        <td>21~25歲</td>
                                                                        <td>0%</td>
                                                                        <td>26~30歲</td>
                                                                        <td>0%</td>
                                                                </tr>
                                                                <tr class="odd child-middle">
                                                                        <td>31~35歲</td>
                                                                        <td>0%</td>
                                                                        <td>36~40歲</td>
                                                                        <td>0%</td>
                                                                </tr>
                                                                <tr class="even child-middle">
                                                                        <td>41~45歲</td>
                                                                        <td>0%</td>
                                                                        <td>46~50歲</td>
                                                                        <td>0%</td>
                                                                </tr>
                                                                <tr class="odd child-middle">
                                                                        <td>51~55歲</td>
                                                                        <td>0%</td>
                                                                        <td>56~60歲</td>
                                                                        <td>0%</td>
                                                                </tr>
                                                                <tr class="even child-middle">
                                                                        <td>61~65歲</td>
                                                                        <td>0%</td>
                                                                        <td>66歲以上</td>
                                                                        <td>0%</td>
                                                                </tr>
                                                        </tbody>
                                                </table>
                                        </div>
                                        <div style="width:49%;display: inline-block;">
                                                <h2>會員職業統計</h2>
                                                <table class="display select dataTable" id="dynamic-table" style="width: 100%;">
                                                        <thead>
                                                                <tr>
                                                                        <th>職業分佈</th>
                                                                        <th>比例</th>
                                                                        <th>職業分佈</th>
                                                                        <th>比例</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody id="job">
                                                                <tr class="odd child-middle">
                                                                        <td>不公開</td>
                                                                        <td id="1">40%</td>
                                                                        <td>農林漁牧業</td>
                                                                        <td id="2">0%</td>
                                                                </tr>
                                                                <tr class="even child-middle">
                                                                        <td>製造業</td>
                                                                        <td id="3">0%</td>
                                                                        <td>營造業</td>
                                                                        <td id="4">0%</td>
                                                                </tr>
                                                                <tr class="odd child-middle">
                                                                        <td>批發及零售業</td>
                                                                        <td id="5">0%</td>
                                                                        <td>運輸及倉儲業</td>
                                                                        <td id="6">0%</td>
                                                                </tr>
                                                                <tr class="even child-middle">
                                                                        <td>住宿及餐飲業</td>
                                                                        <td id="7">0%</td>
                                                                        <td>資訊及通訊傳播業</td>
                                                                        <td id="8">0%</td>
                                                                </tr>
                                                                <tr class="odd child-middle">
                                                                        <td>金融及保險業</td>
                                                                        <td id="9">0%</td>
                                                                        <td>不動產業</td>
                                                                        <td id="10">0%</td>
                                                                </tr>
                                                                <tr class="even child-middle">
                                                                        <td>科學及技術服務業</td>
                                                                        <td id="11">0%</td>
                                                                        <td>公共行政及國防業</td>
                                                                        <td id="12">0%</td>
                                                                </tr>
                                                                <tr class="odd child-middle">
                                                                        <td>教育服務業</td>
                                                                        <td id="13">0%</td>
                                                                        <td>醫療保險及社會工作服務業</td>
                                                                        <td id="14">0%</td>
                                                                </tr>
                                                                <tr class="even child-middle">
                                                                        <td>藝術、娛樂及休閒服務業</td>
                                                                        <td id="15">0%</td>
                                                                        <td>其他</td>
                                                                        <td id="16">0%</td>
                                                                </tr>
                                                        </tbody>
                                                </table>
                                        </div>
                                </div>
                        </div>

                </div>

                <?php include( "html/footer.php"); ?>
            
        </div>
    
        <script>
            
        function unconnected_callback() {
                loading_ajax_hide();
                show_remind( "請先登入" , "error"  );
        };
        function connected_callback( member ) {
                if( member.a_admin !== "true" ){
                        loading_ajax_hide();
                        show_remind( "不是管理者，三秒後轉跳到首頁。" , "error" );
                        setTimeout( function(){ location.href = "../index.php" }, 3000);
                }
                else{
                        init();
                }
        };
        
        </script>
</body>
</html>
