<?php
/**
 *  use PHPExcel for arofly project
 * @category   PHPExcel
 * @version    1.0.0, 2017-09-04
 * @author     abin.chao
 */
require_once 'Classes/PHPExcel.php';

class Excel extends PHPExcel{
    
    private $consoleColumn = array(
        ['B1' , 'FORMAT'],
        ['B2', 'DATETIME'],
        ['B3', 'DEVICEID'],
        ['B4', 'CONSOLEID'],
        ['B5', 'APPV'],/*0526新增(資料表 及此欄位)*/
        ['B6', 'CFV'],/*0526新增(資料表 及此欄位)*/
        ['B7', 'EMAIL'],
        ['B8', 'BIRTH'],
        ['B9', 'WEIGHT'],
        ['B10', 'UNIT'],//欄位格式text 預備未來存json
        ['B11', 'BIKE'],//欄位格式text 預備未來存json
        ['B12', 'TRAINING'],//欄位格式text 預備未來存json
        ['B13', 'TIRE'],//疑似沒建表
        ['B14', 'INDOOR']//欄位格式text 預備未來存json
    );

    /**
     * 建構子
     */
    public function __construct() {
    }

    /**
     * 將excel為console資料表的資料轉為物件
     */
    public function consoleExcel($filePath) {
        $PHPExcel = $this->readFile($filePath);
        $sheet = $PHPExcel->setActiveSheetIndex(0);
        $rowHeight = $sheet->getHighestRow();
        //取得相對應欄位的值
        $console = array();
        for($i=0; $i<count($this->consoleColumn); $i++) {
            $console[$this->consoleColumn[$i][1]] = $PHPExcel->getActiveSheet()->getCell($this->consoleColumn[$i][0])->getFormattedValue();
        }
        return array_merge($console, array(
            "time_s" => $console['DATETIME']
            ,"pam_s" => $this->getOneColumnByRangeToJson($sheet, 'B16:B'.$rowHeight)
            ,"ahz_s" => $this->getOneColumnByRangeToJson($sheet, 'C16:C'.$rowHeight)
            ,"phz_s" => $this->getOneColumnByRangeToJson($sheet, 'D16:D'.$rowHeight)
            ,"paz_s" => $this->getOneColumnByRangeToJson($sheet, 'E16:E'.$rowHeight)
            ,"longitude_s" => $this->getOneColumnByRangeToJson($sheet, 'F16:F'.$rowHeight)
            ,"latitude_s" => $this->getOneColumnByRangeToJson($sheet, 'G16:G'.$rowHeight)
            ,"altitude_s" => $this->getOneColumnByRangeToJson($sheet, 'H16:H'.$rowHeight)
            // ,"GPS_LINK_QUALITY_SCORE" => $this->getOneColumnByRangeToJson($sheet, 'I16:I'.$rowHeight)
            // ,"GPS_GCS_INFO" => $this->getOneColumnByRangeToJson($sheet, 'J16:J'.$rowHeight)
            ,"status" => $this->getOneColumnByRangeToJson($sheet, 'K16:K'.$rowHeight)
            ,"loop_pm" => $this->getOneColumnByRangeToJson($sheet, 'L16:L'.$rowHeight)
            ,"speed_s" => $this->getOneColumnByRangeToJson($sheet, 'M16:M'.$rowHeight)
            ,"cadence_s" => $this->getOneColumnByRangeToJson($sheet, 'N16:N'.$rowHeight)
            ,"power_s" => $this->getOneColumnByRangeToJson($sheet, 'O16:O'.$rowHeight)
            ,"psensor_s" => $this->getOneColumnByRangeToJson($sheet, 'P16:P'.$rowHeight)
            ,"distance_s" => $this->getOneColumnByRangeToJson($sheet, 'Q16:Q'.$rowHeight)
            ,"HRM" => $this->getOneColumnByRangeToJson($sheet, 'R16:R'.$rowHeight)
            ,"calorie" => $this->getOneColumnByRangeToJson($sheet, 'S16:S'.$rowHeight)
        ));
    }

    /**
     * 取得excel單行的資料 
     * example: C行範圍的資料
     * @param object $sheet  PHPExcel_Worksheet
     * @param str $range Range of cells (i.e. "A1:B10"), or just one cell (i.e. "A1")
     * @return string json string of cells
     */
    public function getOneColumnByRangeToJson($sheet, $range) {
        $data = $sheet->rangeToArray($range);
        //將2維陣列轉1維陣列
        $data = array_map('current', $data);
        $data = array_map('strval', $data);
        return json_encode($data);
    }

    /**
     * 讀取擋案
     */
    private function readFile($filePath) {
        try {
            return PHPExcel_IOFactory::load($filePath);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($filePath,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
    }
}
?>