<?php

$echo = send();

echo json_encode($echo);

function send(){
        
        $callback = array();
        
        if( !check_empty( array( "mail_type","rand","http_default_path","title","a_email" ) ) ){
                $callback['msg'] = "輸入資料不完整";
                $callback['success'] = false;
                return $callback;
        }
        
        $mail_type = $_REQUEST["mail_type"];
        $rand = $_REQUEST["rand"];
        $http_default_path = $_REQUEST["http_default_path"];
        $title = $_REQUEST["title"];
        $a_email = $_REQUEST["a_email"];
        
        if( !in_array($mail_type, array("forgot","authenticate")) ){
                $callback['msg'] = "輸入資料錯誤";
                $callback['success'] = false;
                return $callback;
        }
        
        if( $mail_type === "forgot" ){
                $html = '<p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                 </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">這封信是由</span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">發送的。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif">------------------------------<wbr>------------------------------<wbr>----------<br>
                </span><b><span style="font-size:10.5pt;font-family:新細明體,serif">重要！</span></b><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"><br>
                ------------------------------<wbr>------------------------------<wbr>----------</span> </p>
               <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10.5pt;font-family:新細明體,serif">如果您沒有提交密碼重置的請求或不是</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif;color:rgb(55,96,146)">Funbook19</span><span style="font-size:10.5pt;font-family:新細明體,serif">的註冊用戶，<wbr>請立即忽略</span><span style="font-size:10.5pt;font-family:Arial,sans-serif"> </span><span style="font-size:10.5pt;font-family:新細明體,serif">並刪除這封郵件。只有在您確認需要重置密碼的情況下，<wbr>才需要繼續閱讀下面的內容。</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"></span> </p>
               <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif">------------------------------<wbr>------------------------------<wbr>----------<br>
                </span><b><span style="font-size:10.5pt;font-family:新細明體,serif">密碼重置說明</span></b><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"><br>
                ------------------------------<wbr>------------------------------<wbr>----------</span> </p>
                <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">如果您是</span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">的新用戶，或在修改您的註冊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> Email </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">時使用了本地址，<wbr>我們需要對您的地址有效性進行驗證以避免垃圾郵件或地址被濫用。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您的認證網址為：</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                <a target="_blank">http://www.oort.com.tw/scs/forgot_password.php?token='.$rand.'</a>
                <br>
                </span> </p>
                <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">感謝您的訪問，祝您使用愉快！</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">此致</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                 </span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">管理團隊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">.&nbsp;
                 </span><span style="font-family:新細明體,serif;color:rgb(55,96,146)">'.$http_default_path.'</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>
                <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>';
        }
        else if( $mail_type === "authenticate" ){
                $html = '<p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">這封信是由</span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">發送的。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
               <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您收到這封郵件，是由於在</span><span style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">進行了新用戶註冊，或用戶修改</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> Email </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">使用</span><span style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">了這個郵箱地址。如果您並沒有訪問過</span><span style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">，或沒有進行上述操作，請忽略這封郵件。<wbr>您不需要退訂或進行其他進一步的操作。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
               <p class="MsoNormal"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                <span style="background-image:initial;background-repeat:initial">------------------------------<wbr>------------------------------<wbr>----------</span>
                   <br> </span><b><span style="font-size:10pt;font-family:新細明體,serif;color:black;background-image:initial;background-repeat:initial">帳號激活說明</span></b><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                <span style="background-image:initial;background-repeat:initial">------------------------------<wbr>------------------------------<wbr>----------</span>
                   <br>
                   <br> </span><span lang="EN-US" style="font-family:新細明體,serif"></span> </p>
               <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">如果您是</span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">的新用戶，或在修改您的註冊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> Email </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">時使用了本地址，<wbr>我們需要對您的地址有效性進行驗證以避免垃圾郵件或地址被濫用。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
               <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您的認證網址為：</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
               <a target="_blank">http://www.oort.com.tw/scs/authenticate.php?token='.$rand.'</a>
               <br>
               </span> </p>
               <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">感謝您的訪問，祝您使用愉快！</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
               <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">此致</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                </span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">管理團隊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">.&nbsp;
                </span><span style="font-family:新細明體,serif;color:rgb(55,96,146)">'.$http_default_path.'</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
               <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>
               <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>';
        }
        
        header("Access-Control-Allow-Origin:*");
        header('Access-Control-Allow-Credentials:true');
        header('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers:Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With');
        header('Content-Type:text/html; charset=utf-8');
        require 'gmailsystem/gmail_scs.php';
        mb_internal_encoding('UTF-8');
        $callback = mstart( $a_email , $html , $title );
        
        return $callback;
        
}

function check_empty( $value ) {

       $return = true;
       if( gettype($value) === "array" ) {
           foreach ($value as $key => $value2) {
               if( !isset($_REQUEST[$value2]) || empty($_REQUEST[$value2]) ) {
                   return false;
               }
           }
       }
       else if( gettype($value) === "string" ) {
           if( isset($_REQUEST[$value]) && !empty($_REQUEST[$value]) ) {
               return true;
           }
           else {
               return false;
           }
       }
       else {
           return false;
       }
       return $return;
}

?>
