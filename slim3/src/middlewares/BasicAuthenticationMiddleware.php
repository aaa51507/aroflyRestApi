<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class BasicAuthenticationMiddleware {

    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next) {
        $authString = $request->getHeaderLine("Authorization");
        if (empty($authString)) {
            return $next($request, $response);
        }
        //         
        if (strpos($authString, 'Basic') !== false && $this->verifyBasicAuthentication($authString)) {
            $response = $next($request, $response);
            return $response;
        }
        $newResponse = $response->withStatus(401);
        return $newResponse;
    }

    private function verifyBasicAuthentication($authString) {
        return true;
    }
}
