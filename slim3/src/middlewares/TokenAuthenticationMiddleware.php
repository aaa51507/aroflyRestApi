<?php

define("kMiddlewareAuthInfoKey", "auth_info");

class TokenAuthenticationMiddleware extends BasicController {
    private $authController;
    
    public function __construct($ci) {
        $this->authController = new AuthenticationController($ci);
    }

    /**
     * Veryify Token is valid or not
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next) {
        $callback = array();
        $authString = $request->getHeaderLine("Authorization");        
        $token_info = $this->verifyTokenAuthentication($authString);
        if (strpos($authString, 'Token') !== false && $token_info !== false ) {
            if( $token_info[0]["userinfo_Status"] === "inactive" ){
                $callback["success"] = false;
                $callback["msg"] = "your account has been deleted";
                return $this->jsonResponse($response, $callback);
            }
            $requestWithAuthInfo = $request->withAttribute(kMiddlewareAuthInfoKey, $token_info);
            $response = $next($requestWithAuthInfo, $response);
            return $response;
        } 

//        $newResponse = $response->withStatus(401);
//        return $newResponse;
        $callback["success"] = false;
        $callback["msg"] = "TOKEN Error";
        return $this->jsonResponse($response, $callback);
    }

    private function verifyTokenAuthentication($authString) {
        $pos = strpos($authString, "Token");
        if ( $pos === false ) {
            return false;
        }        
        $token = trim(substr($authString, $pos+5));
        return $this->authController->verifyToken($token);
    }
}
