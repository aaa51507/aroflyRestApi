<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        'services' => [
            'path' => __DIR__ . '/../service'
        ],
        'tmp' => [
            'path' => __DIR__ . '/tmp'
        ],
        'csvFile' => [
            'windowsPath' => 'C:/xampp/htdocs/data/app'
            ,'linuxPath' => '/var/www/html/data/app'
        ],
        // Database settings
        'db' => [
            'host' => 'localhost',
            'user' => 'root',
            'pass' => '!@oort',
            'dbname' => 'arofly',
        ],
        /*
        // abin local use Database settings
        'db' => [
            'host' => '210.61.2.209',
            'user' => 'abin',
            'pass' => 'ggyyggy',
            'dbname' => 'arofly',
        ],
        */
    ],
];
