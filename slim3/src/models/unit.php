<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

class Unit extends BasicModel {

    protected $ci;
    protected $db;

    function __construct(ContainerInterface $ci) {
        parent::__construct($ci);
        $this->db = $ci->db;
    }

    /**
     * 取得unit資料, 轉成dataTable格式
     * @param object $filterData ex:array("length"=> "換頁條件", "order"=> "換頁條件","search"=> "換頁條件","start"=> "換頁條件","searchKey"=> "搜尋條件")
     * @return object unit資料表資料
     */
    public function toDatatable($filterData) {
        $r = array();
        
        //search keyWord
        if(isset($filterData['searchKey'])) {
            //長度為5且為數字 = 只搜尋No
            if(is_numeric($filterData['searchKey']) && strlen($filterData['searchKey']) == 5) {
                $search_str = " where unit_ID = '".(int)$filterData['searchKey']."'";
            } else {
                $search_str = " where unit_LicensePlate like '%".$filterData['searchKey']."%'"
                                          ." or unit_Status like '%".$filterData['searchKey']."%' "
                                          ." or unit_LastDispatchedDateTime like '%".$filterData['searchKey']."%' ";
            }
        } else {
            $search_str = "";
        }
        
        $table = 'unit';
        $order_str = " ORDER BY ".((int)$filterData["order"][0]["column"]+1)." ".$filterData["order"][0]["dir"];
        
        $sql = "SELECT unit_ID, unit_LicensePlate, unit_Status, DATE_FORMAT(unit_LastDispatchedDateTime, '%Y-%m-%d %H:%i') as unit_LastDispatchedDateTime, unit_ID FROM $table"." $search_str "."$order_str LIMIT ".$filterData["start"].", ".$filterData["length"];

        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            if ($stmt->execute()) {
                $r["recordsTotal"] = $recordsTotal;
                $r["recordsFiltered"] = $recordsTotal;
            } else {
                $r["data"] = array();
                $r["recordsTotal"] = 0;
                $r["recordsFiltered"] = 0;
            }
        } else {
            $r["data"] = array();
            $r["recordsTotal"] = 0;
            $r["recordsFiltered"] = 0;
        }
        return $r;
    }

    /**
     * 取得unit資料 By Id
     * @param string $id 編號
     * @return object unit資料
     */
    public function getById($id) {
        $sql = " select *, DATE_FORMAT(unit_LastDispatchedDateTime, '%Y-%m-%d %H:%i') as unit_LastDispatchedDateTime from unit where unit_ID = '".$id."' ";
        
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return array("success"=>true, "data" => $data[0]);
        } else {
            return array("success"=>false,"msg"=>"unit_ID不存在");
        }
    }

    /**
     * 取得unit資料 By Id
     * @param object $filterPostData ex:array("欄位名稱"=> "欄位值")
     * @return object bool
     */
    public function create($filterPostData) {
        $filterPostData["unit_LastDispatchedDateTime"]=null;
        
        $sql = "INSERT INTO unit (unit_LicensePlate,unit_Status,unit_LastDispatchedDateTime, unit_CreateByID, unit_CreateDateTime) 
                        VALUES (:unit_LicensePlate,:unit_Status,:unit_LastDispatchedDateTime,:unit_CreateByID,:unit_CreateDateTime)";
        
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute($filterPostData)) {
                $callback["success"]=true;
        } else {
                $callback["success"]=false;
                $callback["msg"]="Insert data fail";
        }
        return $callback;
    }
    
    /**
     * 修改jobsite資料
     * @param object $unitData ex:array("欄位名稱"=> "欄位值")
     * @return object unit資料
     */
    public function updateById($unitData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction();
        try {
            $unitId = $unitData["unit_ID"];
            $prepare = $this->prepareUpdate("unit", $unitData, "`unit_ID`='".$unitId."'");
            $stmt = $dbh->prepare($prepare);
            $stmt->execute();
            $dbh->commit();
            return $this->getById($unitId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }
}

?>
