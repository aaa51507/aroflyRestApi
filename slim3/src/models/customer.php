<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

class Customer extends BasicModel {

    protected $ci;
    protected $db;

    function __construct(ContainerInterface $ci) {
        parent::__construct($ci);
        $this->db = $ci->db;
    }

    /**
     * 取得customer資料 By Id
     * @param string $id 編號
     * @return object 會員資料
     */
    public function getById($id) {
        $sql ="select "
                ." CONCAT( COALESCE(a.address_Address, ''), ' ', COALESCE(a.address_City, ''), ' ', COALESCE(a.address_Province, '')) as address, "
                ." CONCAT( ct.contactinfo_Name, ' ', IF(ct.contactinfo_Extension='', '', CONCAT('(',ct.contactinfo_Extension, ')')), ct.contactinfo_PhoneNumber) as contact, "
                ." cm.*, u.userinfo_UserName as creater, ct.*, "
                ." COALESCE(cm.customerInfo_StartDate, '-') as customerInfo_StartDate, "
                ." ct.contactinfo_ID as customer_Contact_contactinfo_ID, ct.contactinfo_Name as customer_Contact_contactinfo_Name, ct.contactinfo_Title as customer_Contact_contactinfo_Title, ct.contactinfo_PhoneNumber as customer_Contact_contactinfo_PhoneNumber, ct.contactinfo_Extension as customer_Contact_contactinfo_Extension, "
                ." a.address_ID as customer_Address_address_ID,  a.address_Address as customer_Address_address_Address, a.address_City as customer_Address_address_City, a.address_Province as customer_Address_address_Province, a.address_PostalCode as customer_Address_address_PostalCode, a.address_Latitude as customer_Address_address_Latitude, a.address_Longitude as customer_Address_address_Longitude, "
                ." DATE_FORMAT(cm.customerInfo_CreateDateTime, '%Y-%m-%d %H:%i') as customerInfo_CreateDateTime, "
                ." DATE_FORMAT(cm.customerInfo_UpdateDateTime, '%Y-%m-%d %H:%i') as customerInfo_UpdateDateTime "
            ." from customerinfo as cm "
            ." inner join address as a on cm.customerInfo_AddressID = a.address_ID "
            ." inner join contactinfo as ct on cm.customerInfo_ContactID = ct.contactinfo_ID "
            ." inner join userinfo as u on u.userinfo_ID = cm.customerInfo_CreateByID "
            ." where cm.customerInfo_ID = '".$id."'";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            return (array)$stmt->fetch(PDO::FETCH_OBJ);
        } else {
            return false;
        }
    }

    /**
     * 建立customer資料
     * @param object $customerinfoData ex:array("欄位名稱"=> "欄位值")
     * @param object $relateData ex:array("欄位名稱"=> "欄位值")
     * @return object customer資料
     */
    public function create($customerinfoData, $relateData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction(); 
        try { 
            $prepare = $this->prepareInsertSQL("customerinfo", $customerinfoData);
            $stmt = $dbh->prepare($prepare["sql"]);
            $stmt->execute($prepare["val"]);
            $customerId = $dbh->lastInsertId();

            $contactId = $this->createContact("customerinfo", $customerId, $relateData["customer_Contact"]);
            $addressId = $this->createAddress($relateData["customer_Address"]);

            $updateCustomerData = array(
                "customerInfo_ContactID" => $contactId
                ,"customerInfo_AddressID" => $addressId
            );
            $updateSQL = $this->prepareUpdate("customerInfo", $updateCustomerData, "`customerInfo_ID`='".$customerId."'");
            $stmt = $dbh->prepare($updateSQL);
            $stmt->execute();
            $dbh->commit(); 

            return $this->getById($customerId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    /**
     * 建立customer資料
     * @param object $customerinfoData ex:array("欄位名稱"=> "欄位值")
     * @param object $relateData ex:array("欄位名稱"=> "欄位值")
     * @return object customer資料
     */
    public function updateById($customerinfoData, $relateData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction(); 
        try {
            $customerId = $customerinfoData["customerInfo_ID"];
            $prepare = $this->prepareUpdate("customerInfo", $customerinfoData, "`customerInfo_ID`='".$customerId."'");
            $stmt = $dbh->prepare($prepare);
            $stmt->execute();
            $this->updateAddress($relateData["customer_Address"], "`address_ID`='".$relateData["customer_Address"]["address_ID"]."'");
            $this->updateContact($relateData["customer_Contact"], "`contactinfo_ID`='".$relateData["customer_Contact"]["contactinfo_ID"]."'");
            $dbh->commit(); 

            return $this->getById($customerId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    public function updateStatus($customerinfoData, $customerId) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction(); 
        try {
            $updateSQL = $this->prepareUpdate("customerInfo", $customerinfoData, "`customerInfo_ID`='".$customerId."'");
            $stmt = $dbh->prepare($updateSQL);
            $stmt->execute();

            $dbh->commit();

            return $this->getById($customerId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    public function createContact($tableName, $pk, $contactData) {
        $dbh = $this->ci->db;
        $contactData["contactinfo_PrimaryTable"] = $tableName;
        $contactData["contactinfo_PrimaryID"] = $pk;
        $prepare = $this->prepareInsertSQL("contactinfo", $contactData);
        $stmt = $dbh->prepare($prepare["sql"]);
        $stmt->execute($prepare["val"]);
        return $dbh->lastInsertId();
    }

    public function updateContact($contactData, $condition) {
        $dbh = $this->ci->db;
        $updateSQL = $this->prepareUpdate("contactinfo", $contactData, $condition);
        $stmt = $dbh->prepare($updateSQL);
        $stmt->execute();
    }

    public function createAddress($addressData) {
        $dbh = $this->ci->db;
        $prepare = $this->prepareInsertSQL("address", $addressData);
        $stmt = $dbh->prepare($prepare["sql"]);
        $stmt->execute($prepare["val"]);
        return $dbh->lastInsertId();
    }

    public function updateAddress($addressData, $condition) {
        $dbh = $this->ci->db;
        $updateSQL = $this->prepareUpdate("address", $addressData, $condition);
        $stmt = $dbh->prepare($updateSQL);
        $stmt->execute();
    }
}

?>
