<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

class Employee extends BasicModel {

    protected $ci;
    protected $db;

    function __construct(ContainerInterface $ci) {
        parent::__construct($ci);
        $this->db = $ci->db;
    }

    /**
     * 取得employee資料 By UserInfoId
     * @param string $id 編號
     * @return object 員工資料
     */
    public function getByUserInfoId($id) {
        $sql = " select e.* from userinfo as u "
              ." inner join employeeinfo as e on e.employeeinfo_ID = u.employee_ID "
              ." where u.userinfo_ID = '".$id."'";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            if($stmt->rowCount() > 0) {
                return array(
                    "success" => true
                    ,"data" => (array)$stmt->fetch(PDO::FETCH_OBJ)
                );
            } else {
                return array(
                    "success" => false
                    ,"msg" => 'empty data'
                );
            }
        } else {
            return array(
                "success" => false
                ,"msg" => $stmt->errorInfo()
            );
        }
    }
}

?>
