<?php
/**
 * @author Abin
 */
use \Interop\Container\ContainerInterface as ContainerInterface;

class BasicModel {

    protected $ci;

    public function __construct(ContainerInterface $ci) {
        $this->ci = $ci;
    }

    /**
     * 將資料轉為insert的SQL語法
     * @param string $table 資料表名稱
     * @param object $data column欄位名稱與值 ex:array("id"=>1)
     * @return object array(
                          "sql"=> string SQL字串
                          ,"val"=> array 對應欄位名稱的值
                      )
     */
    public function prepareInsertSQL($table, $data) {
        $q = array();
        $column = array();
        $val = array();
        foreach ($data as $key => $value) {
            $column[] = $key;
            if(gettype($value) == "array") {
                $val[] = implode(",", $value);
            } else {
                $val[] = $value;
            }
            $q[] = "?";
        }
        return array(
            "sql" => "INSERT INTO $table (".implode(",",$column).") VALUES(".implode(",",$q).")"
            ,"val" => $val
        );
    }

    /**
     * 將資料轉為Update的SQL語法
     * @param string $table 資料表名稱
     * @param array $data 欄位名稱與值
     * @param string $condition 條件
     * @return string SQL updat語法
     */
    public function prepareUpdate($table, $data, $condition) {
        $set = array();
        foreach ($data as $key => $value) {
            if (gettype($value) == "NULL") {
                $set[] = "`$key`=null";
            } else if (gettype($value) == "integer") {
                $set[] = "`$key`=$value";
            } else if(gettype($value) == "array") {
                $set[] = "`$key`='".implode(",", $value)."'";
            } else if(strpos($value,"(") && strpos($value,")")){
                $set[] = "`$key`=$value";
            } else {
                $set[] = "`$key`='$value'";
            }
        }
        return "UPDATE `$table` SET ".implode(",",$set).($condition != ""?" WHERE $condition;":"").";";  
    }
}
