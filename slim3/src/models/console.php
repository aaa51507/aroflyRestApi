<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

class Console extends BasicModel {

    protected $ci;
    protected $db;

    function __construct(ContainerInterface $ci) {
        parent::__construct($ci);
        $this->db = $ci->db;
    }

    /**
     * 取得console資料 By Id
     * @param string $id console_id
     * @return object console資料
     */
    public function getById($id) {
        $sql ="select * from console where console_id = '".$id."'";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            return (array)$stmt->fetch(PDO::FETCH_OBJ);
        } else {
            return false;
        }
    }

    /**
     * 建立console資料
     * @param object $consoleData ex:array("欄位名稱"=> "欄位值")
     * @return object console資料
     */
    public function create($consoleData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction(); 
        try { 
            $prepare = $this->prepareInsertSQL("console", $consoleData);
            $stmt = $dbh->prepare($prepare["sql"]);
            $stmt->execute($prepare["val"]);
            $consoleId = $dbh->lastInsertId();
            $dbh->commit(); 

            return $this->getById($consoleId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    /**
     * 取得console資料 By range
     * @param string $range "all:全部 or limit x,x"
     * @return object console資料
     */
    public function getList($range) {
        if($range == "all") {
            $sql ="select * from console ";
        } else {
            $range = explode(",", $range);
            $limit = $range[0].", ".$range[1];
            $sql ="select * from console order by console_id asc limit ".$limit;
        }
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        } else {
            return false;
        }
    }

    /**
     * 修改console資料
     * @param object $consoleData ex:array("欄位名稱"=> "欄位值")
     * @param string $consoleId
     * @return object console資料
     */
    public function updateById($consoleData, $consoleId) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction(); 
        try {
            $customerId = $customerinfoData["customerInfo_ID"];
            $prepare = $this->prepareUpdate("console", $consoleData, "`console_id`='".$consoleId."'");
            $stmt = $dbh->prepare($prepare);
            $stmt->execute();
            $dbh->commit(); 

            return $this->getById($consoleId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    //create
    public function createElseConsole($table, $data, $userinfoId) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction(); 
        try { 
            $prepare = $this->prepareInsertSQL($table, $data);
            $stmt = $dbh->prepare($prepare["sql"]);
            $stmt->execute($prepare["val"]);
            $insertId = $dbh->lastInsertId();
            $dbh->commit(); 

            return $this->getElseConsole($table, "`userinfo_id`='".$data["userinfo_id"]."'");
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    //get
    public function getElseConsole($table, $condition) {

        $sql ="select * from $table ".($condition != ""?"where $condition":"");
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            return (array)$stmt->fetch(PDO::FETCH_OBJ);
        } else {
            return false;
        }
    }

    //edit
    public function updateElseConsole($table, $data, $userinfoId) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction();
        try {
            $prepare = $this->prepareUpdate($table, $data, "`userinfo_id`='".$userinfoId."'");
            $stmt = $dbh->prepare($prepare);
            $stmt->execute();
            $dbh->commit(); 

            return $this->getElseConsole($table, "`userinfo_id`='".$userinfoId."'");
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }
}

?>
