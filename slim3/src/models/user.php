<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

class User extends BasicModel {

    protected $ci;
    protected $db;

    function __construct(ContainerInterface $ci) {
        parent::__construct($ci);
        $this->db = $ci->db;
    }

    /**
     * 取得userinfo資料 By UserInfoId
     * @param string $id 編號
     * @return object 員工資料
     */
    public function getById($id) {
        $sql = " select * from userinfo as u "
              ." where u.userinfo_ID = '".$id."'";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            if($stmt->rowCount() > 0) {
                return array(
                    "success" => true
                    ,"data" => (array)$stmt->fetch(PDO::FETCH_OBJ)
                );
            } else {
                return array(
                    "success" => false
                    ,"msg" => 'empty data'
                );
            }
        } else {
            return array(
                "success" => false
                ,"msg" => $stmt->errorInfo()
            );
        }
    }
}

?>
