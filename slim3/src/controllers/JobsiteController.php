<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

require_once ( __DIR__ . "/../models/jobsite.php");

class JobsiteController extends BasicController {

    private $db;
    //jobsite model
    private $jobsiteM;

    //Constructor
    public function __construct(ContainerInterface $ci) {
        parent::__construct("jobsite", $ci);
        $this->jobsiteM = new Jobsite($ci);
    }

    /**
     * 取得所有jobsite資料(DataTable格式)
     */
    public function collection($request, $response, $args) {
        $sql = "select from jobsite";
        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_NUM);
        }

        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得jobsite資料(select2格式)
     */
    public function toSelect2($request, $response, $args) {
        if(isset($args["id"])) {
            $sql = "select jobsite_ID as id, jobsite_Name as text from jobsite where jobsite_CustomerID = '".$args["id"]."'";
        } else {
            $sql = "select jobsite_ID as id, jobsite_Name as text from jobsite ";
        }
        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $r = 0;
        }
        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得所有jobsite資料(DataTable格式)
     */
    public function toDatatable($request, $response, $args) {    
        $params = $request->getQueryParams();
        //search keyWord
        if(isset($params['searchKey'])) {
            $condition = "";
            //長度為5且為數字 = 只搜尋No
            if(is_numeric($params['searchKey']) && strlen($params['searchKey']) == 5) {
                $condition = " where job.jobsite_ID = '".(int)$params['searchKey']."'";
            } else {
                $condition = " where job.jobsite_JoinID like '%".$params['searchKey']."%' "
                               ." or cm.customerInfo_companyName like '%".$params['searchKey']."%' "
                               ." or a.address_Address like '%".$params['searchKey']."%' "
                               ." or a.address_City like '%".$params['searchKey']."%' "
                               ." or a.address_Province like '%".$params['searchKey']."%' "
                               ." or job.jobsite_Scope like '%".$params['searchKey']."%' ";
           }
        } else {
            $condition = "";
        }

        //select資料
        $orderBy = " ORDER BY ".((int)$params["order"][0]["column"]+1)." ".$params["order"][0]["dir"];
        $limit = " LIMIT ".$params["start"].", ".$params["length"];
        $sql = "select SQL_CALC_FOUND_ROWS "
                    ." CONCAT( job.jobsite_ID, ',', job.jobsite_JoinID ) as joinID, "
                    ." job.jobsite_ID as id, "
                    ." cm.customerInfo_companyName, "
                    ." CONCAT( a.address_Address, ' ', COALESCE(a.address_City, ''), ' ', COALESCE(a.address_Province, '')) as address, "
                    ." a.address_City, "
                    ." job.jobsite_Scope, "
                    ." IF(w.wo, w.wo, '0') as wo"
             ." from jobsite as job"
             ." inner join customerinfo as cm on job.jobsite_CustomerID = cm.customerInfo_ID "
             ." inner join address as a on job.jobsite_AddressID = a.address_ID"
             ." left join ( select count(workorder_ID) as wo, w.* "
                         ." from workorder as w group by w.jobsite_ID ) as w on job.jobsite_ID = w.jobsite_ID "
             .$condition
             .$orderBy.$limit;
        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $r["recordsTotal"] = $recordsTotal;
            $r["recordsFiltered"] = $recordsTotal;
        }

        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得customer jobsite資料ById(DataTable格式)
     */
    public function workorderToDatatableByJobsiteId($request, $response, $args) {    
        $params = $request->getQueryParams();

        $condition = " where w.jobsite_ID = '".$params['jobsite_id']."' ";
        //search keyWord
        if(isset($params['searchKey'])) {
            $condition = " and w.workorder_JoinID like '%".$params['searchKey']."%' "
                           ." or c.customerInfo_companyName like '%".$params['searchKey']."%' "
                           ." or j.jobsite_Name like '%".$params['searchKey']."%' "
                           ." or w.workorder_TypeOfWork like '%".$params['searchKey']."%' "
                           ." or u.unit_LicensePlate like '%".$params['searchKey']."%' "
                           ." or w.workorder_EquipmentTools like '%".$params['searchKey']."%' "

                           ." or operator.employeeinfo_FirstName like '%".$params['searchKey']."%' "
                           ." or operator.employeeinfo_LastName like '%".$params['searchKey']."%' "
                           ." or helper.employeeinfo_FirstName like '%".$params['searchKey']."%' "
                           ." or helper.employeeinfo_LastName like '%".$params['searchKey']."%' "
                           ." or w.workorder_Status like '%".$params['searchKey']."%' "
                           ." or w.workorder_TicketID like '%".$params['searchKey']."%' ";
        }

        if(isset($params['startDate']) && isset($params['endDate'])) {
            $condition .= " and w.workorder_EstStartDateTime > '".$params['startDate']. "' and w.workorder_EstStartDateTime < '".$params['endDate']."' ";
        } else if(isset($params['dateController'])) {
            if($params['dateController'] == "yesterday") {
                $condition .= " and w.workorder_EstStartDateTime > (current_date() - INTERVAL 1 DAY ) and w.workorder_EstStartDateTime < current_date()";
            } else if($params['dateController'] == "today") {
                $condition .= " and w.workorder_EstStartDateTime > current_date() and w.workorder_EstStartDateTime < (current_date() + INTERVAL 1 DAY ) ";
            } else if($params['dateController'] == "tomorrow") {
                $condition .= " and w.workorder_EstStartDateTime > (current_date() + INTERVAL 1 DAY ) and w.workorder_EstStartDateTime < (current_date() + INTERVAL 2 DAY )";
            }
        }

        //select資料
        $orderBy = " ORDER BY ".((int)$params["order"][0]["column"]+1)." ".$params["order"][0]["dir"];
        $limit = " LIMIT ".$params["start"].", ".$params["length"];
        $sql = "select SQL_CALC_FOUND_ROWS "
                    ." CONCAT(w.workorder_ID, ',', w.workorder_JoinID) as No "
                    ." , DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') as Date "
                    ." , DATE_FORMAT(w.workorder_EstStartDateTime, '%h:%i %p') as Time "
                    ." , c.customerInfo_companyName as customer "
                    ." , j.jobsite_Name as jobsite "
                    ." , w.workorder_TypeOfWork as TypeOfWork "
                    ." , u.unit_LicensePlate as Unit "
                    ." , REPLACE(w.workorder_EquipmentTools, ',', '<br>') as equipment "
                    ." , CONCAT(COALESCE(operator.employeeinfo_FirstName, '') , ' ', COALESCE(operator.employeeinfo_LastName, '')) as operators "
                    ." , CONCAT(COALESCE(helper.employeeinfo_FirstName, '') , ' ', COALESCE(helper.employeeinfo_LastName, '')) as helpers "
                    ." , w.workorder_Status as Status "
                    ." , w.workorder_TicketID as Ticket "
                ." from workorder as w "
                ." inner join customerinfo as c on c.customerInfo_ID = w.customer_ID "
                ." inner join jobsite as j on j.jobsite_ID = w.jobsite_ID "
                ." left join unit as u on u.unit_ID = w.workorder_UnitFromUnitID "
                ." left join employeeinfo as operator on operator.employeeinfo_ID = w.workorder_OperatorFromEmployeeID "
                ." left join employeeinfo as helper on helper.employeeinfo_ID = w.workorder_HelperFromEmployeeID "
                .$condition
                .$orderBy.$limit;

        $this->ci->logger->info($sql);

        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $r["recordsTotal"] = $recordsTotal;
            $r["recordsFiltered"] = $recordsTotal;
        }

        return $this->jsonResponse($response, $r);
    }

    /**
     * 新增jobsite
     */
    public function create($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $creater = $this->getLoginUser($request);
        //customerinfo
        $jobsiteColumn = ["jobsite_CustomerID", "jobsite_Name", "jobsite_StartDateTime", "jobsite_TypemployeeOfWorks", "jobsite_Scope"
                         ,"jobsite_ContractNo", "jobsite_ChargingMethod", "jobsite_Rate", "jobsite_Frequency", "jobsite_Terms" 
                         ,"jobsite_InternalNote", "jobsite_Remarks"];
        $relateColumn = ["jobsite_Address", "jobsite_Emergency", "jobsite_Supervisor"
                         ,"jobsite_Superintend", "jobsite_Foreman"];
        // jobsite_JoinID  Customer的No加J的No ex:C1J1
        $jobsiteData = $this->getNeedKeyByObject($jobsiteColumn, $data);
        $jobsiteData["jobsite_CreateByID"] = $creater["userinfo_ID"];

        $relateData = $this->getNeedKeyByObject($relateColumn, $data);
        $d = $this->jobsiteM->create($jobsiteData, $relateData);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    /**
     * 修改jobsite by id
     */
    public function updateById($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $updater = $this->getLoginUser($request);
        //customerinfo
        $jobsiteColumn = ["jobsite_ID", "jobsite_CustomerID", "jobsite_Name", "jobsite_StartDateTime", "jobsite_TypemployeeOfWorks", "jobsite_Scope"
                         ,"jobsite_ContractNo", "jobsite_ChargingMethod", "jobsite_Rate", "jobsite_Frequency", "jobsite_Terms" 
                         ,"jobsite_InternalNote", "jobsite_Remarks"];
        $relateColumn = ["jobsite_Address", "jobsite_Emergency", "jobsite_Supervisor"
                         ,"jobsite_Superintend", "jobsite_Foreman"];
        // jobsite_JoinID  Customer的No加J的No ex:C1J1
        $jobsiteData = $this->getNeedKeyByObject($jobsiteColumn, $data);
        $jobsiteData["jobsite_UpdateByID"] = $updater["userinfo_ID"];
        $jobsiteData["jobsite_UpdateDateTime"] = 'now()';

        $relateData = $this->getNeedKeyByObject($relateColumn, $data);

        $d = $this->jobsiteM->updateById($jobsiteData, $relateData);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得jobsite by id
     */
    public function getByID($request, $response, $args) {
        $r = $this->jobsiteM->getById($args["id"]);
        return $this->jsonResponse($response, $r);
    }
}   
?>