<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

require_once ( __DIR__ . "/../models/activity.php");
require_once ( __DIR__ . "/../models/punch.php");
require_once ( __DIR__ . "/../models/user.php");

class ActivityController extends BasicController
{
    private $db;
    private $activityM;
    private $punchM;
    private $userM;
    //Constructor
    public function __construct(ContainerInterface $ci) {
        parent::__construct("workorder", $ci);
        $this->activityM = new Activity($ci);
        $this->punchM = new Punch($ci);
        $this->userM = new User($ci);
    }

    /**
     * 取得編輯頁相關訊息
     */
    public function getByEmployeeIdandDate($request, $response, $args) {
        $params = $request->getQueryParams();
        $r = $this->activityM->getByEmployeeIdandDate($params['employee_id'], $params['date']);
        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得activity相關訊息
     */
    public function getByWorkorderId($request, $response, $args) {
        $params = $request->getQueryParams();
        $date = $params['date'];
        $employee_id = $params['employee_id']; 

        //select資料
        $sql = " select  DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') as Date "
                  ." , CONCAT(COALESCE(e.employeeinfo_FirstName, ''), ' ', COALESCE(e.employeeinfo_LastName, '')) as operators "
                  ." , p.*, e.employeeinfo_ID as employeeinfo_ID "
                  ." , DATE_FORMAT(p.punch_start_time, '%h:%i%p') as punch_start_time "
                  ." , DATE_FORMAT(p.punch_end_time, '%h:%i%p') as punch_end_time "
                  ." , count(w.workorder_ID) as workorders "
                  ." , count(case w.workorder_Status when 'Dispatched' then 1 else null end) as Dispached "
                  ." , count(case w.workorder_Status when 'In Progress' then 1 else null end) as InProgress "
                  ." , count(case w.workorder_Status when 'Complete' then 1 else null end) as Complete "
              ." from employeeinfo as e "
              ." inner join workorder as w on w.workorder_OperatorFromEmployeeID = e.employeeinfo_ID "
                         ." and workorder_Status != 'Pending' and  DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') = '".$date."' "
              ." left join punch as p on p.employeeinfo_ID = e.employeeinfo_ID and DATE_FORMAT(p.punch_start_time, '%Y-%m-%d') = DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') "
              ." where e.employeeinfo_ID = '".$employee_id."' "
              ." group by DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') ";

        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r = (array)$stmt->fetchAll(PDO::FETCH_OBJ)[0];
        }
        $creater = $this->userM->getById($r['punch_CreateByID']);
        if($creater['success']) {
            $r['creater'] = $creater['data']['userinfo_UserName'];
        }
        $updater = $this->userM->getById($r['punch_UpdateByID']);
        if($updater['success']) {
            $r['updater'] = $updater['data']['userinfo_UserName'];
        }

        //select workorder資料
        $sql = " select w.* "
                ." ,DATE_FORMAT(w.workorder_StartTime, '%h:%i%p') as workorder_StartTime "
                ." ,DATE_FORMAT(w.workorder_CompleteTime, '%h:%i%p') as workorder_CompleteTime "
                ." ,DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') as Date "
                ." ,DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d %H:%i') as workorder_EstStartDateTime "
              ." from workorder as w "
              ." where w.workorder_Status != 'Pending' "
              ." and w.workorder_OperatorFromEmployeeID = '".$employee_id."' "
              ." and DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') = '".$date."' ";
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r['workorder_list'] = $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得dataTable資料格式
     */
    public function toDatatable($request, $response, $args) {
        $params = $request->getQueryParams();
        $condition = array();
        //search keyWord
        if(isset($params['searchKey'])) {
            $condition[] = " e.employeeinfo_FirstName like '%".$params['searchKey']."%' "
                           ." or e.employeeinfo_LastName like '%".$params['searchKey']."%' ";
        }
        if(isset($params['employee_ID'])) {
            $condition[] = " e.employeeinfo_ID = '".$params['employee_ID']."' ";
        }

        //關聯條件
        if(isset($params['startDate']) && isset($params['endDate'])) {
            $dataRange = " w.workorder_EstStartDateTime >= '".$params['startDate']. " 00:00:00' and w.workorder_EstStartDateTime <= '".$params['endDate']." 23:59:59' ";
        }

        //select資料
        $orderBy = " ORDER BY ".((int)$params["order"][0]["column"]+1)." ".$params["order"][0]["dir"];
        $limit = " LIMIT ".$params["start"].", ".$params["length"];
        $sql = " select "
                    ." CONCAT(DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d'), ',', e.employeeinfo_ID) as Date  "
                    ." , CONCAT(COALESCE(e.employeeinfo_FirstName, '') , ' ', COALESCE(e.employeeinfo_LastName, '')) as operators "
                    ." , COALESCE(DATE_FORMAT(p.punch_start_time, '%Y-%m-%d %H:%i'), '-') as punch_in "
                    ." , COALESCE(DATE_FORMAT(p.punch_end_time, '%Y-%m-%d %H:%i'), '-') as punch_out "
                    ." , count(w.workorder_ID) as workorders "
                    ." , count(case w.workorder_Status when 'Dispatched' then 1 else null end) as Dispached "
                    ." , count(case w.workorder_Status when 'In Progress' then 1 else null end) as InProgress "
                    ." , count(case w.workorder_Status when 'Complete' then 1 else null end) as Complete "
                ." from employeeinfo as e "
                ." inner join workorder as w on w.workorder_OperatorFromEmployeeID = e.employeeinfo_ID "
                           ." and workorder_Status != 'Pending' and ".$dataRange
                //取與w.workorder_EstStartDateTime同一天的打卡紀錄
                ." left join punch as p on p.employeeinfo_ID = e.employeeinfo_ID and DATE_FORMAT(p.punch_start_time, '%Y-%m-%d') = DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') "
                .(count($condition)>0?" where ".implode(" and ", $condition):"")
                ." group by DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') "
                .$orderBy.$limit;
        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $r["recordsTotal"] = $recordsTotal;
            $r["recordsFiltered"] = $recordsTotal;
        }

        return $this->jsonResponse($response, $r);
    }

    /**
     * 新/修 punch time 資料
     */
    public function punchTime($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $loginInfo = $this->getLoginUser($request);

        $punchColumn = ["punch_ID", "employeeinfo_ID", "punch_start_time", "punch_end_time", "punch_Longitude", "punch_Latitude"];
        $punchData = $this->getNeedKeyByObject($punchColumn, $data);
        $punchData['punch_start_time']?$punchData['punch_start_time']= $data['Date']." ".date("H:i:s", strtotime($punchData['punch_start_time'])):false;
        $punchData['punch_end_time']?$punchData['punch_end_time']= $data['Date']." ".date("H:i:s", strtotime($punchData['punch_end_time'])):false;
        if(isset($punchData['punch_ID']) && $punchData['punch_ID']) {
            $punchData['punch_UpdateByID'] = $loginInfo["userinfo_ID"];
            $punchData['punch_UpdateDateTime'] = 'now()';
            return $this->jsonResponse($response, $this->punchM->update($punchData, $punchData['punch_ID']));
        } else {
            $getPunch = $this->punchM->getPunch($data['employeeinfo_ID'], $data['Date']);
            if($getPunch['success']) {
                $punchData['punch_UpdateByID'] = $loginInfo["userinfo_ID"];
                $punchData['punch_UpdateDateTime'] = 'now()';
                return $this->jsonResponse($response, $this->punchM->update($punchData, $getPunch["data"]['punch_ID']));
            } else {
                $punchData['punch_CreateByID'] = $loginInfo["userinfo_ID"];
                return $this->jsonResponse($response, $this->punchM->create($punchData));
            }
        }
    }
}   
?>