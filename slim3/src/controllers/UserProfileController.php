<?php
use \Interop\Container\ContainerInterface as ContainerInterface;
use Slim\Http\UploadedFile;

require_once ( __DIR__ . "/../models/console.php");

class UserProfileController extends BasicController {

    public function __construct(ContainerInterface $ci) {
        parent::__construct("Console", $ci);
        $this->ci = $ci;
        $this->db = $ci->db;
        $this->consoleM = new Console($ci);
    }

    public function getByTable($request, $response, $args) {
        $data = $request->getQueryParams();
        $table = explode(".", $data['userinfo_oTableColumn']);
        $d = $this->consoleM->getElseConsole($table[0], "`userinfo_id`='".$data['userinfo_id']."'");
        if(count($d) > 1) {
            $result = array(
                "userinfo_ID" => $data['userinfo_id']
                , "userinfo_value" => end($d)
                , "userinfo_oTableColumn" => $data['userinfo_oTableColumn']
            );
        } else {
            $result = array(
                "userinfo_value" => ""
                , "userinfo_oTableColumn" => $data['userinfo_oTableColumn']
            );
        }
        //特別處理 echo 大頭貼連結
        if($table[0] == "userinfo_avator") {
            $result = array(
                "userinfo_ID" => $data['userinfo_id']
                , "userinfo_oTableColumn" => $data['userinfo_oTableColumn']
            );
            if(count($d) > 1) {
                $result['userinfo_value'] = "http://".$_SERVER['HTTP_HOST']."/arofly/storage/account/".$data['userinfo_id']."/".end($d)."?".time();
            } else {
                $result['userinfo_value'] = "http://".$_SERVER['HTTP_HOST']."/arofly/storage/account/".$data['userinfo_id']."/avator.png?".time();
            }
        }
        $r = $d?array("success"=> true, "data"=> $result):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    public function getAll($request, $response, $args) {
        $data = $request->getQueryParams();
        $tables = ["userinfo_appv", "userinfo_avator", "userinfo_bike_mode", "userinfo_birth", "userinfo_cfv", "userinfo_console_id", "userinfo_date_time", "userinfo_device_id", "userinfo_email", "userinfo_format", "userinfo_height", "userinfo_indoor", "userinfo_sex", "userinfo_tire_size", "userinfo_training_mode", "userinfo_unit", "userinfo_weight"];
        $d = array(
            "userinfo_id" => $data['userinfo_id']
        );
        for($i=0;$i<count($tables);$i++) {
            $r = $this->consoleM->getElseConsole($tables[$i], "`userinfo_id`='".$data['userinfo_id']."'");
            if(isset($r[$tables[$i]])) {
                $d[$tables[$i]] = $r[$tables[$i]];
            } else {
                $d[$tables[$i]] = "";
            }
        }
        return $this->jsonResponse($response, $d);
    }

    public function setAll($request, $response, $args) {
        $data = $request->getParsedBody();
        $tables = ["userinfo_appv", "userinfo_avator", "userinfo_bike_mode", "userinfo_birth", "userinfo_cfv", "userinfo_console_id", "userinfo_date_time", "userinfo_device_id", "userinfo_email", "userinfo_format", "userinfo_height", "userinfo_indoor", "userinfo_sex", "userinfo_tire_size", "userinfo_training_mode", "userinfo_unit", "userinfo_weight"];
        $d = array(
            "userinfo_id" => $data['userinfo_id']
        );
        for($i=0;$i<count($tables);$i++) {
            if(isset($data[$tables[$i]])) {
                $elseConsoleData = array(
                                       "userinfo_id" => $data["userinfo_id"]
                                       , $tables[$i] => $data[$tables[$i]]
                                   );
                $get = $this->consoleM->getElseConsole($tables[$i], "`userinfo_id`='".$data['userinfo_id']."'");
                if(count($get) > 1) {
                    $r = $this->consoleM->updateElseConsole($tables[$i], $elseConsoleData, $data["userinfo_id"]);
                } else {
                    $r = $this->consoleM->createElseConsole($tables[$i], $elseConsoleData, $data["userinfo_id"]);
                }
                $d[$tables[$i]] = $r[$tables[$i]];
            }
        }
        return $this->jsonResponse($response, $d);
    }

    public function createUpdateByTable($request, $response, $args) {
        $data = $request->getParsedBody();
        $table = explode(".", $data['userinfo_oTableColumn']);
        $elseConsoleData = array(
                               "userinfo_id" => $data["userinfo_id"]
                               , $table[1] => $data["userinfo_value"]
                           );
        $get = $this->consoleM->getElseConsole($table[0], "`userinfo_id`='".$data['userinfo_id']."'");
        if(count($get) > 1) {
            $d = $this->consoleM->updateElseConsole($table[0], $elseConsoleData, $data["userinfo_id"]);
        } else {
            $d = $this->consoleM->createElseConsole($table[0], $elseConsoleData, $data["userinfo_id"]);
        }
        $r = $d?array("success"=> true):array("success"=> false);
        return $this->jsonResponse($response, $r);
    }
}
