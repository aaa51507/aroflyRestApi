<?php

/**
 * Description of AuthenticationController
 *
 * @author Jim Yu <jimbo626@gmail.com>
 */
define("kAuthenticationTableName", "authentication");

class AuthenticationController extends BasicController {

    private $db;

    //put your code here
    public function __construct($ci) {
        parent::__construct(kAuthenticationTableName, $ci);
        $this->db = $this->ci->db;
    }

    public function verifyToken($token) {
        $token_info = $this->findTokenInDB($token);
        if ( $token_info !== false ) {
            return $token_info;
        }
        return false;
    }

    public function createToken($userinfo_ID) {
        return $this->setRandomToken($userinfo_ID);
    }

    // build & set token
    private function setRandomToken($userinfo_ID) {
        $md5_token = $this->forLoopBuildRandomToken();
        
//        echo $this->tableName;
        $sql = "SELECT *  FROM " . $this->tableName . " WHERE userinfo_ID = '" . $userinfo_ID . "'";
//        echo $sql;
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $data = array_filter($data);
            if ( !empty($data) ) {
    //            echo "Update";
                // Update
                $sql = "UPDATE ".$this->tableName." SET authentication_token='$md5_token',authentication_CreateDateTime='".date('Y-m-d H:i:s')."' WHERE userinfo_ID='$userinfo_ID'";
                $stmt = $this->db->prepare($sql);
                if ($stmt->execute()) {
                        return $md5_token;
                } else {
                        return false;
                }
            }
        }
        
//            echo "Insert";
        // Insert to authentication table
        $sql = "INSERT INTO " . $this->tableName . " (userinfo_ID, authentication_token, authentication_CreateDateTime)"
                . " VALUES (:userinfo_ID, :authentication_token, :authentication_CreateDateTime)";
        $data = array();
        $data["userinfo_ID"] = $userinfo_ID;
        $datatime = date('Y-m-d H:i:s');
        $data["authentication_CreateDateTime"] = $datatime;
        $data["authentication_token"] = $md5_token;
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute($data)) {
            //updata last login time
            $sql = "UPDATE userinfo SET userinfo_LastLoginTime='$datatime',userinfo_LastLoginIp='".$_SERVER['REMOTE_ADDR']."' WHERE userinfo_ID='$userinfo_ID'";
            $stmt = $this->db->prepare($sql);
            $stmt->execute();

            return $md5_token;
        }
        

        return false;
    }

    // build token
    private function forLoopBuildRandomToken() {
        while (1) {
            $token = getRandom(20);
            $md5_token = md5($token);
            if ( $this->findTokenInDB($md5_token) === false ) {
                break;
            }
        }
        return $md5_token;
    }

    private function findTokenInDB($token) {
        $sql = "SELECT au.*, u.* "
              ." , DATE_FORMAT(u.userinfo_CreateDateTime, '%Y-%m-%d %H:%i') as userinfo_CreateDateTime "
              ." , DATE_FORMAT(u.userinfo_LastLoginTime, '%Y-%m-%d %H:%i') as userinfo_LastLoginTime "
              ." FROM " . $this->tableName . " as au JOIN userinfo as u on u.userinfo_ID=au.userinfo_ID WHERE au.authentication_token = '" . $token . "'";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $data = array_filter($data);
            if ( !empty($data) ) {
                return $data;
            }
        }
        return false;
    }

}
