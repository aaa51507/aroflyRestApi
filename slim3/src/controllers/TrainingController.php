<?php
use \Interop\Container\ContainerInterface as ContainerInterface;
use Slim\Http\UploadedFile;

require_once ( __DIR__ . "/../models/console.php");

class TrainingController extends BasicController {
    private $tmpPath;
    private $excelPath;
    private $consoleColumn = [
                "userinfo_id"
                , "FORMAT"
                , "DATETIME"
                , "DEVICEID"
                , "CONSOLEID"
                , "APPV"
                , "CFV"
                , "EMAIL"
                , "BIRTH"
                , "WEIGHT"
                , "UNIT"
                , "BIKE"
                , "TRAINING"
                , "TIRE"
                , "INDOOR"
                , "time_s"
                , "pam_s"
                , "ahz_s"
                , "phz_s"
                , "paz_s"
                , "speed_s"
                , "cadence_s"
                , "power_s"
                , "psensor_s"
                , "distance_s"
                , "longitude_s"
                , "latitude_s"
                , "altitude_s"
                , "status"
                , "HRM"
                , "hrmax_pm"
                , "loop_pm"
                , "calorie"
            ];

    public function __construct(ContainerInterface $ci) {
        parent::__construct("Console", $ci);
        $this->ci = $ci;
        $this->db = $ci->db;
        $this->tmpPath = $this->ci->get('settings')['tmp']['path'];
        $this->excelPath = $this->ci->get('settings')['services']['path']."/excel";
        $this->consoleM = new Console($ci);
    }

    public function create($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $creater = $this->getLoginUser($request);
        //console
        $consoleData = $this->getNeedKeyByObject($this->consoleColumn, $data);
        $consoleData["userinfo_id"] = $creater["userinfo_ID"];
        $d = $this->consoleM->create($consoleData);
        $this->createUpdateElseConsole($consoleData, $creater["userinfo_ID"]);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    public function getList($request, $response, $args) {
        $data = $request->getQueryParams();
        $r = $this->consoleM->getList($data["pagingInfo"]);
        return $this->jsonResponse($response, $r);
    }

    public function getById($request, $response, $args) {
        $r = $this->consoleM->getById($args["id"]);
        return $this->jsonResponse($response, $r);
    }

    public function test($request, $response, $args) {
        $tableAndVal = array(
            array("table" => "userinfo_format", "data" => array("userinfo_format" => $consoleData["FORMAT"]))
        );
        $condition = "`userinfo_id`='145'";
        for($i=0;$i<count($tableAndVal);$i++) {
            $table = $tableAndVal[$i]["table"];
            $data = $tableAndVal[$i]["data"];
            $data["userinfo_id"] = $userinfoId;
            $get = $this->consoleM->getElseConsole($table, $condition);
            echo count($get);
        }
        // echo PHP_OS;
        //$r = $this->consoleM->getById($args["id"]);
        //return $this->jsonResponse($response, $r);
    }

    /**
     * trainingTest Controller固定輸出json格式
     */
    public function trainingTest($request, $response, $args) {
        $r = array(
            array(
                "start_time" => "2017-09-05 08:38:10"
                ,"step" => 1
                ,"duration" => 50
                ,"distance" => 944
                ,"state" => 1
                ,"calories" => 2
            )
        );
        return $this->jsonResponse($response, $r);
    }

    /**
     * 上傳檔案
     */
    public function upload($request, $response, $args) {
        //取得檔案
        $uploadedFiles = $request->getUploadedFiles();
        //handle single input with single file upload
        $uploadedFile = $uploadedFiles['uploadFile'];
        //取得參數
        $params = $request->getQueryParams();
        //取得登入者的資料
        $creater = $this->getLoginUser($request);

        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            if(PHP_OS == "Linux") {
                $movePath = $this->ci->get('settings')['csvFile']['linuxPath']."/".$creater["userinfo_ID"];
            } else {
                $movePath = $this->ci->get('settings')['csvFile']['windowsPath']."/".$creater["userinfo_ID"];
            }
            $file = $this->moveUploadedFile($uploadedFile, $movePath);
            if($file['extension'] != "afr" && $file['extension'] != "csv") {
                return $this->jsonResponse($response, array("success"=> false, "result"=> "Upload file type error."));
            }
            if($file['extension'] == "afr") {
                $this->arfToCsv($file["filePath"]);
                $file['filePath'] = explode(".", $file['filePath'])[0].".csv";
            }
            $consoleData = $this->getExcelData($file["filePath"]);
            //此id是否存在，不存在:新增   存在:更新
            $getById = $this->consoleM->getById($params['trainingCloudId']);
            $consoleData["userinfo_id"] = $creater["userinfo_ID"];
            if(count($getById) > 1) {
                $d = $this->consoleM->updateById($consoleData, $params['trainingCloudId']);
            } else {
                $d = $this->consoleM->create($consoleData);
            }
            //其他資料表新增資料
            $this->createUpdateElseConsole($consoleData, $creater["userinfo_ID"]);
            $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
            return $this->jsonResponse($response, $r);
        } else {
            return $this->jsonResponse($response, array("success"=> false, "result"=> "Upload file fail."));
        }
    }

    /**
     * 取得excel檔案資料，並轉為console data格式
     * @param string $filePath excel檔案路徑
     * @return object $consoleData ex:array("欄位名稱"=> "欄位值")
     */
    private function getExcelData($filePath) {
        require_once ( $this->excelPath."/Excel.php");
        $excel = new Excel();
        $data = $excel->consoleExcel($filePath);
        return $data;
    }

    /**
     * 上傳的檔案搬移至指定位子
     * @param UploadedFile $uploadedFile UploadedFile(slim object)
     * @param string $movePath 搬移至此路徑
     * @return array(
     *             name => 檔案名稱(含附檔名)
     *             path => 檔案路徑
     *             filePath => 檔案路徑含檔名
     *             extension => 附檔名
     *         )
     */
    private function moveUploadedFile(UploadedFile $uploadedFile, $movePath)
    {
        if(!is_dir($movePath)) {
            mkdir($movePath, 0777, true);
        }
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $filename = sprintf('%s.%s', date('Y-m-d_His'), $extension);
        $uploadedFile->moveTo($movePath . DIRECTORY_SEPARATOR . $filename);

        return array(
            "name" => $filename
            , "path" => $movePath
            , "filePath" => $movePath."/".$filename
            , "extension" => $extension
        );
    }

    /**
     * arf檔案轉csv (serverOS:windows環境才能轉)
     * @param string $filePath 檔案路徑
     * @return array(
     *             output =>
     *             return =>
     *         )
     */
    private function arfToCsv($filePath) {
        //OS type = windows
        $transferExe = $this->excelPath."/Transfer.exe ";
        $cmd = $transferExe.$filePath;
        exec($cmd, $output, $return);
        return array(
            "output" => $output
            ,"return" => $return
        );
    }

    private function createUpdateElseConsole($consoleData, $userinfoId) {
        $tableAndVal = array(
            array("table" => "userinfo_format", "data" => array("userinfo_format" => $consoleData["FORMAT"]))
            , array("table" => "userinfo_date_time", "data" => array("userinfo_date_time" => $consoleData["DATETIME"]))
            , array("table" => "userinfo_device_id", "data" => array("userinfo_device_id" => $consoleData["DEVICEID"]))
            , array("table" => "userinfo_console_id", "data" => array("userinfo_console_id" => $consoleData["CONSOLEID"]))
            , array("table" => "userinfo_appv", "data" => array("userinfo_appv" => $consoleData["APPV"]))
            , array("table" => "userinfo_cfv", "data" => array("userinfo_cfv" => $consoleData["CFV"]))
            , array("table" => "userinfo_email", "data" => array("userinfo_email" => $consoleData["EMAIL"]))
            , array("table" => "userinfo_birth", "data" => array("userinfo_birth" => $consoleData["BIRTH"]))
            , array("table" => "userinfo_weight", "data" => array("userinfo_weight" => $consoleData["WEIGHT"]))
            , array("table" => "userinfo_unit", "data" => array("userinfo_unit" => $consoleData["UNIT"]))
            , array("table" => "userinfo_bike_mode", "data" => array("userinfo_bike_mode" => $consoleData["BIKE"]))
            , array("table" => "userinfo_training_mode", "data" => array("userinfo_training_mode" => $consoleData["TRAINING"]))
            , array("table" => "userinfo_tire_size", "data" => array("userinfo_tire_size" => $consoleData["TIRE"]))
            , array("table" => "userinfo_indoor", "data" => array("userinfo_indoor" => $consoleData["INDOOR"]))
        );
        $condition = "`userinfo_id`='".$userinfoId."'";
        for($i=0;$i<count($tableAndVal);$i++) {
            $table = $tableAndVal[$i]["table"];
            $data = $tableAndVal[$i]["data"];
            $data["userinfo_id"] = $userinfoId;
            $get = $this->consoleM->getElseConsole($table, $condition);
            if(count($get) > 1) {
                $d = $this->consoleM->updateElseConsole($table, $data, $userinfoId);
            } else {
                $d = $this->consoleM->createElseConsole($table, $data, $userinfoId);
            }
        }
    }
}
