<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

require_once ( __DIR__ . "/../models/customer.php");

class CustomersController extends BasicController {

    private $db;
    //customer model
    private $customerM;
    //customerinfo必要的column;
    private $customerinfoNeedColumn = [
        'customerInfo_companyName'
        ,'customerInfo_PayTerm'
        ,'customerInfo_FaxNumber'
        ,'customerInfo_EMail'
        ,'customerInfo_TaxNumber'
        ,'customerInfo_PaymentMethod'
    ];
    //contactinfo必要的column;
    private $contactinfoNeedColumn = [
        'contactinfo_Name'
        ,'contactinfo_Title'
        ,'contactinfo_PhoneNumber'
        ,'contactinfo_Extension'
    ];
    //contactinfo必要的column;
    private $addressNeedColumn = [
        'address_Address'
    ];

    public function __construct(ContainerInterface $ci) {
        parent::__construct("Customer", $ci);
        $this->db = $ci->db;
        $this->customerM = new Customer($ci);
    }

    /**
     * 取得所有客戶資料
     */
    public function collection($request, $response, $args) {
        $data = $this->getTableData();
        return $this->jsonResponse($response, $data);
    }

    /**
     * 取得所有客戶資料(select2格式)
     */
    public function toSelect2($request, $response, $args) {
        $r = array();
        $sql = " SELECT cm.customerInfo_ID as id, cm.customerInfo_companyName as text "
              ." ,ct.contactinfo_Name, ct.contactinfo_PhoneNumber "
              ." FROM customerinfo as cm "
              ." inner join contactinfo as ct on cm.customerInfo_ContactID = ct.contactinfo_ID ";
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $r = 0;
        }
        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得所有客戶資料(DataTable格式)
     */
    public function toDatatable($request, $response, $args) {    
        //檢查Params格式是否有誤
        $params = $request->getQueryParams();
        $needKey = ["length","order","search"];
        if($this->checkParam($needKey, $params)) {
            return $this->parameterErrorResponse($response);
        }
        //search keyWord
        if(isset($params['searchKey'])) {
            //長度為5且為數字 = 只搜尋No
            if(is_numeric($params['searchKey']) && strlen($params['searchKey']) == 5) {
                $condition = " where cm.customerInfo_ID = '".(int)$params['searchKey']."'";
            } else {
                $condition = " where cm.customerInfo_companyName like '%".$params['searchKey']."%' "
                               ." or a.address_Address like '%".$params['searchKey']."%' "
                               ." or a.address_City like '%".$params['searchKey']."%' "
                               ." or a.address_Province like '%".$params['searchKey']."%' "
                               ." or ct.contactinfo_Name like '%".$params['searchKey']."%' "
                               ." or ct.contactinfo_Extension like '%".$params['searchKey']."%' "
                               ." or ct.contactinfo_Extension like '%".$params['searchKey']."%' "
                               ." or ct.contactinfo_PhoneNumber like '%".$params['searchKey']."%' "
                               ." or cm.customerInfo_Status = '".$params['searchKey']."' ";
           }
        } else {
            $condition = "";
        }

        //select資料
        $orderBy = " ORDER BY ".((int)$params["order"][0]["column"]+1)." ".$params["order"][0]["dir"];
        $limit = " LIMIT ".$params["start"].", ".$params["length"];
        $sql = "select SQL_CALC_FOUND_ROWS cm.customerInfo_ID, "
                    ." cm.customerInfo_companyName as companyName, "
                    ." CONCAT( a.address_Address, ' ', COALESCE(a.address_City, ''), ' ', COALESCE(a.address_Province, '')) as address, "
                    ." CONCAT( ct.contactinfo_Name, '<br>', ct.contactinfo_PhoneNumber ) as contact,"
                    ." IF(w.inProgress, w.inProgress, '0') as inProgress,"
                    ." cm.customerInfo_Status,"
                    ." CONCAT(cm.customerInfo_ID, ',', cm.customerInfo_Status) "
             ." from customerinfo as cm"
             ." inner join address as a on cm.customerInfo_AddressID = a.address_ID"
             ." inner join contactinfo as ct on cm.customerInfo_ContactID = ct.contactinfo_ID "
             ." left join (select w.*, count(w.workorder_ID) as inProgress from workorder as w "
                         ." group by w.customer_ID "
                         ." ) as w on w.customer_ID = cm.customerInfo_ID "
             .$condition
             .$orderBy.$limit;
             
        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $r["recordsTotal"] = $recordsTotal;
            $r["recordsFiltered"] = $recordsTotal;
        }

        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得customer jobsite資料ById(DataTable格式)
     */
    public function jobsiteToDatatableByCustomerId($request, $response, $args) {    
        //檢查Params格式是否有誤
        $params = $request->getQueryParams();
        //search keyWord
        if(isset($params['searchKey'])) {
            $condition = " j.jobsite_JoinID like '%".$params['searchKey']."%' "
                        ." or j.jobsite_ContractNo like '%".$params['searchKey']."%' "
                        ." or j.jobsite_Name like '%".$params['searchKey']."%' "
                        ." or j.jobsite_Scope like '%".$params['searchKey']."%' "
                        // ." or ct.Status like '%".$params['searchKey']."%' "
                        ." or j.wo like '%".$params['searchKey']."%' ";
        } else {
            $condition = "";
        }

        if(isset($params['startDate']) && isset($params['endDate'])) {
            if($condition != "") {
                $condition .= " and ";
            }
            $condition .= " j.jobsite_StartDateTime > '".$params['startDate']. "' and j.jobsite_StartDateTime < '".$params['endDate']."' ";
        } 

        if($condition != "") {
            $condition = " and ".$condition;
        }

        //select資料
        $orderBy = " ORDER BY ".((int)$params["order"][0]["column"]+1)." ".$params["order"][0]["dir"];
        $limit = " LIMIT ".$params["start"].", ".$params["length"];
        $sql = " select "
                  ." CONCAT(j.jobsite_ID, ',', j.jobsite_JoinID) as JobsiteNO "
                  ." , j.jobsite_ContractNo as Contract "
                  ." , j.jobsite_Name as JobSite "
                  ." , j.jobsite_Scope as Scope "
                  ." , j.jobsite_StartDateTime as StartDate "
                  ." ,'-' as Status "
                  ." , j.wo as wo "
              ." from customerinfo as c "
              ." left join (select j.*, count(w.workorder_ID) as wo from jobsite as j "
                         ." left join workorder as w on w.jobsite_ID = j.jobsite_ID "
                         ." group by j.jobsite_ID "
                         ." ) as j on j.jobsite_CustomerID = c.customerInfo_ID "
              ." where c.customerInfo_ID = '".(int)$params["customerId"]."'"
              .$condition
              .$orderBy.$limit;

        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $r["recordsTotal"] = $recordsTotal;
            $r["recordsFiltered"] = $recordsTotal;
        }

        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得customers by id
     */
    public function getByID($request, $response, $args) {
        $r = $this->customerM->getById($args["id"]);
        return $this->jsonResponse($response, $r);
    }

    /**
     * 新增customers
     */
    public function create($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $creater = $this->getLoginUser($request);
        //customerinfo
        $customerinfoColumn = ["customerInfo_companyName", "customerInfo_StartDate", "customerInfo_PayTerm", "customerInfo_FaxNumber", "jobsite_Scope"
                         ,"customerInfo_EMail", "customerInfo_TaxNumber", "customerInfo_Status", "customerInfo_Remarks", "customerInfo_Tag" 
                         ,"customerInfo_PaymentMethod"];
        $relateColumn = ["customer_Contact", "customer_Address"];
        
        $customerinfoData = $this->getNeedKeyByObject($customerinfoColumn, $data);
        $customerinfoData["customerInfo_CreateByID"] = $creater["userinfo_ID"];
        $relateData = $this->getNeedKeyByObject($relateColumn, $data);

        $d = $this->customerM->create($customerinfoData, $relateData);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    /**
     * 修改customers by id
     */
    public function updateById($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $updater = $this->getLoginUser($request);
        //customerinfo
        $customerinfoColumn = ["customerInfo_ID", "customerInfo_companyName", "customerInfo_StartDate", "customerInfo_PayTerm", "customerInfo_FaxNumber", "jobsite_Scope"
                         ,"customerInfo_EMail", "customerInfo_TaxNumber", "customerInfo_Status", "customerInfo_Remarks", "customerInfo_Tag" 
                         ,"customerInfo_PaymentMethod"];
        $relateColumn = ["customer_Contact", "customer_Address"];

        $customerinfoData = $this->getNeedKeyByObject($customerinfoColumn, $data);
        $customerinfoData["customerInfo_UpdateByID"] = $updater["userinfo_ID"];
        $customerinfoData["customerInfo_UpdateDateTime"] = 'now()';
        $relateData = $this->getNeedKeyByObject($relateColumn, $data);

        $d = $this->customerM->updateById($customerinfoData, $relateData);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    /**
     * 修改customers active
     */
    public function switchStatu($request, $response, $args) {
        //檢查request參數是否有誤
        $needKey = ["customerInfo_ID"];
        $data = $request->getParsedBody();
        if($this->checkParam($needKey, $data)) {
            return $this->parameterErrorResponse($response);
        }
        //取得customer資料
        $customerData = $this->customerM->getById($data["customerInfo_ID"]);
        //取得登入者的資料
        $updater = $this->getLoginUser($request);
        //customerinfo
        $customerinfoData = $this->getNeedKeyByObject($needKey, $data);
        $customerinfoData["customerInfo_UpdateDateTime"] = date('Y-m-d H:i:s');
        $customerinfoData["customerInfo_UpdateByID"] = $updater["userinfo_ID"];
        //修改狀態
        if($customerData["customerInfo_Status"] == "active") {
            $customerinfoData["customerInfo_Status"] = "inactive";
        } else {
            $customerinfoData["customerInfo_Status"] = "active";
        }
        $d = $this->customerM->updateStatus($customerinfoData, $data["customerInfo_ID"]);
        $r = $d?array("success"=> true, "status"=> $d["customerInfo_Status"]):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    /**
     * 檢查欄位資料是否有不存在
     * @param array $needKey 所需欄位
     * @param object $data 欄位資料
     * @return true資料錯誤(有不存在)/false資料正確(皆存在)
     */
    public function checkParam($needKey, $data) {
        for($i=0;$i<count($needKey);$i++) {
            if(!array_key_exists($needKey[$i], $data) || !isset($data[$needKey[$i]])) {
                return true;
            }
        }
        return false;
    }
}
