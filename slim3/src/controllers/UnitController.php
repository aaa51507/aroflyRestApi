<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

define("kUserInfoTableName", "unit");

require_once ( __DIR__ . "/../models/unit.php");

class UnitController extends BasicController {

    private $db;
    private $unitM;

    private $authController;

    //Constructor
    public function __construct(ContainerInterface $ci) {
        parent::__construct(kUserInfoTableName, $ci);
        $this->db = $ci->db;
        $this->unitM = new Unit($ci);

        $this->authController = new AuthenticationController($ci);
    }

    public function toDatatable($request, $response, $args) {
        //$this->ci->logger->info("Get unit list to Datatable");
        $result = $this->unitM->toDatatable($request->getQueryParams());
        return $this->jsonResponse($response, $result);
    }

    public function getByID($request, $response, $args) {
        // $this->ci->logger->info("Get unit by ID");
        $result = $this->unitM->getById($args["id"]);
        return $this->jsonResponse($response, $result);
    }
    
    public function create($request, $response, $args) {
        //$this->ci->logger->info("Create unit");
        $unitData = $request->getParsedBody();
        $updater = $this->getLoginUser($request);
        $unitData["unit_CreateByID"] = (int)$updater["userinfo_ID"];
        $unitData["unit_CreateDateTime"] = date('Y-m-d H:i:s');
        
        $result = $this->unitM->create($unitData);
        return $this->jsonResponse($response, $result);
    }

    public function updateById($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $updater = $this->getLoginUser($request);
        
        $unitColumn = ["unit_ID", "unit_LicensePlate", "unit_Status", "unit_LastDispatchedDateTime"];
        $unitData = $this->getNeedKeyByObject($unitColumn, $data);
        
        $unitData["unit_UpdateByID"] = $updater["userinfo_ID"];
        $unitData["unit_UpdateDateTime"] = 'now()';

        $d = $this->unitM->updateById($unitData);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }
    
}
