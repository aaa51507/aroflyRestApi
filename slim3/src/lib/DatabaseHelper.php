<?php
/**
 * Description of DatabaseHelper
 *
 * @author Jim Yu <jimbo626@gmail.com>
 */
class DatabaseHelper {
    /**
     * SELECT $objectsFormat FROM $table WHERE $conditionArray AND $conditionString
     * 
     * @param $databsae
     * @param $table Table Name
     * @param $conditionArray Condition Dictionary, (key, value) pairs 
     * @param $conditionString Condition String, default ""
     * @param $objectsFormat Select Objects String, default "*"
     * 
     * @return False or Data
     */
    public static function select($db, $table, $conditionArray = array(), $conditionString = "", $objectsFormat = '*') {
        $condition = "";
        // Loop throw condition array
        foreach ($conditionArray as $key => $value) {
            if (gettype($value) === "array") {
                $condition .= $key . "" . $value["type"];
                $value = $value["value"];
            } else {
                $condition .= $key . "=";
            }
            if (gettype($value) == "NULL") {
                $condition .= "'NULL' AND ";
            } else if (gettype($value) == "integer") {
                $condition .= $value . " AND ";
            } else if ($value == "") {
                $condition .= "'' AND ";
            } else {
                $condition .= "'" . $value . "' AND ";
            }
        }
        // Remove extra AND
        if (strlen($condition)) {
            $condition = substr($condition, 0, -4);
        }
        // Append ConditionString
        if (strlen($conditionString)) {
            if (strlen($condition)) {
                $condition .= " AND " . $conditionString;
            } else {
                $condition = $conditionString;
            }
        }
        // Construct SQL
        $sql = "SELECT $objectsFormat FROM $table";
        if (strlen($condition)) {
            $sql .= " WHERE " . $condition;
        }
        return DatabaseHelper::execute($db, $sql);
    }

    /**
     * Execute SQL Query
     * 
     * @param $db database connection
     * @param type $sql
     * @return false or Data
     */
    public static function execute($db, $sql) {
        $stmt = $db->prepare($sql);
        if ($stmt->execute()) {
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $f_data = array_filter($data);
            if (!empty($f_data)) {
                return $data;
            }
        }
        return FALSE;
    }
    
    public function selectMysql($db, $table, $Condition_arr = array(), $Condition2 = "", $SELECT = '*') {
        $Condition = "";
//        print_r(print$Condition_arr);
        if (!empty($Condition_arr)) {
            $Condition = "WHERE ";
            foreach ($Condition_arr as $key => $value) {
                if (gettype($value) === "array") {
                    $Condition .= $key . "" . $value["type"];
                    $value = $value["value"];
                } else {
                    $Condition .= $key . "=";
                }
                if (gettype($value) == "NULL") {
                    $Condition .= "'NULL' AND ";
                } else if (gettype($value) == "integer") {
                    $Condition .= $value . " AND ";
                } else if ($value == "") {
                    $Condition .= "'' AND ";
                } else {
                    $Condition .= "'" . $value . "' AND ";
                }
            }
            $Condition = substr($Condition, 0, -4);
        }
        $sql = "SELECT $SELECT FROM $table $Condition $Condition2";
        return DatabaseHelper::execute($db, $sql);
//        $stmt = $this->dbh->prepare($sql);
//        if ($stmt->execute()) {
//            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
//            $f_data = array_filter($data);
//            if (empty($f_data)) {
////                        $callback["success"]=false;
////                        $callback["msg"]="search empty";
//                return FALSE;
//            }
//            return $data;
//        } else {
////                    $callback["success"]=false;
////                    $callback["msg"]="cmd error";
//            return FALSE;
//        }
    }
        
    public static function execute2($db, $sql) {
        $stmt = $db->prepare($sql);
        if ($stmt->execute()) {
            return TRUE;
        }
        return FALSE;
    }
    
    public function updateMysql($db, $table, $json, $keyword ) {
//        echo $table;
//        print_r($json);
//        print_r($keyword);
            $set_value = "";   
            $key_string = "";
            foreach ($json as $key => $value) {
                if(gettype($value) == "string") {
                    $set_value = $set_value."`$key`='$value',";
                } else if(gettype($value) == "NULL") {
                    $set_value = $set_value."`$key`=null,";
                } else {
                    $set_value = $set_value."`$key`=$value,";
                }
            }
            $set_value = substr( $set_value ,0 ,-1);

            foreach ($keyword as $key => $value) {
                if(gettype($value) == "string") {
                    $key_string = $key_string."`$key`= '".$value."' AND";
                } else if( gettype($value) == "integer" ) {
                    $key_string = $key_string."`$key`=$value AND";
                } else if ( $value == ""){
                    $key_string = $key_string."`$key`= '' AND";
                } else if ( gettype($value) == "NULL"){
                    $key_string = $key_string."`$key` is null AND";
                } else {
                    $value = (string) $value;
                    $key_string = $key_string."`$key`= $value AND";
                }
            }
            $key_string = substr( $key_string ,0 ,-3);

            $sql = "UPDATE `$table` SET  $set_value WHERE $key_string";
//            echo $sql;
            return DatabaseHelper::execute2($db, $sql);
    }
}
