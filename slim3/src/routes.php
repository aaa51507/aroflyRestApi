<?php
/***
 * User controller
 */
$app->group('/users', function(){

    // User Info
    $this->get('', 'UsersController:getUserInfo');
    // User list
//    $this->get('', 'UsersController:collection');
    // User list allToDataTable
    $this->get('/toDataTable', 'UsersController:toDatatable');
    // Create user
    $this->post('/createUserByAdmin', 'UsersController:createUserByAdmin');
    //  get Permissions
    $this->get('/getPermissions', 'UsersController:getPermissionsByToken');
    //  check User State
    $this->get('/checkUserState', 'UsersController:checkUserState');
    // edit My Name
    $this->post('/editMyName', 'UsersController:editMyName');
    // edit My Email
    $this->post('/editMyEmail', 'UsersController:editMyEmail');
    // edit My Password
    $this->post('/editMyPassword', 'UsersController:editMyPassword');
    // re-Send Email For Auth
    $this->post('/reSendEmailForAuth', 'UsersController:reSendEmailForAuth');
    // edit By User ID
    $this->post('/editByUserID', 'UsersController:editByUserID');
    //switch userinfo status
    $this->post('/switchStatu', 'UsersController:switchStatu');
    //user Logout  
    $this->post('/userLogout', 'UsersController:userLogout');
    // get Employees Simple
    $this->get('/getEmployeesSimple', 'UsersController:getEmployeesSimple');
    // Get user by id
    $this->get('/{id}', 'UsersController:getUserByID');
    // Update user by id
    $this->put('/{id}', 'UsersController:updateUserByID');
    // Delete user by id
    $this->delete('/{id}', 'UsersController:deleteUserByID');
    // User reset password, update user password
//    $this->put('/{id}/reset-password', 'UsersController:resetPassword');
    //switch userinfo status
    
})->add('TokenAuthenticationMiddleware');
// Create user
$app->post('/users', 'UsersController:createUser');
// 
$app->post('/users/forgotPassword', 'UsersController:forgotPassword');
// 
$app->post('/users/emailAuthentication', 'UsersController:emailAuthentication');
// 
$app->post('/users/checkForgotToken', 'UsersController:checkForgotToken');
// 
$app->post('/users/editPasswordByForgotToken', 'UsersController:editPasswordByForgotToken');
// Content-Type: application/json; charset=utf-8
$app->post('/users/login', 'UsersController:userLogin')->add('BasicAuthenticationMiddleware');

/***
 * Employee controller
 */
$app->group('/employees', function(){
    // User list
    $this->get('', 'EmployeesController:collection');
    // User list allToDataTable
    $this->get('/toDataTable', 'EmployeesController:toDatatable');
    // Create employee
    $this->post('/buildEmployee', 'EmployeesController:createEmployee');
    // edit By User ID
    $this->post('/editByEmployeeID', 'EmployeesController:editByEmployeeID');
    // get Employee By ID
    $this->get('/getEmployeeByID', 'EmployeesController:getEmployeeByID');
    // get Employee By User ID
    $this->get('/getEmployeeByUserID', 'EmployeesController:getEmployeeByUserID');
    //switch userinfo status
    $this->post('/switchStatu', 'EmployeesController:switchStatu');
//    // Update user by id
//    $this->put('/{id}', 'EmployeesController:updateUserByID');
//    // Delete user by id
//    $this->delete('/{id}', 'EmployeesController:deleteUserByID');
//    // User reset password, update user password
//    $this->put('/{id}/reset-password', 'EmployeesController:resetPassword');
    
//    $this->get('/getPermissions', 'EmployeesController:getPermissions');
    
})->add('TokenAuthenticationMiddleware');


/***
 * Customer Controller
 */
$app->group('/customers', function(){
    $this->get('', 'CustomersController:collection');
    $this->get('/toSelect2', 'CustomersController:toSelect2');
    $this->get('/toDataTable', 'CustomersController:toDatatable');
    $this->get('/jobsiteToDatatableByCustomerId', 'CustomersController:jobsiteToDatatableByCustomerId');
    $this->post('/create', 'CustomersController:create');
    $this->post('/updateById', 'CustomersController:updateById');
    $this->post('/switchStatu', 'CustomersController:switchStatu');
    $this->get('/{id}', 'CustomersController:getByID');
})->add('TokenAuthenticationMiddleware');

/***
 * Jobsite Controller
 */
$app->group('/jobsites', function(){
    $this->get('/toSelect2', 'JobsiteController:toSelect2');
    $this->get('/toSelect2/{id}', 'JobsiteController:toSelect2');

    $this->get('/toDataTable', 'JobsiteController:toDatatable');
    $this->get('/workorderToDatatableByJobsiteId', 'JobsiteController:workorderToDatatableByJobsiteId');
    $this->post('/create', 'JobsiteController:create');
    $this->post('/updateById', 'JobsiteController:updateById');
    $this->get('/{id}', 'JobsiteController:getByID');
})->add('TokenAuthenticationMiddleware');

/***
 * Unit Controller
 */
$app->group('/units', function(){
    $this->get('/toDataTable', 'UnitController:toDatatable');
    $this->get('/{id}', 'UnitController:getByID');
    $this->post('/create', 'UnitController:create');
    $this->put('/updateById', 'UnitController:updateById');
})->add('TokenAuthenticationMiddleware');

/***
 * WorkOrder Controller
 */
$app->group('/workorders', function(){
    $this->post('/create', 'WorkOrderController:create');
    $this->post('/updateById', 'WorkOrderController:updateById');
    $this->post('/cancel', 'WorkOrderController:cancel');
    $this->get('/dispatching', 'WorkOrderController:dispatching');
    $this->get('/dispatchingUnit', 'WorkOrderController:dispatchingUnit');
    $this->get('/toDataTable', 'WorkOrderController:toDatatable');
    $this->get('/{id}', 'WorkOrderController:getByID');
})->add('TokenAuthenticationMiddleware');

/***
 * Activity Controller
 */
$app->group('/activitys', function(){
    $this->get('/toDataTable', 'ActivityController:toDatatable');
    $this->get('/getByEmployeeIdandDate', 'ActivityController:getByEmployeeIdandDate');
    $this->post('/punchTime', 'ActivityController:punchTime');
})->add('TokenAuthenticationMiddleware');

/***
 * userProfile Controller
 */
$app->group('/userProfile', function(){
    $this->get('/get', 'UserProfileController:getByTable');
    $this->get('/getAll', 'UserProfileController:getAll');
    $this->post('/set', 'UserProfileController:createUpdateByTable');
    $this->post('/setAll', 'UserProfileController:setAll');
})->add('TokenAuthenticationMiddleware');

/***
 * Training Controller
 */
$app->group('/training', function(){
    $this->post('/create', 'TrainingController:create');
    $this->post('/upload', 'TrainingController:upload');
    $this->get('/getList', 'TrainingController:getList');
    $this->get('/test', 'TrainingController:test');
    $this->get('/{id}', 'TrainingController:getByID');
})->add('TokenAuthenticationMiddleware');

/***
 * TrainingTest
 */
$app->get('/trainingTest', 'TrainingController:trainingTest');

/***
 * Testing
 */
$app->get('/[{name}]', function ($request, $response, $args) {
    if($args['name'] == "info") {
        return phpinfo();
    }
    else if($args['name'] == "backend") {
        header('Location: http://mepopedia.com');
    }
    else {
        // Sample log message
        $this->logger->info("Slim-Skeleton '/' route");    
        // Render index view
        return $this->renderer->render($response, '/index.phtml', $args);
    }
});
