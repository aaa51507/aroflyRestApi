<?php

//autoload名稱定義
namespace lib;

// Configuration Class
class Config {
	//定義靜態變數 dataType => object
    static $confArray;
	
	/**
	 * 根據key讀取conf定義的內容
	 * @param string $key 要取得的confArray key
	 * @return string confArray此key的val
	 */
    public static function read($key) {
        return self::$confArray[$key];
    }

	/**
	 * 將key & value值賦予靜態變數 $confArray
	 * @param string $key 物件的key
	 * @param string $name 要賦予物件key的val
	 */
    public static function write($key, $val) {
        self::$confArray[$key] = $val;
    }
}