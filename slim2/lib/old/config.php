<?php

date_default_timezone_set('Asia/Taipei');

define('MAX_NUM', 5);

define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '!@oort');
define('DB_NAME', 'r');

define('DB_MEMBER_GET_INFO', "" );

define('DEFAULT_HELP_INTEREST', 0.01 );
define('DEFAULT_DIRECT_ORGANIZE_INTEREST', 0.01 );
define('DEFAULT_DIRECT_ORGANIZE_MAX_INTEREST', 0.1 );
define('DEFAULT_DIRECT_PUSH_INCOME', 0.1 );
define('DEFAULT_HELP_INTEREST_TIME', "00:10:00" );

define('default_r_path', "C:\\xampp\\htdocs\\r\\" );
define('default_static_pages_path', "C:\\xampp\\htdocs\\r\\default_static_pages\\" );
define('upload_transient_file', "C:\\xampp\\htdocs\\r\\transient\\" );
define('data_path', "C:\\xampp\\htdocs\\r\\data\\" );
define('account_path', "C:\\xampp\\htdocs\\r\\data\\account\\" );
define('channel_path', "C:\\xampp\\htdocs\\r\\data\\channel\\" );
define('page_path', "C:\\xampp\\htdocs\\r\\data\\page\\" );
define('pair_path', "C:\\xampp\\htdocs\\r\\data\\pair\\" );
define('language_path', "C:\\xampp\\htdocs\\r\\language\\" );
define('delete_account_path', "C:\\xampp\\htdocs\\r\\data\\delete_file\\account\\" );
define('delete_channel_path', "C:\\xampp\\htdocs\\r\\data\\delete_file\\channel\\" );
define('delete_page_path', "C:\\xampp\\htdocs\\r\\data\\delete_file\\page\\" );
define('delete_pair_path', "C:\\xampp\\htdocs\\r\\data\\delete_file\\pair\\" );

define('http_email_send', "http://www.oort.com.tw/r/v_forget.php" );
define('http_upload_transient_file', "http://www.oort.com.tw/r/transient/" );
define('http_data_path', "http://www.oort.com.tw/r/data/" );
define('http_account_path', "http://www.oort.com.tw/r/data/account/" );
define('http_channel_path', "http://www.oort.com.tw/r/data/channel/" );
define('http_page_path', "http://www.oort.com.tw/r/data/page/" );
define('http_pair_path', "http://www.oort.com.tw/r/data/pair/" );
define('http_default_path', "http://www.oort.com.tw/r/" );
define('http_authenticate_path', "http://www.oort.com.tw/r/v_authenticate.php" );

define('http_manager_team_path', "http://www.oort.com.tw" );

?>