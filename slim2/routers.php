<?php

use controllers\User\userController;
use middleware\userMiddleware;
use controllers\Project\projectController;
use controllers\WorkOrder\workorderController;

$app->contentType('application/json');

$app->get('/', function () use ($app) {
	echo "index page";
});

$app->group('/project', function () use ($app) {
	$projectController = new projectController($app);
	$app->get('/all', function () use ($app, $projectController) {
		echo $projectController->getAll();
	});
	$app->get('/allToDataTable', function () use ($app, $projectController) {
		echo $projectController->getAllToDataTable();
	});	
});

$app->group('/workorder', function () use ($app) {
	$workorderController = new workorderController($app);
	$app->get('/all', function () use ($app, $workorderController) {
		echo $workorderController->getAll();
	});
	$app->get('/allToDataTable', function () use ($app, $workorderController) {
		echo $workorderController->getAllToDataTable();
	});	
});

$app->group('/member', function () use ($app) {
	
        function &array_make_references(&$arrSomething)
        {
            $arrAllValuesReferencesToOriginalValues=array();
            foreach($arrSomething as $mxKey=>&$mxValue)
                $arrAllValuesReferencesToOriginalValues[$mxKey]=&$mxValue;
            return $arrAllValuesReferencesToOriginalValues;
        }
	$userController = new userController($app);
	$userMiddleware = new userMiddleware($app);
	
        $authenticateForRole = function ( $role = 'normal' ) {
            return function () use ( $role ) {
//                $userController = new memberController($app);
                if ( $userController->belongsToRole($role) === false ) {
                    $return = array("success"=>false,"msg"=>"authen error");
                    echo json_encode($return);
                    exit;
                }
            };
        };
        
	$app->get('/', function () use ($app, $userController) {
		echo "member index";
	});
	$app->put('/', function() use($app, $userController) {
		echo $userController->put();
	});
	$app->delete('/', function() use($app, $userController) {
		echo $userController->delete();
	});
	
        
	$app->get('/all', function() use($app, $userController) {
		$app->contentType('application/json');
		echo $userController->getAll();
	});
	$app->get('/getUserByToken', array($userMiddleware, 'checkGetByToken'), function() use($app, $userController) {
		echo $userController->getByToken();
	});
	$app->get('/getUserToDataTable', function() use($app, $userController) {
		echo $userController->getUserToDataTable();
	});
        
	$app->post('/add', array($userMiddleware, 'checkAddParmas'), function() use($app, $userController) {
		echo $userController->add();
	});
	$app->get('/login', array($userMiddleware, 'checkLoginParmas'), function() use($app, $userController) {
		echo $userController->login();
	});
        
        //call_user_func_array(array($userMiddleware, "getByToken"), array("admin","POST"))
	$app->post('/buildEmployee', array($userMiddleware, 'checkBuildEmployeeParmas'), array($userMiddleware, 'checkAdminTokenPost') , function($member_data) use($app, $userController) {//  $authenticateForRole("admin"), , 
//                print_r($member_data);
//                echo $member_data;
		echo $userController->buildEmployee($member_data);
	});
	$app->get('/getEmployeeToDataTable', function() use($app, $userController) {
		echo $userController->getEmployeeToDataTable();
	});
        
});
