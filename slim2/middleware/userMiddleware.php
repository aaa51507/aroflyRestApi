<?php
namespace middleware;

require __DIR__.'/../lib/old/global.php';
//use lib\Core;
//use PDO;
use models\User\User;

class userMiddleware {
	
	private $app;
        
//	function __invoke(Request $req,  Response $res, callable $next) {
//            // Do stuff before passing along
//            $newResponse = $next($req, $res);
//            // Do stuff after route is rendered
//            return $newResponse; // continue
//        }
    
	function __construct($app) {
		$this->app = $app;
//		$this->core = Core::getInstance();
	}
        
//	public function getByToken($role = 'normal') {
//                $request = $this->app->request();
//                $token = $request->post('token');
//		$User = new User ();
//		$Users = $User->getUserByToken($token);
//                print_r($Users);
//                if(!$Users["success"]){
//                    exit;
//                }
//                if($role!==$Users["data"]["userinfo_InExUser"]){
//                    exit;
//                }
////		return json_encode($Users);
//                return true;
//	}
        
	public function checkAdminTokenPost($route) {
                $callback = array();
                $request = $this->app->request();
                $token = $request->post('token');
//		$Users = $this->getUserByToken($token);
                $User = new User ();
		$Users = $User->getUserByToken($token);
//                print_r($Users);
                if(!$Users["success"]){
                    echo json_encode($Users);
                    exit;
                }
                if($Users["data"]["userinfo_InExUser"]!=="admin"){
                    $callback['msg'] = "you don't have permissions";
                    $callback['success'] = false;
                    echo json_encode($callback);
                    exit;
                }
//		return json_encode($Users);
                $User_data = $Users["data"];
                //
                $routeParams = $route->getParams();
                if (!is_array($routeParams)) {
                    $routeParams = array();
                }
                array_push($routeParams, $User_data);  // <== could load the user object from the DB (or cache)
                $route->setParams($routeParams);
                
                return $Users["data"];
	}
        
	public function getByToken($role = 'normal',$type='GET') {
                $callback = array();
                $request = $this->app->request();
                $token = $type==="GET" ? $request->get('token') : $request->post('token');
//		$Users = $this->getUserByToken($token);
                $User = new User ();
		$Users = $User->getUserByToken($token);
//                print_r($Users);
                if(!$Users["success"]){
                    echo json_encode($Users);
                    exit;
                }
                if($role !== 'normal' && $role!==$Users["data"]["userinfo_InExUser"]){
                    $callback['msg'] = "you don't have permissions";
                    $callback['success'] = false;
                    echo json_encode($callback);
                    exit;
                }
//		return json_encode($Users);
                return $Users["data"];
	}
	
	public function getUserByToken($token) {
                $callback = array();
		$sql = "SELECT * from userinfo WHERE userinfo_token like '%$token%'";
		$stmt = $this->core->dbh->prepare($sql);
		if ($stmt->execute()) {
                        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        $f_data = array_filter($data);
                        if(empty($f_data)){
                            $callback["success"]=false;
                            $callback["msg"]="token get User info fail";
                            return $callback;  
                        }
		} else {
                        $callback["success"]=false;
                        $callback["msg"]="token get User info fail";
                        return $callback;
		}
//                print_r($data);
                unset($data[0]["userinfo_token"]);
                $callback["success"]=true;
                $callback["data"]=$data[0];
//                return setRandomToken($data);
                return $callback;
		$User = new User ();
		$users = $User->getUserByToken($token);
                if(!$users["success"]){
                    exit;
                }
                if($role!==$users["data"]["userinfo_InExUser"]){
                    exit;
                }
//		return json_encode($users);
                return true;

	}
        
	public function checkAddParmas() {
                if( !check_empty( array('userinfo_UserName','userinfo_Password','userinfo_Email') , $this->app , "POST" ) ) {
                        $callback['msg'] = "parameter is error.";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        exit;
                }
	}
	
	public function checkLoginParmas() {
                if( !check_empty( array('userinfo_UserName','userinfo_Password') , $this->app , "GET" ) ) {
                        $callback['msg'] = "parameter is error.";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        exit;
                }
	}
        
	public function checkGetByToken() {
                if( !check_empty( array('token') , $this->app , "GET" ) ) {
                        $callback['msg'] = "parameter is error.";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        exit;
                }
	}
        
	public function checkBuildEmployeeParmas() {
                if( !check_empty( array('token','employeeinfo_FirstName','employeeinfo_MiddleName','employeeinfo_LastName') , $this->app , "POST" ) ) {
                        $callback['msg'] = "parameter is error.";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        exit;
                }
	}
        
}
?>