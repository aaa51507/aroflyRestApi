﻿<?php

use models\Employee\Employee;

$app->get('/employee', function () use ($app) {
	echo "employee";
});

$app->get('/employee/getById', function () use ($app) {
	echo "employee getById";
});

$app->post('/employee/add', function () use ($app) {
	echo "employee add";
});

$app->put('/employee', function () {
	echo 'employee PUT route';
});

$app->delete('/employee', function () {
	echo 'employee DELETE route';
});