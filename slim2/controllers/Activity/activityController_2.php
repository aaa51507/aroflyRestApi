﻿<?php

use models\Activity\Activity;

$app->get('/activity', function () use ($app) {
	echo "activity";
});

$app->get('/activity/getById', function () use ($app) {
	echo "activity getById";
});

$app->post('/activity/add', function () use ($app) {
	echo "activity add";
});

$app->put('/activity', function () {
	echo 'activity PUT route';
});

$app->delete('/activity', function () {
	echo 'activity DELETE route';
});