<?php
namespace controllers\Project;
use models\CommonFunction;

class projectController {
	
	private $app;

	function __construct($app) {
		$this->app = $app;
	}
	
	public function getAll() {
		$comFun = new CommonFunction();
		return json_encode($comFun->getTableData("project"));
	}
	
	public function getAllToDataTable() {
		$comFun = new CommonFunction();
		return json_encode(array(
				"data" => $comFun->getTableDataToDataTable("project")
		));
	}
}
?>