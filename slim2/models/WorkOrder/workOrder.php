<?php
/**
 * This is an example of WorkOrder Class using PDO
 */
namespace models\WorkOrder;
use lib\Core;
use PDO;

class WorkOrder {
	protected $core;
	function __construct() {
		$this->core = Core::getInstance();
	}
	
	public function insertWorkOrder($data) {
            
	}

	public function getWorkOrderByToken($token) {
        
	}
        
	/**
	 *
	 * @retirm [object, object...] 
	 */
	public function getWorkOrders() {
		$r = array();		
		$sql = "SELECT * FROM workOrder";
		$stmt = $this->core->dbh->prepare($sql);
		//$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_ASSOC);		   	
		} else {
			$r = 0;
		}		
		return $r;
	}
	// Update the data of an workOrder
	public function updateWorkOrder($data) {
		
	}
	// Delete workOrder
	public function deleteWorkOrder($id) {
		
	}
}

?>