<?php
/**
 * This is an example of Jobsite Class using PDO
 */
namespace models\Jobsite;
use lib\Core;
use PDO;

class Jobsite {
	protected $core;
	function __construct() {
		$this->core = Core::getInstance();
	}
	
	public function insertJobsite($data) {
            
	}

	public function getJobsiteByToken($token) {
        
	}
        
	/**
	 *
	 * @retirm [object, object...] 
	 */
	public function getJobsites() {
		$r = array();		
		$sql = "SELECT * FROM jobsite";
		$stmt = $this->core->dbh->prepare($sql);
		//$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_ASSOC);		   	
		} else {
			$r = 0;
		}		
		return $r;
	}
	// Update the data of an jobsite
	public function updateJobsite($data) {
		
	}
	// Delete jobsite
	public function deleteJobsite($id) {
		
	}
}

?>