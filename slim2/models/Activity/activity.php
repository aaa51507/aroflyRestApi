<?php
/**
 * This is an example of Activity Class using PDO
 */
namespace models\Activity;
use lib\Core;
use PDO;

class Activity {
	protected $core;
	function __construct() {
		$this->core = Core::getInstance();
	}
	
	public function insertActivity($data) {
            
	}

	public function getActivityByToken($token) {
        
	}
        
	/**
	 *
	 * @retirm [object, object...] 
	 */
	public function getActivitys() {
		$r = array();		
		$sql = "SELECT * FROM activity";
		$stmt = $this->core->dbh->prepare($sql);
		//$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_ASSOC);		   	
		} else {
			$r = 0;
		}		
		return $r;
	}
	// Update the data of an activity
	public function updateActivity($data) {
		
	}
	// Delete activity
	public function deleteActivity($id) {
		
	}
}

?>