<?php
/**
 * This is an example of User Class using PDO
 */
namespace models\User;
use lib\Core;
use PDO;

class User {
	protected $core;
	function __construct() {
		$this->core = Core::getInstance();
	}
	
	/**
	 * 新增會員
	 * @param object $data 會員資料
	 * @return string 新增成功的會員id
	 */
	public function insertUser($data) {
                $callback = array();
		try {
                        $check = $this->core->selectMysql("userinfo",array("userinfo_UserName"=>$data["userinfo_UserName"]));
                        if($check){
                                $callback["success"]=false;
                                $callback["msg"]="帳號重複註冊";
                                return $callback;
                        }
                        $check = $this->core->selectMysql("userinfo",array("userinfo_Email"=>$data["userinfo_Email"]));
                        if($check){
                                $callback["success"]=false;
                                $callback["msg"]="E-mail重複註冊";
                                return $callback;
                        }
                        
                        $token = $this->buildRandomToken();
                        //, userinfo_Password, u_name, userinfo_Email, u_role, userinfo_CreateDateTime
                        //, :userinfo_Password, :u_name, :userinfo_Email, :u_role, :userinfo_CreateDateTime
			$sql = "INSERT INTO userinfo (userinfo_InExUser,userinfo_UserName,userinfo_Password,userinfo_Email,userinfo_CreateDateTime,userinfo_token)
					VALUES ('normal',:userinfo_UserName,:userinfo_Password,:userinfo_Email,:userinfo_CreateDateTime,:userinfo_token)";
			$stmt = $this->core->dbh->prepare($sql);
//			echo $data;
                        $data["userinfo_CreateDateTime"]=date('Y-m-d H:i:s');
                        $data["userinfo_token"]=$token["set"];
			if ($stmt->execute($data)) {
                                $callback["success"]=true;
                                $callback["data"]=$token["re_token"];
			} else {
                                $callback["success"]=false;
                                $callback["msg"]="Insert data fail";
			}
		} catch(PDOException $e) {
                        $callback["success"]=false;
                        $callback["msg"]=$e->getMessage();
                }
                return $callback;
	}
	/**
	 * 利用會員id取得特定會員
	 * @param string $id user.id
	 * @return  test
	 */
	public function getUserByToken($token) {
                $callback = array();
		$sql = "SELECT * from userinfo WHERE userinfo_token like '%$token%'";
		$stmt = $this->core->dbh->prepare($sql);
		if ($stmt->execute()) {
                        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        $f_data = array_filter($data);
                        if(empty($f_data)){
                            $callback["success"]=false;
                            $callback["msg"]="token get user info fail";
                            return $callback;  
                        }
		} else {
                        $callback["success"]=false;
                        $callback["msg"]="token get user info fail";
                        return $callback;
		}
//                print_r($data);
                unset($data[0]["userinfo_token"]);
                $callback["success"]=true;
                $callback["data"]=$data[0];
//                return setRandomToken($data);
                return $callback;
	}
	/**
	 * 登入
	 * @param string $id user.id
	 * @return  test
	 */
	public function userLogin($data) {
                $callback = array();
//                print_r($data);
                $check = $this->core->selectMysql("userinfo",$data);
                if(!$check){
                        $callback["success"]=false;
                        $callback["msg"]="帳號或密碼錯誤";
                        return $callback;
                }
//                print_r($check);
                return $this->setRandomToken( $check[0] );
	}
        
	/**
	 * 取得所有會員
	 * @retirm [object, object...] 會員資料
	 */
	public function getUsers() {
		$r = array();		
		$sql = "SELECT * FROM userinfo";
		$stmt = $this->core->dbh->prepare($sql);
		//$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_ASSOC);
		} else {
			$r = 0;
		}		
		return $r;
	}
	
	/**
	 * 取得所有會員
	 * @retirm [object, object...] 會員資料
	 */
	public function getUsersToDataTable() {
		$r = array();		
		$sql = "SELECT * FROM userinfo";
		$stmt = $this->core->dbh->prepare($sql);
		//$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_NUM);
		} else {
			$r = 0;
		}		
		return $r;
	}
        
	/**
	 * 新增Employee
	 * @param object $data Employee資料
	 * @return true
	 */
	public function buildEmployee($data,$member_data) {
                $callback = array();
		try {
                        //, userinfo_Password, u_name, userinfo_Email, u_role, userinfo_CreateDateTime
                        //, :userinfo_Password, :u_name, :userinfo_Email, :u_role, :userinfo_CreateDateTime
			$sql = "INSERT INTO employeeinfo (employeeinfo_FirstName,employeeinfo_MiddleName,employeeinfo_LastName,employeeinfo_CreateDateTime,employeeinfo_CreateByID)
					VALUES (:employeeinfo_FirstName,:employeeinfo_MiddleName,:employeeinfo_LastName,:employeeinfo_CreateDateTime,'".$member_data["userinfo_ID"]."')";
			$stmt = $this->core->dbh->prepare($sql);
//			echo $data;
                        $data["employeeinfo_CreateDateTime"]=date('Y-m-d H:i:s');
//                        $data["userinfo_token"]=$token["set"];
			if ($stmt->execute($data)) {
                                $callback["success"]=true;
			} else {
                                $callback["success"]=false;
                                $callback["msg"]="Insert data fail";
			}
		} catch(PDOException $e) {
                        $callback["success"]=false;
                        $callback["msg"]=$e->getMessage();
                }
                return $callback;
	}
        
	/**
	 * 取得所有Employee
	 * @retirm [object, object...] Employee資料
	 */
	public function getEmployeeToDataTable() {
		$r = array();		
		$sql = "SELECT * FROM employeeinfo";
		$stmt = $this->core->dbh->prepare($sql);
		//$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_NUM);
		} else {
			$r = 0;
		}		
		return $r;
	}
        
        
        
	// Update the data of an user
	public function updateUser($data) {
		
	}
	// Delete user
	public function deleteUser($id) {
		
	}
        
        // build token
        private function buildRandomToken() {
                $callback = array();
                $md5_token = $this->forLoopBuildRandomToken();
                $set_token = array( $md5_token );
                $set_token = json_encode( $set_token );
                return array("set"=>$set_token,"re_token"=>$md5_token);
        }
        
        // build & set token
        private function setRandomToken( $account ) {
                $MAX_NUM = MAX_NUM;
                $callback = array();
                $user_reuserme_token = $account["userinfo_token"];
                $md5_token = $this->forLoopBuildRandomToken();
                if( $user_reuserme_token === '' || $user_reuserme_token === '[]' ) {
                    $set_token = array( $md5_token );
                    $set_token = json_encode( $set_token );
                }
                else {
                    $user_reuserme_token = json_decode( $user_reuserme_token );
                    if( count( $user_reuserme_token ) >= (int)$MAX_NUM )//MAX_NUM
                    {
                        $splice_num = count( $user_reuserme_token ) - (int)$MAX_NUM + 1;
                        array_splice($user_reuserme_token, 0, $splice_num);
                    }
                    $user_reuserme_token[] = $md5_token ;
                    $set_token = $user_reuserme_token;
                    $set_token = json_encode( $set_token );
                }
                
                //update 一些資料
                $sql = "UPDATE userinfo SET userinfo_UpdateDateTime='".date('Y-m-d H:i:s')."', userinfo_token='".$set_token."' WHERE userinfo_ID=".$account["userinfo_ID"];
                $stmt = $this->core->dbh->prepare($sql);
                $data["userinfo_CreateDateTime"]=date('Y-m-d H:i:s');
                if ($stmt->execute($data)) {
                        $callback['data'] = $md5_token;
                        $callback['success'] = true;
                        return $callback;
                } else {
                        $callback['msg'] = "save token error";
                        $callback['success'] = false;
                        return $callback;
                }
                
                return $callback;
        }
        
        // build token
        private function forLoopBuildRandomToken() {
                while( 1 ) {
                    $token = getRandom(20);
                    $md5_token = md5($token);
                    $sql = "SELECT * from userinfo WHERE userinfo_token LIKE '%\\\"$md5_token\\\"%'";
                    $stmt = $this->core->dbh->prepare($sql);
                    if ($stmt->execute()) {
                        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        $data = array_filter($data);
                        if(empty($data)){
                                break;
                        }
                    } else {
                            break;
                    }
                }
                return $md5_token;
        }
}

?>
