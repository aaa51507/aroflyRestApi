<?php
/**
 * This is an example of Unit Class using PDO
 */
namespace models\Unit;
use lib\Core;
use PDO;

class Unit {
	protected $core;
	function __construct() {
		$this->core = Core::getInstance();
	}
	
	public function insertUnit($data) {
            
	}

	public function getUnitByToken($token) {
        
	}
        
	/**
	 *
	 * @retirm [object, object...] 
	 */
	public function getUnits() {
		$r = array();		
		$sql = "SELECT * FROM unit";
		$stmt = $this->core->dbh->prepare($sql);
		//$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_ASSOC);		   	
		} else {
			$r = 0;
		}		
		return $r;
	}
	// Update the data of an unit
	public function updateUnit($data) {
		
	}
	// Delete unit
	public function deleteUnit($id) {
		
	}
}

?>