// JavaScript Document

//上方中間最新消息頁籤
$(function(){
		var _showTab = 0;
		$('.tab-block').each(function(){
			var $tab = $(this);

			$('ul.tabs li', $tab).eq(_showTab).addClass('active');
			$('.tab_content', $tab).hide().eq(_showTab).show();

			$('ul.tabs li', $tab).click(function() {
				var $this = $(this),
					_clickTab = $this.find('a').attr('href');
				$this.addClass('active').siblings('.active').removeClass('active');
				$(_clickTab).stop(false, true).fadeIn(0).siblings().hide();

				return false;
			}).find('a').focus(function(){
				this.blur();
			});
		});
});

$(document).ready(function() {
		
	
});

//人氣作者頁籤效果
$(function(){
	$('#join-content div.item').eq(0).show();
	$('#join-content div.item').eq(1).hide();
	$('ul.hot-rank').eq(0).show();
	$('ul.hot-rank').eq(1).hide();
	
	$('#join-tab div.item').eq(0).click(function(e) {
		$(this).addClass('active');
		$('#join-tab div.item').eq(1).removeClass('active');
        $('#join-content div.item').eq(0).show();
		$('#join-content div.item').eq(1).hide();
    });
	$('#join-tab div.item').eq(1).click(function(e) {
		$(this).addClass('active');
		$('#join-tab div.item').eq(0).removeClass('active');
        $('#join-content div.item').eq(1).show();
		$('#join-content div.item').eq(0).hide();
    });
});

//廣告列表區 瀑布流
$(function(){
    if ( $("#ad-area-container").length >0 ) // jack 20160404
    {
	var $container = $("#ad-area-container").masonry();  
		$container.imagesLoaded(function() {
		$container.masonry({
			itemSelector:".ad-area",
			columnWidth:".grid-sizer"
		});
	});
    }
});