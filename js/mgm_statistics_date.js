/**
 * @author: abin
 * @createDate: 2016/3/14
 * */
$(document).ready(function() {
    /* highcharts 顯示語言設定*/
    Highcharts.setOptions({
          lang: {
                months: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
                weekdays: ["星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
                shortMonths: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            }
    });
    mgm_managers_console.HighStockChartSeriesEventFun = function(tag, e) {
        $.statistics_table(tag, e);
    }
    
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    today = mm+'/'+dd+'/'+yyyy;
    
    var firstDaythisYear = '1/1/'+yyyy;
    
    mgm_managers_console.getCntAccountByYear(formatDate(new Date(firstDaythisYear)), formatDate(new Date(today)));
    //mgm_managers_console.getCntAccountByYear(formatDate(new Date('2016/1/22')), formatDate(new Date('2016/4/30')));
});

var mgm_managers_console = {
    /**
     * HighStockChart折線圖，點擊圖表點時，會呼叫的function
     * @param obj tag bindevent的tag
     * @param clickEvent clickEvent 點擊的event值
     */
    HighStockChartSeriesEventFun: function(tagEvent, clickEvent) {
        console.log("HighStockChartSeriesEventFun");
        console.log(tagEvent);
        console.log(clickEvent);
    },
    
    /**
     * 取得一年內每日的會員成長數目
     * @param string startData 查詢起始日 ex:2015-01-01
     * @param string endDate 查詢結束日 ex:2015-01-31
     */
    getCntAccountByYear: function(startData, endDate) {
        var createLine = function(data) {
            console.log(data);
            var target = $("#chartList");
            var allLine = [];
            $.each(data, function(k,v){
        
                var yUnitName;
                switch( k ){ //table:date_statistics
                    case "income"://table:date_statistics > col:income ,etc
                        yUnitName = "支出";
                        break;
                    case "click":
                        yUnitName = "次數";
                        break;
                    case "vip":
                        yUnitName = "收入";
                        break;
                    case "page":
                        yUnitName = "文章數";
                        break;
                    case "accessories":
                        yUnitName = "次數";
                        break;
                    case "accessories_size":
                        yUnitName = "bytes";
                        break;
                }
                
                //20160528 AL
                target.append('<div class="col-lg-12" style="margin-bottom: 50px;">\n\
                                    <h4 class="word-middle-title"> 【 '+k+' <span class="word-span">'+v['name']+'</span> 】 </h4>\n\
                                    <div id="'+k+'" style="height: 200px;"></div>\n\
                               </div>');
                var point = {
                    events: {
                        click: function (e) {
                            var callBackFnc = mgm_managers_console.HighStockChartSeriesEventFun;
                            if(typeof(callBackFnc) == "function") {
                                callBackFnc(this, e);
                            } else {
                                console.log("HighStockChartSeriesEventFun is not a function!!");
                            }
                        }
                    }
                };
                mgm_managers_console.initHighStockChart(k, [{
                    name : yUnitName,
                    data : v['value'],
                    marker : {
                        enabled : true,
                        radius : 3
                    },
                    shadow : true,
                    point: point
                }]);
                allLine.push({name:v['name'], data:v['value'], point: point});
                return;
            });
            //20160528 AL
            target.prepend('<div class="col-lg-12" style="margin-bottom: 50px;">\n\
                                <h4 class="word-middle-title"> 【 All <span class="word-span">全部</span> 】 </h4>\n\
                                <div id="allLine" style="height: 200px;"></div>\n\
                           </div>');
            mgm_managers_console.initHighStockChart('allLine', allLine);
            $("#chartList").append('<div class="clearfix"></div>');
        }
        $.ajax({
                    type : "POST" ,
                    dataType: "json",
                    url : "php/mgm_statistics_date.php" ,
                    async: true ,
                    data : {
                        startDate: startData
                        ,endDate: endDate
                    } ,
                    success : createLine ,
                    error : function(e) {console.log(e)}
        });
    },

    /**
     * 建置Highcharts的折線圖
     * @param string tagID tag的id
     * @param array xAxis x軸的資料
     * ex:[1213,1235,2342]
     * @param array data y軸的資料
     * ex[1213,1235,2342]
     */
    initHighcharts: function(tagID, xAxis, data) {
        if($('#'+tagID).highcharts() != undefined) {
            $('#'+tagID).highcharts().destroy();
        }
        $('#'+tagID).html("");
        $('#'+tagID).highcharts({
            title: {
                text: 'a',
                style: {
                    visibility: 'hidden'
                }
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: xAxis
            },
            yAxis: {
                title: {
                    text: yUnitName,
                    rotation:360,
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }],
                labels: {
                    formatter: function () {
                        return Highcharts.numberFormat(this.value,0);
                    }
                }
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'center',
                verticalAlign: 'bottom',
                borderWidth: 0
            },
            series: [{
                name: yUnitName,
                data: data
            }],
            credits: {
                enabled: false
            },
        });
    },
    
    /**
     * 建置HighStockChart的折線圖
     * @param string tagID tag的id
     * @param array[array] data 折線圖的資料
     * ex:[[1236902400000,148],[1237161600000,359],[1237248000000,871],[1237334400000,954]]
     */
    initHighStockChart: function(tagID, data) {
        if($('#'+tagID).highcharts() != undefined) {
            $('#'+tagID).highcharts().destroy();
        }
        $('#'+tagID).html("");
        $('#'+tagID).highcharts('StockChart', {
            /*
            chart: {
                height: 200,
            },
            */
            rangeSelector : {
                selected: 4,
                inputEnabled: false,
                buttonTheme: {
                    visibility: 'hidden'
                },
                labelStyle: {
                    visibility: 'hidden'
                }
            },

            title : {
                text : ''
            },
            yAxis: {
                allowDecimals: false,
            },
            series : data,
            credits: {
                enabled: false
            }
        });
    },
};

/**
 * 日期轉字串 yyyy-MM-dd
 * @param date date 時間
 */
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
