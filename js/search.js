
$("document").ready(function() {

        cancel_checkall();
        fn_sql_show_full_columns_from_table();
        fn_btn_search();
        fn_get_all_btn_fast();
        
        //--------------

        $("#btn_search").unbind('click').bind('click', function() {
                fn_btn_clean_search_condition();
                fn_btn_search();
        });

        $.set_search_conditions = [];
        $("#btn_add_search_condition").unbind('click').bind('click', function() {
                fn_btn_add_search_condition();
        });

        $("#btn_clean_search_condition").unbind('click').bind('click', function() {
                fn_btn_clean_search_condition();
        });

        $("#btn_set_btn_fast").unbind('click').bind('click', function() {
            if ( $("#area_btn_search_condition_list").children() == [] ) {
                    show_remind("須先新增搜尋條件");
            } else {
                    fn_btn_set_btn_fast();
            }
        });

        /*搜尋快捷鍵*/
        $("#area_btn_fast_name").delegate("[id='btn_fast']", "click", function() {
                $("#area_btn_search_condition_list").html('');
                $.set_search_conditions = [];
                $.as_id = $(this).attr('as_id');
                fn_btn_fast_list_conditions();
                fn_btn_fast_list_search();
        });

        /*搜尋快捷鍵 刪除X icon*/
        $("#area_btn_fast_name").delegate("[id='btn_fast_delete']", "click", function() {
                $.as_id = $(this).parent().children("div[id='btn_fast']").attr('as_id');
                fn_btn_fast_delete();
        });

        /*搜尋條件*/
        $("#area_btn_search_condition_list").delegate("[id='search_condituon_delete']", "click", function() {
                var key = $(this).attr( "key" );
                console.log( $.set_search_conditions );
                $.each( $.set_search_conditions , function( index , value ){
                        console.log( value["key"] )
                        console.log( 123)
                        if( value["key"] == key ){
                                $.set_search_conditions.splice( index , 1 );
                                console.log( 456)
                                return false;
                        }
                });
                $(this).parent().remove();
                console.log( $.set_search_conditions );

                fn_btn_search_condition_query();
        });
        
});




function init_page_switch(){

        if( $.View != undefined  )
        {
                    $.View.view_getOptionsFromForm().destroy();
                    $.View.view_getOptionsFromForm()._SetOpts({
                            focus: $("body"),
                            focus_2: $("#showresult"),
                            focus_3: $("#page-switch"),
                            hiddenresult : $( "#hiddenresult" ),
                            cancel_checkbox_func: function(){
                                if( $( "#checkall" ).is( ":checked" ) ) {
                                    $( "#checkall" ).click();
                                }
                            }
                    });
                    $.View.view_getOptionsFromForm().init();
            
        }


}

function fn_btn_clean_search_condition(){
        $("#area_btn_search_condition_list").html('');
        $.set_search_conditions = [];
        $.View.view_getOptionsFromForm().destroy();
        $("#showresult").html('');
}

function fn_sql_show_full_columns_from_table() {

        var data = {
        };
        var success_back = function(data) {

                console.log(data);
                var tmp = "";
                data = JSON.parse(data);
                console.log(data);
                if (data.success) {

                        $.each(data.data, function(index, value) {
                                tmp += '<option value="' + value.field + '">' + value.comment + '</option>';
                        });

                        $("#selece-search-table thead select").html(tmp);
                        fn_btn_default();
                } else {
                        $("#selece-search-table thead select").html("");
                        //show_remind( data.msg , "error" );
                }

        };
        var error_back = function(data) {
                console.log(data);
        };
        $.Ajax("POST", "php/" + $.global_php + "?func=fn_sql_show_full_columns_from_table", data, "", success_back, error_back);

}

function fn_btn_search() {

            var data = {
            };
            var success_back = function(data) {

                    console.log(data);
                    var tmp = "";
                    data = JSON.parse(data);
                    console.log(data);
                    if (data.success) 
                    {
                            var select_colname = [];
                            $.each( $("table select:not(#search_select)") , function(index, value) {
                                    //if( $.inArray( select_colname , $( value ).val() ) === -1 )
                                    select_colname[select_colname.length] = $(value).val();
                            })

                        console.log(select_colname);
                        $.each( data.data, function(index, value) {

                                tmp += '<tr style="height: 120px;" class="showing odd child-middle" role="row"'+ $.global_AI_id +'="' + value[$.global_AI_id]+ '">';

                                tmp += '<td class="center"><input type="checkbox" name="vehicle" ></td>'; // checkbox

                                console.log(value);
                                for (var i = 0; i < select_colname.length ; i++ )
                                {
                                        if (value && value[select_colname[i]]) 
                                        {
                                                if (    value[select_colname[i]].search(".jpg") !== -1 ||
                                                        value[select_colname[i]].search(".jpeg") !== -1 ||
                                                        value[select_colname[i]].search(".png") !== -1 ||
                                                        value[select_colname[i]].search(".gif") !== -1) 
                                                {
                                                        tmp += '<td class="center">' +
                                                                '<a>' +
                                                                '<div style="background-image: url(\'' + value[select_colname[i]] + '\'); cursor: pointer; width: 100px; height: 100px; margin: 0px; left: 7%;" class="bg_top"></div>' +
                                                                '</a>' +
                                                                '</td>';
                                                }
                                                else 
                                                {
                                                        if (value[select_colname[i]].length > 30) {
                                                                tmp += '<td class="center">' + value[select_colname[i]].substr(0, 30) + '...' + '</td>';
                                                        } else {
                                                                tmp += '<td class="center">' + value[select_colname[i]] + '</td>';
                                                        }
                                                }
                                        } else {
                                                tmp += '<td class="center"></td>';
                                        }
                                }

                                tmp +=  '<td class="child-inline center">';
                                
                                /*鉛筆icon 會員清單 mgm_management_account, 管理公告json_site_notice_manage, 管理訊息json_station_message */
                                if ($.global_php == 'json_mg_account.php' || $.global_php == 'json_site_notice_manage.php' || $.global_php == 'json_station_message.php'){
                                        tmp +=  '<button id="PencilModify" class="btn btn-xs btn-info blue-button" style="margin-right:5px;">' +
                                                '<i class="ace-icon fa fa-pencil bigger-120"></i>' +
                                                '</button>';
                                }
                                
                                
                                /*垃圾桶刪除icon 管理公告json_site_notice_manage, 管理訊息json_station_message */
                                if ($.global_php == 'json_site_notice_manage.php' || $.global_php == 'json_station_message.php'){
                                        tmp +=  '<button id="RubbishDelete" class="btn btn-xs btn-info blue-button" style="margin-right:5px;">' +
                                                '<i class="ace-icon fa fa-trash-o bigger-120"></i>' +
                                                '</button>';
                                }
                                
                                
                                if ($.global_state == 'a_state'){ //會員清單 mgm_management_account
                                        if (value[$.global_state] == 'block') {
                                                tmp += '<button id="display_block" class="btn btn-xs btn-danger green-button" style="margin-right:5px;">暫停會員</button>';
                                        } else if (value[$.global_state] == 'blockade') {
                                                tmp += '<button id="display_block" class="btn btn-xs btn-danger red-button" style="margin-right:5px;">已暫停會員</button>';
                                        }
                                } else if ($.global_state == "p_display"){ //文章管理 mgm_articles_list_manage
                                        if (value[$.global_state] == 'block') {
                                                tmp += '<button id="display_block" class="page_display btn btn-xs btn-danger green-button" style="margin-right:5px;" display="block">上架中</button>';
                                        } else if (value[$.global_state] == 'none') {
                                                tmp += '<button id="display_block" class="page_display btn btn-xs btn-danger red-button" style="margin-right:5px;" display="none">下架中</button>';
                                        }
                                } else if (value[$.global_state] == 'blockade') {
                                        
                                }
                                tmp += '</td>' + '</tr>';
                        });

                        //$( "#selece-search-table tbody" ).html( tmp );
                        $("#hiddenresult").html(tmp);

                        init_page_switch();


                } else {
                        $("#page-switch").html('');
                        $("#hiddenresult").html('');
                        $("#showresult").html('');
                        show_remind( "找不到符合搜尋字詞的會員" );

                }

        };
        var error_back = function(data) {
                console.log(data);
        };
        $.Ajax( "POST" , "php/" + $.global_php + "?func=fn_btn_search" , data, "" , success_back, error_back);


}

function fn_btn_add_search_condition()
{
        $( "#area_btn_search_condition_list" ).append(
                '<div style="display: block; background-color: #26b4a7; float: left; padding: 4px; margin-left: 5px; margin-top: 5px; height: 30px; color: white; ">' +
                '    <button class="close" type="button" style="float: left; font-size: 24px; margin-right: 5px;" id="search_condituon_delete" key="' + $("#search_select").val() + "_" + $("#input_search_condition").val().toString() + '"> × </button>' +
                '    <div id="btn-search-con-text" style="float: left; margin-right: 10px;">' + $("#search_select").children( "option[value=" + $("#search_select").val() + "]" ).html() + ': ' + $("#input_search_condition").val() + '</div>' +
                '</div>'
        );

        var push_obj = {};
        var search_select = $("#search_select").val().toString();
        var input_search_condition = $("#input_search_condition").val().toString();
        push_obj[search_select] = input_search_condition;
        push_obj["key"] = search_select + "_" + input_search_condition;
        $.set_search_conditions.push( push_obj );

        console.log( $.set_search_conditions );
        $("#input_search_condition").val('');

        fn_btn_search_condition_query();
}

function fn_btn_search_condition_query(){

        var search_cmd = JSON.parse(JSON.stringify($.set_search_conditions));
        $.each( search_cmd , function( index , value ){
                delete value["key"];
                search_cmd [ index ] = value;
        });

        var data = {
                search_cmd: JSON.stringify(search_cmd)
        };
        var success_back = function(data) {

                console.log(data);
                var tmp = "";
                data = JSON.parse(data);
                console.log(data);
                if (data.success) 
                {
                        var select_colname = [];
                        $.each( $("table select:not(#search_select)") , function(index, value) {
                                //if( $.inArray( select_colname , $( value ).val() ) === -1 )
                                select_colname[select_colname.length] = $(value).val();
                        })

                        console.log(select_colname);
                        $.each( data.data, function(index, value) {

                                tmp += '<tr style="height: 120px;" class="showing odd child-middle" role="row"'+ $.global_AI_id +'="' + value[$.global_AI_id]+ '">';

                                tmp += '<td class="center"><input type="checkbox" name="vehicle" ></td>'; // checkbox

                                console.log(value);
                                for (var i = 0; i < select_colname.length ; i++ )
                                {
                                        if (value && value[select_colname[i]]) 
                                        {
                                                if (    value[select_colname[i]].search(".jpg") !== -1 ||
                                                        value[select_colname[i]].search(".jpeg") !== -1 ||
                                                        value[select_colname[i]].search(".png") !== -1 ||
                                                        value[select_colname[i]].search(".gif") !== -1) 
                                                {
                                                        tmp += '<td class="center">' +
                                                                '<a>' +
                                                                '<div style="background-image: url(\'' + value[select_colname[i]] + '\'); cursor: pointer; width: 100px; height: 100px; margin: 0px; left: 7%;" class="bg_top"></div>' +
                                                                '</a>' +
                                                                '</td>';
                                                }
                                                else 
                                                {
                                                        if (value[select_colname[i]].length > 30) {
                                                                tmp += '<td class="center">' + value[select_colname[i]].substr(0, 30) + '...' + '</td>';
                                                        } else {
                                                                tmp += '<td class="center">' + value[select_colname[i]] + '</td>';
                                                        }
                                                }
                                        } else {
                                                tmp += '<td class="center"></td>';
                                        }
                                }

                                tmp +=  '<td class="child-inline center">';
                                
                                /*鉛筆icon 會員清單 mgm_management_account, 管理公告json_site_notice_manage, 管理訊息json_station_message */
                                if ($.global_php == 'json_mg_account.php' || $.global_php == 'json_site_notice_manage.php' || $.global_php == 'json_station_message.php'){
                                        tmp +=  '<button id="PencilModify" class="btn btn-xs btn-info blue-button" style="margin-right:5px;">' +
                                                '<i class="ace-icon fa fa-pencil bigger-120"></i>' +
                                                '</button>';
                                }
                                
                                
                                /*垃圾桶刪除icon 管理公告json_site_notice_manage, 管理訊息json_station_message */
                                if ($.global_php == 'json_site_notice_manage.php' || $.global_php == 'json_station_message.php'){
                                        tmp +=  '<button id="RubbishDelete" class="btn btn-xs btn-info blue-button" style="margin-right:5px;">' +
                                                '<i class="ace-icon fa fa-trash-o bigger-120"></i>' +
                                                '</button>';
                                }
                                
                                
                                if ($.global_state == 'a_state'){ //會員清單 mgm_management_account
                                        if (value[$.global_state] == 'block') {
                                                tmp += '<button id="display_block" class="btn btn-xs btn-danger green-button" style="margin-right:5px;">暫停會員</button>';
                                        } else if (value[$.global_state] == 'blockade') {
                                                tmp += '<button id="display_block" class="btn btn-xs btn-danger red-button" style="margin-right:5px;">已暫停會員</button>';
                                        }
                                } else if ($.global_state == "p_display"){ //文章管理 mgm_articles_list_manage
                                        if (value[$.global_state] == 'block') {
                                                tmp += '<button id="display_block" class="page_display btn btn-xs btn-danger green-button" style="margin-right:5px;">上架中</button>';
                                        } else if (value[$.global_state] == 'none') {
                                                tmp += '<button id="display_block" class="page_display btn btn-xs btn-danger red-button" style="margin-right:5px;">下架中</button>';
                                        }
                                } else if (value[$.global_state] == 'blockade') {
                                        
                                }
                                tmp += '</td>' + '</tr>';
                        });

                        //$( "#selece-search-table tbody" ).html( tmp );
                        $("#hiddenresult").html(tmp);

                        init_page_switch();


                } else {
                        $("#page-switch").html('');
                        $("#hiddenresult").html('');
                        $("#showresult").html('');
                        show_remind( "找不到符合搜尋字詞的會員" );

                }

        };
        var error_back = function(data) 
        {
                console.log(data);
        };
        $.Ajax("POST", "php/" + $.global_php + "?func=fn_btn_search_condition_query" , data , "" , success_back, error_back);


}

function fn_btn_set_btn_fast()
{
        var fast_name = $("#fast-name").val();
        $("#fast-name").val('');

        $.each( $.set_search_conditions , function( index , value ){
                delete value["key"];
                $.set_search_conditions[ index ] = value;
        });
        console.log($.set_search_conditions);

        var data = {
                fast_name: (fast_name == '') ? '預設' : fast_name,
                sql: JSON.stringify($.set_search_conditions)
        };
        var success_back = function(data) {

                console.log(data);
                data = JSON.parse(data);
                console.log(data);
                if (data.success) {

                        show_remind("成功儲存搜尋快捷");
                        $( "#area_btn_fast_name" ).append(
                                '<div style="display: block; background-color: rgb(145, 184, 208); float: left; padding: 4px; margin-left: 5px; margin-top: 5px; height: 30px; color: white; ">' +
                                '    <button class="close" type="button" style="float: left; font-size: 24px; margin-right: 5px;" id="btn_fast_delete" as_id="' + data.as_id + '"> × </button>' +
                                '    <div id="btn_fast" style="float: left; margin-right: 10px;" as_id="' + data.data.as_id + '">' + data.data.fast_name + '</div>' +
                                '</div>'
                        );

                        $( "#area_btn_search_condition_list" ).children().remove();
                        $.set_search_conditions = [];

                } else {
                        show_remind("儲存搜尋快捷失敗");
                }

        };
        var error_back = function(data) {
                console.log(data);
        };
        $.Ajax("POST", "php/" + $.global_php + "?func=fn_btn_set_btn_fast", data, "", success_back, error_back);


}

function fn_get_all_btn_fast()
{
        var data = {
        };
        var success_back = function(data) {

                console.log(data);
                data = JSON.parse(data);
                console.log(data);
                if (data.success) {
                        var tmp = '';
                        $.each(data.data, function(index, value) {
                                tmp +=  '<div style="display: block; background-color: rgb(145, 184, 208); float: left; padding: 4px; margin-left: 5px; margin-top: 5px; height: 30px; color: white; ">' +
                                        '       <button as_id="' + value.as_id + '" class="close" type="button" style="float: left; font-size: 24px; margin-right: 5px;" id="btn_fast_delete"> × </button>' +
                                        '       <div id="btn_fast" style="float: left; margin-right: 10px;" as_id="' + value.as_id + '">' + value.a_search_name + '</div>' +
                                        '</div>';
                        });

                        $("#area_btn_fast_name").html(tmp);

                } else {
                        $("#area_btn_fast_name").html('');
                }

        };
        var error_back = function(data) {
                console.log(data);
        };
        $.Ajax("POST", "php/" + $.global_php + "?func=fn_get_all_btn_fast", data, "", success_back, error_back);


}

function fn_btn_fast_delete()
{
        var data = {
                as_id: $.as_id
        };
        var success_back = function(data) {

                console.log(data);
                data = JSON.parse(data);
                console.log(data);
                if (data.success) {
                        $("[as_id=" + $.as_id + "]").parent().remove();
                        $("#area_btn_search_condition_list").html('');
                        $.set_search_conditions = [];
                } else {
                }

        };
        var error_back = function(data) {
                console.log(data);
        };
        $.Ajax("POST", "php/" + $.global_php + "?func=fn_btn_fast_delete", data, "", success_back, error_back);


}

function fn_btn_fast_list_conditions()
{
        var data = {
                as_id: $.as_id
        };
        var success_back = function(data) {

                console.log(data);
                data = JSON.parse(data);
                console.log(data);
                if (data.success) {
                        var tmp = '';
                        $.each(data.data, function(index, value) {
                                //$("#search_select").children( "option[value=" + $("#search_select").val() + "]" ).html()
                                //var column = $("#search_select").children('option').attr(value['column']).html();

                                var table = value['table'];
                                var column = value['column'];
                                var value = value['value'];

                                var column_html = $("#search_select").children( "option[value=" + column + "]" ).html();
                                var value_html = '';
                                value_html = value.replace(/%/g,"").toString();

                                tmp +=  '<div style="display: block; background-color: #26b4a7; float: left; padding: 4px; margin-left: 5px; margin-top: 5px; height: 30px; color: white; ">' +
                                        '       <button class="close" type="button" style="float: left; font-size: 24px; margin-right: 5px;" id="search_condituon_delete" key="' + column + "_" + value + '"> × </button>' +
                                        '       <div id="" style="float: left; margin-right: 10px;" table="' + table + '" column="' + column + '" val="' + value + '">' + column_html + ': ' + value_html + '</div>' +
                                        '</div>';

                                var push_obj = {};
                                push_obj[ column ] = value ;
                                push_obj["key"] = column + "_" + value;
                                $.set_search_conditions.push( push_obj );
                        });
                        console.log( $.set_search_conditions );

                        $("#area_btn_search_condition_list").html(tmp);

                } else {
                }

        };
        var error_back = function(data) {
                console.log(data);
        };
        $.Ajax("POST", "php/" + $.global_php + "?func=fn_btn_fast_list_conditions", data, "", success_back, error_back);


}

function fn_btn_fast_list_search() {

        var data = {
                as_id: $.as_id
        };
        var success_back = function(data) {

                console.log(data);
                var tmp = "";
                data = JSON.parse(data);
                console.log(data);
                if (data.success) 
                {
                        var select_colname = [];
                        $.each( $("table select:not(#search_select)") , function(index, value) {
                                //if( $.inArray( select_colname , $( value ).val() ) === -1 )
                                select_colname[select_colname.length] = $(value).val();
                        })

                        console.log(select_colname);
                        $.each( data.data, function(index, value) {

                                tmp += '<tr style="height: 120px;" class="showing odd child-middle" role="row"'+ $.global_AI_id +'="' + value[$.global_AI_id]+ '">';

                                tmp += '<td class="center"><input type="checkbox" name="vehicle" ></td>'; // checkbox

                                console.log(value);
                                for (var i = 0; i < select_colname.length ; i++ )
                                {
                                        if (value && value[select_colname[i]]) 
                                        {
                                                if (    value[select_colname[i]].search(".jpg") !== -1 ||
                                                        value[select_colname[i]].search(".jpeg") !== -1 ||
                                                        value[select_colname[i]].search(".png") !== -1 ||
                                                        value[select_colname[i]].search(".gif") !== -1) 
                                                {
                                                        tmp += '<td class="center">' +
                                                                '<a>' +
                                                                '<div style="background-image: url(\'' + value[select_colname[i]] + '\'); cursor: pointer; width: 100px; height: 100px; margin: 0px; left: 7%;" class="bg_top"></div>' +
                                                                '</a>' +
                                                                '</td>';
                                                }
                                                else 
                                                {
                                                        if (value[select_colname[i]].length > 30) {
                                                                tmp += '<td class="center">' + value[select_colname[i]].substr(0, 30) + '...' + '</td>';
                                                        } else {
                                                                tmp += '<td class="center">' + value[select_colname[i]] + '</td>';
                                                        }
                                                }
                                        } else {
                                                tmp += '<td class="center"></td>';
                                        }
                                }

                                tmp +=  '<td class="child-inline center">';
                                
                                /*鉛筆icon 會員清單 mgm_management_account, 管理公告mgm_site_notice_manage, 管理訊息mgm_station_message */
                                if ($.global_php == 'json_mg_account.php' || $.global_php == 'json_site_notice_manage.php' || $.global_php == 'json_station_message.php'){
                                        tmp +=  '<button id="PencilModify" class="btn btn-xs btn-info blue-button" style="margin-right:5px;">' +
                                                '<i class="ace-icon fa fa-pencil bigger-120"></i>' +
                                                '</button>';
                                }
                                
                                
                                /*垃圾桶刪除icon 管理公告mgm_site_notice_manage, 管理訊息mgm_station_message */
                                if ($.global_php == 'json_site_notice_manage.php' || $.global_php == 'json_station_message.php'){
                                        tmp +=  '<button id="RubbishDelete" class="btn btn-xs btn-info blue-button" style="margin-right:5px;">' +
                                                '<i class="ace-icon fa fa-trash-o bigger-120"></i>' +
                                                '</button>';
                                }
                                
                                
                                if ($.global_state == 'a_state'){ //會員清單 mgm_management_account
                                        if (value[$.global_state] == 'block') {
                                                tmp += '<button id="display_block" class="btn btn-xs btn-danger green-button" style="margin-right:5px;">暫停會員</button>';
                                        } else if (value[$.global_state] == 'blockade') {
                                                tmp += '<button id="display_block" class="btn btn-xs btn-danger red-button" style="margin-right:5px;">已暫停會員</button>';
                                        }
                                } else if ($.global_state == "p_display"){ //文章管理 mgm_articles_list_manage
                                        if (value[$.global_state] == 'block') {
                                                tmp += '<button id="display_block" class="page_display btn btn-xs btn-danger green-button" style="margin-right:5px;">上架中</button>';
                                        } else if (value[$.global_state] == 'none') {
                                                tmp += '<button id="display_block" class="page_display btn btn-xs btn-danger red-button" style="margin-right:5px;">下架中</button>';
                                        }
                                } else if (value[$.global_state] == 'blockade') {
                                        
                                }
                                tmp += '</td>' + '</tr>';
                        });

                        //$( "#selece-search-table tbody" ).html( tmp );
                        $("#hiddenresult").html(tmp);

                        init_page_switch();


                } else {
                        $("#page-switch").html('');
                        $("#hiddenresult").html('');
                        $("#showresult").html('');
                        show_remind( "找不到符合搜尋字詞的會員" );
                }

        };
        var error_back = function(data) 
        {
                console.log(data);
        };
        $.Ajax("POST", "php/" + $.global_php + "?func=fn_btn_fast_list_search" , data , "" , success_back, error_back);


}