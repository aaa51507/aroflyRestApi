/**
 * @author: abin
 * @createDate: 2016/3/11
 * */
$(document).ready(function() {
    /* highcharts 顯示語言設定*/
    Highcharts.setOptions({
          lang: {
                months: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
                weekdays: ["星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
                shortMonths: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            }
    });
    mgm_membership_growth_statistics.HighStockChartSeriesEventFun = function(tag, e) {
        $.statistics_table(tag, e);
    }
    mgm_membership_growth_statistics.getCntAccountByYear(formatDate(new Date('2016/1/24')), formatDate(new Date('2016/3/10')));
});

var mgm_membership_growth_statistics = {
    /**
     * HighStockChart折線圖，點擊圖表點時，會呼叫的function
     * @param obj tag bindevent的tag
     * @param clickEvent clickEvent 點擊的event值
     */
    HighStockChartSeriesEventFun: function(tagEvent, clickEvent) {
        console.log("HighStockChartSeriesEventFun");
        console.log(tagEvent);
        console.log(clickEvent);
    },
    
    /**
     * 取得一年內每日的會員成長數目
     * @param string startData 查詢起始日 ex:2015-01-01
     * @param string endDate 查詢結束日 ex:2015-01-31
     */
    getCntAccountByYear: function(startData, endDate) {
        var createLine = function(data) {
            mgm_membership_growth_statistics.initHighStockChart('container2', data['seriesDate']);
            //mgm_membership_growth_statistics.initHighcharts('container', data['xAxis'], data['data']);
        }
        $.ajax({
                    type : "POST" ,
                    dataType: "json",
                    url : "php/mgm_membership_growth_statistics.php" ,
                    async: true ,
                    data : {
                        startDate: startData
                        ,endDate: endDate
                    } ,
                    success : createLine ,
                    error : function(e) {console.log(e)}
        });
    },

    /**
     * 建置Highcharts的折線圖
     * @param string tagID tag的id
     * @param array xAxis x軸的資料
     * ex:[1213,1235,2342]
     * @param array data y軸的資料
     * ex[1213,1235,2342]
     */
    initHighcharts: function(tagID, xAxis, data) {
        if($('#'+tagID).highcharts() != undefined) {
            $('#'+tagID).highcharts().destroy();
        }
        $('#'+tagID).html("");
        $('#'+tagID).highcharts({
            title: {
                text: 'a',
                style: {
                    visibility: 'hidden'
                }
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: xAxis
            },
            yAxis: {
                title: {
                    text: '人數',
                    rotation:360,
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }],
                labels: {
                    formatter: function () {
                        return Highcharts.numberFormat(this.value,0);
                    }
                }
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'center',
                verticalAlign: 'bottom',
                borderWidth: 0
            },
            series: [{
                name: '人數',
                data: data
            }],
            credits: {
                enabled: false
            },
        });
    },
    
    /**
     * 建置HighStockChart的折線圖
     * @params string tagID tag的id
     * @params array[array] data 折線圖的資料
     * @params function seriesEventFun 點擊資料點時,回傳資料給執行的function
     * ex:[[1236902400000,148],[1237161600000,359],[1237248000000,871],[1237334400000,954]]
     */
    initHighStockChart: function(tagID, data, seriesEventFun) {
        if($('#'+tagID).highcharts() != undefined) {
            $('#'+tagID).highcharts().destroy();
        }
        $('#'+tagID).html("");
        $('#'+tagID).highcharts('StockChart', {
            rangeSelector : {
                selected: 4,
                inputEnabled: false,
                buttonTheme: {
                    visibility: 'hidden'
                },
                labelStyle: {
                    visibility: 'hidden'
                }
            },

            title : {
                text : ''
            },
            yAxis: {
                allowDecimals: false,
            },
            series : [{
                name : '人數',
                data : data,
                marker : {
                    enabled : true,
                    radius : 3
                },
                shadow : true,
                point: {
                    events: {
                        click: function (e) {
                            var callBackFnc = mgm_membership_growth_statistics.HighStockChartSeriesEventFun;
                            if(typeof(callBackFnc) == "function") {
                                callBackFnc(this, e);
                            } else {
                                console.log("HighStockChartSeriesEventFun is not a function!!");
                            }
                        }
                    }
                }
            }],
            credits: {
                enabled: false
            }
        });
    },
};

/**
 * 日期轉字串 yyyy-MM-dd
 * @param date date 時間
 */
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
