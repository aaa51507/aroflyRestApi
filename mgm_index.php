<?php header("X-Frame-Options: DENY");?>
<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale = 1.0, user-scalable = 0" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="app-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="apple-touch-fullscreen" content="yes" />
        <link rel='shortcut icon' href='template/images/favicon.ico' type='x-icon'>
        <title>AROFLY | javascirpt API</title>
        <meta name="description" content="What you see what you get Enjoy to Interactive with living objects">

        <link class="icon" href="../icon/logo.png" rel="apple-touch-icon-precomposed" />
        <link class="icon" href="../icon/logo.png" rel="SHORTCUT ICON" />
        
        <link rel="stylesheet" href="css/all_css.css">
        <link rel="stylesheet" href="css/global.css">
        
        <link href="css/plugins/codemirror/codemirror.css" rel="stylesheet">
        <link href="css/plugins/codemirror/ambiance.css" rel="stylesheet">

        <!-- CodeMirror -->
        <script src="js/plugins/codemirror/codemirror.js"></script>
        <script src="js/plugins/codemirror/mode/javascript/javascript.js"></script>
        
        <?php include( "js/all_js.php"); ?>
        <!-- jquery.dataTables -->
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="js/dataTablesPlugin.js"></script>
        
        <style>
            input {
                border: 2px solid rgb(163, 177, 178);
                border-radius: 3px;
                color: rgb(102, 102, 102);
                display: inline-block;
                font-size: 12px;
                height: 30px;
                margin: 0 10px;
                padding: 0 5px;
            }
            .function_title {
                margin: 20px 0px 10px; 
                color: rgb(68, 68, 68); 
                font-weight: 600;
            }
            .list {
                width: 100%;
                overflow-x: scroll;
            }
            #datatable1 {
                width: 90%;
                text-align: left;
            }
        </style>
        <!-- edit by abin for restAPI-->
		 <script src="js/mgm_index/restAPI.js"></script>
		 <script>
			$( document ).ready(function() {
				var getUrlData = function() {
					return window.location.href.split("#")[1];
				}
				
				var cleanCallBack = function() {
					$("#sendCallBack").val('');
					$("#sendStatus").val('');
					$("#ajaxExample").val('');
					if($("#example")[0] != undefined) {
						$("#example").DataTable().destroy();
					}
					$("#datatable").html("");
				}
				
				var initView = function(selectPageEvent) {
					var selectPage = $("#selectPage");
					selectPage.html("");
					var apiData = $.RESTfulAPI.conf;
					$.each(apiData, function(key, val) {
						selectPage.append(
							'<option value="'+ key +'">'+ key +'</option>'
						);
					});
					selectPage.change(selectPageEvent);
				}
				
				var initDataTable = function(column, data) {
					if($("#example")[0] != undefined) {
						$("#example").DataTable().destroy();
					}
					$("#datatable").html("");
					$("#datatable").append('<table id="example" class="display" width="100%"></table>');
					$('#example').DataTable({
						data: data['data'],
						columns: column
					});
				}
				
				var testSendEvent = function(e) {
					cleanCallBack();
					var data = {};
					try {
						data = JSON.parse($("#testData").val());
					}
					catch(e) {
						$("#sendCallBack").val("send error: " + e.message);
						return;
					}
					$.ajax({
						type: $("#testMethod").val(),
						dataType: "text",
						url: "slim2"+$("#testUrl").val(),
						data: data,
						success: function(msg, status){
							$("#sendCallBack").val(msg);
							if(status == "success") {
								$("#sendStatus").val(200);
							}
							if($("#testUrl").val().indexOf("DataTable") != -1) {
								var data = {};
								try {
									data = JSON.parse(msg);
									var column = [];
									for(var i=0; i<data['data'][0].length; i++) {
										column.push({title: "a"+i});
									}
									initDataTable(column, data);
								}
								catch(e) {
									$("#sendCallBack").val("initDataTable error: " + e.message);
									return;
								}
								console.log(data);
							}
						},
						error:function(xhr, ajaxOptions, thrownError){ 
							$("#sendStatus").val(xhr.status);
							$("#sendCallBack").val(thrownError);
						}
					});
					var ajaxS = 'var data = '
							+ $("#testData").val() +
							';\n\ var success_back = function(data) {\n\
							};\n\
							var error_back = function(data) {\n\
									console.log(data);\n\
							};\n\
							$.Ajaxq( "page_queue" , "'+ $("#testMethod").val() +'" , "'+ "slim2"+$("#testUrl").val() +'" , data, "", success_back, error_back);';
					$("#ajaxExample").val(ajaxS);
				}
				
				var testButtonEvent = function(e) {
					var data = $(this).data("conf");
					//清除callBack資料
					cleanCallBack();
					
					$("#testData").val(JSON.stringify(data['data']));
					$("#testDesc").html(data['desc']);
					$("#testMethod").val(data['type']);
					$("#testUrl").val(data['url']);
				}
					
				var selectPageEvent = function(e) {
					var testingButton = $("#testingButton");
					testingButton.html("");
					$("#testUrl").val('');
					$("#testData").val('');
					cleanCallBack();
					
					var key = $(this).val();
					var apiData = $.RESTfulAPI.conf[key];
					var button = '';
					for(var i=0; i<apiData.length; i++) {
						for(var j=0; j<apiData[i]['test'].length; j++) {
							var test = apiData[i]['test'][j];
							button = '<input class="button" value="'+ test['buttonTitle'] +'" type="button">';
							button = $(button).appendTo(testingButton);
							button.data( "conf",  {
								type: apiData[i]['type']
								,url: apiData[i]['url']
								,data: test['data']
								,desc: test['desc']
							});
							button.click(testButtonEvent);
						}
					}
				}
				
				initView(selectPageEvent);
				cleanCallBack();
				if(getUrlData() != undefined) {
					$("#selectPage").val(getUrlData());
				}
				$("#selectPage").trigger("change");
				$("#testSend").click(testSendEvent);
			});
		 </script>
</head>

<body>
        <div id="all">

                <?php include( "html/loading.php"); ?>
                <?php include( "html/header.php"); ?>

                <div class="content">
                        <?php include( "html/sidebar_setting.php"); ?>

                        <div class="main-content container">                        
                                <div class="path">
                                        <a href="#">Console</a> > <a href="#">javascirpt API</a>
                                </div>

                                <div class="list">

                                        <h2>Everything about your Pets</h2>
                                        <p>
                                            Login User Token <input type="text" placeholder="" id="user_token" style="width: 180px;" >
                                        </p>
                                        <h3 class="function_title">controllers
                                                <select id="selectPage" style="margin: 0px 10px; padding: 0px 10px; width: auto;">
                                                        <option>GET</option>
                                                        <option>POST</option>
                                                </select>
                                        </h3>
                                        <p>
                                                <span>method<select id="testMethod" style="margin:0 10px;" disabled>
                                                        <option value="GET">GET</option>
                                                        <option value="POST">POST</option>
                                                        <option value="PUT">PUT</option>
                                                        <option value="DELETE">DELETE</option>
                                                </select>
                                                </span>
                                                PHPurl
                                                <input id="testUrl"  placeholder="http://..." value="http://www.oort.com.tw/scs/php/public/test/testDataTable" style="width: 60%;" type="text">
						<input id="testSend" class="button" value="送出" type="button">
                                        </p>

                                        
                                        
                                        <table class="display select dataTable" id="datatable1">
                                                <thead>
                                                        <tr>
                                                                <th style="width:50%;">Input json data</th>
                                                                <th>Description</th>
                                                        </tr>
                                                </thead>
                                                <tbody>
                                                        <tr class="odd child-middle">
                                                                <td>
                                                                        <!--textarea id="code1"-->
																		<textarea id="testData">
                                                                        </textarea>
                                                                </td>
                                                                <td id="testDesc">測試填入錯誤帳號</td>
                                                        </tr>
                                                        <tr>
                                                                <td colspan="2" id="testingButton">
                                                                        <input class="button" value="測試填入錯誤帳號" id="category_add" type="button">
                                                                        <input class="button" value="測試無填入帳號" id="category_add" type="button">
                                                                        <input class="button" value="測試填入錯誤密碼" id="category_add" type="button">
                                                                        <input class="button" value="測試無填入密碼" id="category_add" type="button">
                                                                        <input class="button" value="Try it out!" id="category_add" type="button" style="float: right;margin-right: 70px; " >
                                                                </td>
                                                        </tr>
                                                        <tr class="odd child-middle">
                                                                <td colspan="2"> 
                                                                        <p>Response<input id="sendStatus" type="text"></p>
                                                                </td>
                                                        </tr>
                                                        <tr class="odd child-middle">
                                                                <td colspan="2"> 
                                                                        <p>CallBack</p>
                                                                        <textarea id="sendCallBack">
                                                                        </textarea>
                                                                </td>
                                                        </tr>
                                                        <tr class="odd child-middle">
                                                                <td colspan="2"> 
                                                                        <p> Ajax</p>
                                                                        <textarea id="ajaxExample">
                                                                        </textarea>
                                                                </td>
                                                        </tr>
                                                        <!--tr class="odd child-middle">
                                                                <td colspan="2"> 
                                                                        <p>Incloud Function</p>
                                                                        <textarea id="code3" >

< jquery.dataTables >
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="js/dataTablesPlugin.js"></script>

                                                                        </textarea>
                                                                </td>
                                                        </tr-->
                                                        <tr class="child-middle">
                                                                <td colspan="2"> 
                                                                        <p>Response UI - Datatable</p>
																		<div id="datatable" style="width: 100%;">
                                                                        </div>
                                                                </td>
                                                        </tr>
                                                </tbody>
                                        </table>
                                </div>
                        </div>

                </div>

                <?php include( "html/footer.php"); ?>

        </div>
    


    <script>
         $(document).ready(function(){

             /*var editor_one = CodeMirror.fromTextArea(document.getElementById("code1"), {
                 lineNumbers: true,
                 matchBrackets: true,
                 styleActiveLine: true,
                 theme:"ambiance"
             });*/

             var editor_two = CodeMirror.fromTextArea(document.getElementById("code1"), {
                 lineNumbers: true,
                 matchBrackets: true,
                 styleActiveLine: true
             });
             var editor_two = CodeMirror.fromTextArea(document.getElementById("code2"), {
                 lineNumbers: true,
                 matchBrackets: true,
                 styleActiveLine: true
             });
             var editor_two = CodeMirror.fromTextArea(document.getElementById("code3"), {
                 lineNumbers: true,
                 matchBrackets: true,
                 styleActiveLine: true
             });
             var editor_two = CodeMirror.fromTextArea(document.getElementById("code4"), {
                 lineNumbers: true,
                 matchBrackets: true,
                 styleActiveLine: true
             });
             var editor_two = CodeMirror.fromTextArea(document.getElementById("code5"), {
                 lineNumbers: true,
                 matchBrackets: true,
                 styleActiveLine: true
             });

        });
            
        function init() {
                loading_ajax_hide();
                show_remind( "已登入，填入 User Token" , "success"  );
                $( "#user_token" ).val( getCookie( "scs_cookie" ) )
        };
        
        function unconnected_callback() {
                loading_ajax_hide();
                show_remind( "請先登入" , "error"  );
        };
        function connected_callback( member ) {
                if( member.a_admin !== "true" ){
                        loading_ajax_hide();
                        show_remind( "不是管理者，三秒後轉跳到首頁。" , "error" );
                        setTimeout( function(){ location.href = "../index.php" }, 3000);
                }
                else{
                        init();
                }
        };
        
        </script>
</body>
</html>
