<?php header("X-Frame-Options: DENY");?>
<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale = 1.0, user-scalable = 0" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="app-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="apple-touch-fullscreen" content="yes" />
        <link rel='shortcut icon' href='template/images/favicon.ico' type='x-icon'>
        <title>影片管理 | healing_fruits</title>
        <meta name="description" content="What you see what you get Enjoy to Interactive with living objects">

        <link class="icon" href="../icon/logo.png" rel="apple-touch-icon-precomposed" />
        <link class="icon" href="../icon/logo.png" rel="SHORTCUT ICON" />
        
        <link rel="stylesheet" href="css/all_css.css">
        <link rel="stylesheet" href="css/global.css">
        
        <?php include( "js/all_js.php"); ?>
        <script src="js/jquery.pagination.js"></script>
        <script src="js/arod/management_account.jquery.pagination.js"></script>
        <script>
                $.global_date_statistics_column = "vip"; //table:date_statistics > col:vip
        </script>
        <!--script src="js/search.js"></script-->
        <script src="js/batch.js"></script>

        <!-- include HIGHCHARTS-->
        <script src="js/highstock.js"></script>
        <script src="js/mgm_highchart_jack.js"></script>
        <script src="js/mgm_hichart_click_jack.js"></script>
        <!-- for arod edit function (點擊折線圖的點會呼叫的fun)-->

        <!-- datePicker -->
        <script src="js/jquery-ui.js"></script>

        <!-- jquery.dataTables -->
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="js/dataTablesPlugin.js"></script>

        <script>

                $(document).ready(function() {
                });

                loading_ajax_show();

                function init() {
                            
                            $("#condition_Datatable_starttime").datepicker();
                            $("#condition_Datatable_starttime").datepicker( "setDate", "01/01/" + ( new Date().getFullYear() ) );

                            $("#condition_Datatable_endtime").datepicker();
                            $("#condition_Datatable_endtime").datepicker( "setDate", ( new Date().getMonth() + 1 ) + "/" + ( new Date().getDate() ) + "/" + ( new Date().getFullYear() ) );

                            $( "#DL_mainCate" ).html( "" );
                            $( "#SD_mainCate" ).html( "" );
                    
                            var data = {
                            };
                            var success_back = function(data) {

                                        console.log(data);
                                        var tmp = "";
                                        var i;
                                        data = JSON.parse(data);
                                        console.log(data);
                                        if (data.success) {
                                            
                                                // show_remind( "讀取成功" , "success" );
                                                var tmp_html = "";
                                                tmp_html += '<option value="">全部</option>';
                                                $.each(data.data, function(index, value) {
                                                        tmp_html += '<option value="' + value[0] + '">' + value[1] + '</option>';
                                                });
                                                $("#DL_mainCate").html(tmp_html);
                                                $("#SD_mainCate").html(tmp_html);

                                                $("#DL_mainCate").unbind('change').bind('change', function() {

                                                            load_read_category_subCate( $("#DL_subCate") , $(this).val() );
                                                });
                                                $("#DL_mainCate").trigger('change');
                                                
                                                $("#SD_mainCate").unbind('change').bind('change', function() {

                                                            load_read_category_subCate( $("#SD_subCate") , $(this).val() );
                                                });
                                                $("#SD_mainCate").trigger('change');

                                        } else {
                                                show_remind( data.msg , "error" );
                                        }

                            };
                            var error_back = function(data) {
                                        console.log(data);
                            };
                            $.Ajaxq( "page_queue" , "POST" , "/hf/backendui/php/json_mgm_video.php?func=fn_read_category_mainCate", data, "", success_back, error_back);

                            // --------------------------------------------------------------------------------------------------

                            $("#btn_Datatable").unbind('click').bind('click', function() {

                                        $.initDatatable_1();
                            });
                            
                            $("#DL_p_id-setting").unbind('click').bind('click', function() {

                                        
                                        $( "#DL_p_id-video" ).show();
                                        $( "#DL_p_id-video iframe" )[0].src = "https://www.youtube.com/embed/" + $( "#DL_p_id" ).val() ;
                                        
                                        // http://stackoverflow.com/questions/2068344/how-do-i-get-a-youtube-video-thumbnail-from-the-youtube-api
                                        $( "#DL_p_id-video img" )[0].src = "http://img.youtube.com/vi/" + $( "#DL_p_id" ).val() + "/maxresdefault.jpg" ;
                                        
                            });
                            
                            $("#btn_checkbox_bonus_show").unbind('click').bind('click', function() {

                                        $( "#myModalVideo" ).modal( "show" );
                                        
                                        $("#DL_p_id").val( "" );
                                        $("#DL_p_title").val( "" );
                                        $("#DL_mainCate").val( "" );
                                        $("#DL_subCate").val( "" );
                                        $("#DL_p_pre_chn").val( "" );
                                        $("#DL_p_pre_eng").val( "" );
                                        $("#DL_p_back_chn").val( "" );
                                        $("#DL_p_back_eng").val( "" );

                                        $( "#DL_model_title" ).html( "新增影片" );

                                        
                            });
                            
                            $("#DL_save").unbind('click').bind('click', function() {

                                        $( "#myModalVideo" ).modal( "hide" );
                                        
                                        var data = {
                                                    //token:      $.th_cookie
                                                    p_id        : $("#DL_p_id").val()       ,
                                                    p_title     : $("#DL_p_title").val()    ,
                                                    mainCate    : $("#DL_mainCate").val()   ,
                                                    subCate     : $("#DL_subCate").val()    ,
                                                    p_pre_chn   : $("#DL_p_pre_chn").val()  ,
                                                    p_pre_eng   : $("#DL_p_pre_eng").val()  ,
                                                    p_back_chn  : $("#DL_p_back_chn").val() ,
                                                    p_back_eng  : $("#DL_p_back_eng").val()
                                        };
                                        var success_back = function(data) {

                                                    console.log(data);
                                                    var tmp = "";
                                                    var tmp_search = "";
                                                    var i;
                                                    data = JSON.parse(data);
                                                    console.log(data);
                                                    if (data.success) {

                                                            show_remind( "儲存成功" , "success" );

                                                    } else {
                                                            show_remind( data.msg , "error" );
                                                    }

                                        };
                                        var error_back = function(data) {
                                                    console.log(data);
                                        };
                                        $.Ajax("POST", "/hf/backendui/php/json_mgm_video.php?func=fn_create_update_video", data, "", success_back, error_back);

                                        
                            });
                            
                            $("#DL_cancel").unbind('click').bind('click', function() {
                                
                                        $( "#myModalVideo" ).modal( "hide" );
                                        
                                        $( "#DL_p_id-video" ).hide();
                                        $( "#DL_p_id-video iframe" )[0].src = "" ;
                                        $( "#DL_p_id-video img" )[0].src = "" ;
                            });
                            
                            $('#myModalVideo').on('hidden.bs.modal', function () {
                                
                                        $("#DL_p_id").val( "" );
                                        $("#DL_p_title").val( "" );
                                        $("#DL_mainCate").val( "" );
                                        $("#DL_subCate").val( "" );
                                        $("#DL_p_pre_chn").val( "" );
                                        $("#DL_p_pre_eng").val( "" );
                                        $("#DL_p_back_chn").val( "" );
                                        $("#DL_p_back_eng").val( "" );
                                        
                                        $( "#DL_p_id-video" ).hide();
                                        $( "#DL_p_id-video iframe" )[0].src = "" ;
                                        $( "#DL_p_id-video img" )[0].src = "" ;
                                        
                            });
                            
                }

                $.initDatatable_1 = function initDatatable_1() {

                        //destory dataTable
                        $("#example").DataTable().destroy();
                        $("#datatable").html("");

                        var column = [ "影片標題" , "分類" , "子分類" , "點閱數" , "分享數" , "發佈時間" , "最近推播時間" , "推播次數" , "操作" ];

                        

                        createSearchTable( '#search_Datatable' , '#example', column );

                                var ajax = '/hf/backendui/php/json_mgm_video.php?func=fn_read_regex&' + 
                                                        'startTime=' + $("#condition_Datatable_starttime").datepicker( "getDate" ).getTime() + '&' +
                                                        'endTime=' + $("#condition_Datatable_endtime").datepicker( "getDate" ).getTime() + '&' +
                                                        'mainCate=' + $( "#SD_mainCate" ).val() + '&' +
                                                        'subCate=' + $( "#SD_subCate" ).val() + '&' +
                                                        'title=' + $( "#SD_title" ).val() + '&' +
                                                        'display=' + $( "#SD_display" ).val() + '&' +
                                                'operation_html_blockade=' +
                                                    '<a id="json_mgm_video_block" target-state="blockade" class="mark msg_box">下架中</a> <a id="json_mgm_video_notification" class="mark msg_box">推播</a> <a id="json_mgm_video_edit" class="mark msg_box">編輯</a><a id="json_mgm_video_delete" class="mark msg_box">刪除</a>' + '&' +
                                                'operation_html_block=' +
                                                    '<a id="json_mgm_video_block" target-state="block" class="mark msg_box">上架中</a> <a id="json_mgm_video_notification" class="mark msg_box">推播</a>  <a id="json_mgm_video_edit" class="mark msg_box">編輯</a> <a id="json_mgm_video_delete" class="mark msg_box">刪除</a>' ;
                                            

                            var order = [
                        		[1, 'desc']
                            ];

                            var tmp_lengthMenu = [[10, 25, 50, -2], [10, 25, 50, "All"]]  ;

                            createTable('#datatable', '#example', column);
                            createDataTable( '#example', ajax , order , tmp_lengthMenu );
                            $('#example').data("rows_selected", []);
                            addEvent('#datatable', '#example');

                            $('#example tbody').on('click', '[id=json_mgm_video_block]', function(e) {
                                    var $row = $(this).closest('tr');
                                    // Get row data
                                    var data = $('#example').DataTable().row($row).data();
                                    // Get row ID
                                    //fn_list_management_account_single_dialogue();
                                    //.api().ajax.url('newurl.php').load();
                                    console.log( data );
                                    
                                    if( $(this).attr( "target-state" ) == "block" )
                                    {
                                                $(this).html( "下架中" );
                                                $(this).attr( "target-state" , "blockade" );
                                    }else{
                                                $(this).html( "上架中" );
                                                $(this).attr( "target-state" , "block" );
                                    }

                                    var data = {
                                                page_id     : data[0] ,
                                                display     : $(this).attr( "target-state" ) ,
                                    };
                                    var success_back = function(data) {

                                                console.log(data);
                                                var tmp = "";
                                                var tmp_search = "";
                                                var i;
                                                data = JSON.parse(data);
                                                console.log(data);
                                                if (data.success) {

                                                        show_remind( "更改成功" , "success" );

                                                } else {
                                                        show_remind( data.msg , "error" );
                                                }

                                    };
                                    var error_back = function(data) {
                                                console.log(data);
                                    };
                                    $.Ajax("POST", "/hf/backendui/php/json_mgm_video.php?func=fn_update_page_block_blockade" , data, "", success_back, error_back);

                            });
                            
                            
                            $('#example tbody').on('click', '[id=json_mgm_video_edit]', function(e) {
                                
                                        var $row = $(this).closest('tr');
                                        // Get row data
                                        var data = $('#example').DataTable().row($row).data();
                                        // Get row ID
                                        //fn_list_management_account_single_dialogue();
                                        //.api().ajax.url('newurl.php').load();
                                        console.log( data );
                                        //$.initDatatable_2( data[0] );

                                        
                                        var data = {
                                                    page_id        : data[0] ,
                                        };
                                        var success_back = function(data) {

                                                    console.log(data);
                                                    var tmp = "";
                                                    var tmp_search = "";
                                                    var i;
                                                    data = JSON.parse(data);
                                                    console.log(data);
                                                    if (data.success) {

                                                            $( "#myModalVideo" ).modal( "show" );
                                                            // show_remind( "讀取成功" , "success" );
                                                            
                                                            $("#DL_p_id").val( data.data.p_id );
                                                            $("#DL_p_title").val( data.data.p_title );
                                                            $("#DL_mainCate").val( data.data.p_main_category_id );
                                                            // $("#DL_subCate").val( data.data.p_sub_category_id );
                                                            load_read_category_subCate( $("#DL_subCate") , data.data.p_main_category_id , data.data.p_sub_category_id );
                                                            
                                                            $("#DL_p_pre_chn").val( data.data.p_pre_chn );
                                                            $("#DL_p_pre_eng").val( data.data.p_pre_eng );
                                                            $("#DL_p_back_chn").val( data.data.p_back_chn );
                                                            $("#DL_p_back_eng").val( data.data.p_back_eng );



                                                            $( "#DL_model_title" ).html( $("#DL_p_title").val() );
                                                            
                                                            $( "#DL_p_id-video" ).show();
                                                            $( "#DL_p_id-video iframe" )[0].src = "https://www.youtube.com/embed/" + $( "#DL_p_id" ).val() ;
                                                            $( "#DL_p_id-video img" )[0].src = "http://img.youtube.com/vi/" + $( "#DL_p_id" ).val() + "/maxresdefault.jpg" ;

                                                            

                                                    } else {
                                                            show_remind( data.msg , "error" );
                                                    }

                                        };
                                        var error_back = function(data) {
                                                    console.log(data);
                                        };
                                        $.Ajax("POST", "/hf/backendui/php/json_mgm_video.php?func=fn_read_singleLog", data, "", success_back, error_back);

                                    

                            });
                            
                            
                            $('#example tbody').on('click', '[id=json_mgm_video_notification]', function(e) {
                                    var $row = $(this).closest('tr');
                                    // Get row data
                                    var data = $('#example').DataTable().row($row).data();
                                    // Get row ID
                                    //fn_list_management_account_single_dialogue();
                                    //.api().ajax.url('newurl.php').load();
                                    console.log( data );
                                    
                                    $("#btn_notification_yes").data( "focus" , data[0] );
                                    $( "#myModalnotification" ).modal( "show" );
                                    
                                    $("#btn_notification_yes").unbind('click').bind('click', function() {
                                                loading_ajax_show();
                                                var data = {
                                                        token        : getCookie("scs_cookie") ,
                                                        title        : $("#input_noti_title").val() ,
                                                        message      : $("#input_noti_message").val() ,
                                                        page_id      : $("#btn_notification_yes").data( "focus" )
                                                };
                                                var success_back = function(data) {

                                                            console.log(data);
                                                            var tmp = "";
                                                            data = JSON.parse(data);
                                                            console.log(data);
                                                            if (data.success) {
                                                                    loading_ajax_hide();
                                                                    show_remind( "推播成功" , "success" );
                                                                    $.initDatatable_1();

                                                            } else {
                                                                    show_remind( data.msg , "error" );
                                                            }

                                                };
                                                var error_back = function(data) {
                                                            console.log(data);
                                                };
                                                $.Ajax("POST", "/hf/backendui/php/json_mgm_video.php?func=fn_notification", data, "", success_back, error_back);

                                    });
                            });
                            
                            $('#example tbody').on('click', '[id=json_mgm_video_delete]', function(e) {
                                    var $row = $(this).closest('tr');
                                    // Get row data
                                    var data = $('#example').DataTable().row($row).data();
                                    // Get row ID
                                    //fn_list_management_account_single_dialogue();
                                    //.api().ajax.url('newurl.php').load();
                                    console.log( data );
                                    
                                    var $row = $(this).closest('tr');
                                    // Get row data
                                    var data = $('#example').DataTable().row($row).data();
                                    // Get row ID
                                    //fn_list_management_account_single_dialogue();
                                    //.api().ajax.url('newurl.php').load();
                                    console.log( data );
                                    //$.initDatatable_2( data[0] );

                                    $("#myModaldelete_yes").data( "focus" , data[0] );

                                    $( "#myModaldelete" ).modal( "show" );

                                    $("#myModaldelete_yes").unbind('click').bind('click', function() {

                                                var data = {
                                                            page_id        : $("#myModaldelete_yes").data( "focus" ) ,
                                                };
                                                var success_back = function(data) {

                                                            console.log(data);
                                                            var tmp = "";
                                                            var tmp_search = "";
                                                            var i;
                                                            data = JSON.parse(data);
                                                            console.log(data);
                                                            if (data.success) {

                                                                    show_remind( "刪除成功" , "success" );
                                                                    $.initDatatable_1();


                                                            } else {
                                                                    show_remind( data.msg , "error" );
                                                            }

                                                };
                                                var error_back = function(data) {
                                                            console.log(data);
                                                };
                                                $.Ajax("POST", "/hf/backendui/php/json_mgm_video.php?func=fn_delete", data, "", success_back, error_back);

                                    });


                            });

                    }
                
                function load_read_category_subCate( tmp_sub_focus , tmp_value , tmp_sub_value )
                {
                    
                            tmp_sub_focus.html( "" );
                    
                            var data = {
                                        mainCate : tmp_value
                            };
                            var success_back = function(data) {

                                        console.log(data);
                                        var tmp = "";
                                        var tmp_search = "";
                                        var i;
                                        data = JSON.parse(data);
                                        console.log(data);
                                        if (data.success) 
                                        {
                                            
                                                // show_remind( "讀取成功" , "success" );
                                                var tmp_html = "";
                                                tmp_html += '<option value="">全部</option>';
                                                $.each(data.data, function(index, value) {
                                                        tmp_html += '<option value="' + value[0] + '">' + value[1] + '</option>';
                                                });
                                                tmp_sub_focus.html(tmp_html);
                                                
                                                if( tmp_sub_value )
                                                tmp_sub_focus.val( tmp_sub_value );


                                                //if( $("#datatable").html() == "" )
                                                //$.initDatatable_1();

                                        } else {
                                                show_remind( data.msg , "error" );
                                        }

                            };
                            var error_back = function(data) {
                                        console.log(data);
                            };
                            $.Ajaxq( "page_queue" , "POST" , "/hf/backendui/php/json_mgm_video.php?func=fn_read_category_subCate", data, "", success_back, error_back);

                }
        </script>
</head>

<body>
        <div id="all">

                <?php include( "html/loading.php"); ?>
                <?php include( "html/header.php"); ?>

                <div class="content">
                        <?php include( "html/sidebar_setting.php"); ?>

                        <div class="main-content container">                        
                                <div class="path">
                                        <a href="#">控制後台</a> > <a href="#">影片管理</a>
                                </div>

                                <div class="list">

                                        <h2>影片管理</h2>

                                        <!-- search box -->
                                        <div class="dataTable_search_box">
                                                <div id="condition_Datatable">
                                                        <table>
                                                                <tr class="condition_4" data-column="2">
                                                                        <td align="center">
                                                                                開始日期:
                                                                                <input type="text" placeholder="開始時間" class="lbox datepicker" id="condition_Datatable_starttime">
                                                                        </td>
                                                                        <td align="center">
                                                                                結束日期:
                                                                                <input type="text" placeholder="開始時間" class="lbox datepicker" id="condition_Datatable_endtime">
                                                                        </td>
                                                                        <td class="center">主分類:
                                                                                <select style=" width: 50%;" id="SD_mainCate">
                                                                                        <option value=""></option>
                                                                                </select>
                                                                        </td>
                                                                        <td class="center">子分類:
                                                                                <select style=" width: 50%;" id="SD_subCate">
                                                                                        <option value=""></option>
                                                                                </select>
                                                                        </td>
                                                                </tr>
                                                                <tr class="condition_4" data-column="2">
                                                                        <td align="center">
                                                                                標題:<input type="text" id="SD_title" class="column_filter hasDatepicker">
                                                                        </td>
                                                                        <td class="center">狀態:
                                                                                <select style=" width: 50%;" id="SD_display">
                                                                                        <option value="">全部</option>
                                                                                        <option value="block">上架中</option>
                                                                                        <option value="blockade">下架中</option>
                                                                                </select>
                                                                        </td>
                                                                </tr>
                                                        </table>
                                                </div>

                                                <button id="btn_Datatable" class="submit">查詢</button>
                                        </div>
                                        <!-- function btn box -->
                                        <div class="dataTable_function_btn_box">
                                                <div class="float-left">操作結果訊息顯示</div>
                                                <button onclick="$('#myModalVideo').modal('show');" type="button" class="float-right" id="btn_checkbox_bonus_show">+新增影片</button><!--data-target="" data-toggle="modal"-->
                                        </div>

                                        <div id="datatable"></div>
                                </div>
                        </div>

                </div>

                <?php include( "html/footer.php"); ?>

        </div>
    
        <!--add-->
        <div class="modal fade" id="myModalVideo" aria-hidden="false">
                <div class="modal-dialog">
                        <div id="dialog-msg">
                                <dl>
                                        <div class="list">
                                                <h2 id="DL_model_title" ></h2>
                                                <form role="form">
                                                        <ul>

                                                                <li>
                                                                        <span>影片ID </span>
                                                                        <input type="text" data-input="title" id="DL_p_id" placeholder="標題" value="dvKDq3uGA7o" style="width: 48%;" >
                                                                        <input type="button" id="DL_p_id-setting" class="button" value="設定" style="width: 10%; height: 32px; margin-left: 1%;">
                                                                </li>
                                                                <div class="video" id="DL_p_id-video" style="display: none;" >
                                                                        <iframe width="560" height="315" src="" frameborder="0" allowfullscreen></iframe>
                                                                        <img style="width: 100%;" >
                                                                </div>
                                                                <!--li class="center">
                                                                        <nav>
                                                                                <img style="background-image: url('http://www.oort.com.tw/funbook19/data/adsense/5/icon2.jpg');width: 34%;" src="http://www.oort.com.tw/funbook19/data/adsense/5/icon2.jpg" class="remittances-img" id="pair_photo">
                                                                                <a class="zoom" href="http://www.oort.com.tw/funbook19/data/adsense/5/icon2.jpg" target="_blank"></a>
                                                                        </nav>
                                                                        <a class="button" href="#" data-dismiss="modal" id="btn_update_board">上傳圖片</a>
                                                                        <a class="button delet" href="#" data-dismiss="modal">清除圖片</a>
                                                                </li-->
                                                                <li>
                                                                        <span>影片標題 </span>
                                                                        <input type="text" value="" placeholder="標題" id="DL_p_title" data-input="title">
                                                                </li>
                                                                <li>
                                                                        <span>影片主類別 </span>
                                                                        <select id="DL_mainCate" >
                                                                        </select>
                                                                </li>
                                                                <li>
                                                                        <span>影片次類別 </span>
                                                                        <select id="DL_subCate" >
                                                                        </select>
                                                                </li>
                                                                <li>
                                                                        <span>前標</span>
                                                                        <textarea style="margin-left: 30%; width: 60%;" placeholder="內容" class="form-control limited" id="DL_p_pre_chn" ></textarea>

                                                                </li>
                                                                <li>
                                                                        <span>前標英文</span>
                                                                        <textarea style="margin-left: 30%; width: 60%;" placeholder="內容" class="form-control limited" id="DL_p_pre_eng" ></textarea>

                                                                </li>
                                                                <li>
                                                                        <span>後標</span>
                                                                        <textarea style="margin-left: 30%; width: 60%;" placeholder="內容" class="form-control limited" id="DL_p_back_chn" ></textarea>

                                                                </li>
                                                                <li>
                                                                        <span>後標英文</span>
                                                                        <textarea style="margin-left: 30%; width: 60%;" placeholder="內容" class="form-control limited" id="DL_p_back_eng" ></textarea>

                                                                </li>
                                                                <li class="center">
                                                                        <a class="button" id="DL_save" >確定</a>
                                                                        <a class="button delete" id="DL_cancel" >取消</a>
                                                                </li>
                                                        </ul>
                                                </form>
                                        </div>
                                </dl>
                        </div>
                </div>
        </div>

        <!--delete-->
        <div class="modal fade" id="myModaldelete" aria-hidden="false">
                <div class="modal-dialog">
                        <div id="dialog-msg">
                                <dl>
                                        <div class="list">
                                                <h2>刪除影片</h2>
                                                <p class="words"><strong>注意：刪除就不能復原了</strong></p>
                                                <form method="get">
                                                        <nav>
                                                                <a data-dismiss="modal" id="myModaldelete_yes" class="button" type="button">確定</a>
                                                                <a data-dismiss="modal" class="button delete" type="button">取消</a>
                                                        </nav>
                                                </form>
                                        </div>
                                </dl>
                        </div>
        </div>
        </div>

        <!--edit-->
        <div class="modal fade" id="myModaledit" aria-hidden="false">
                <div class="modal-dialog">
                        <div id="dialog-msg">
                                <dl>
                                        <div class="list">
                                                <h2>編輯影片</h2>
                                                <form role="form">
                                                        <ul>

                                                                <div class="video">
                                                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/nuHNji1AzY4" frameborder="0" allowfullscreen></iframe>
                                                                </div>
                                                                <li>
                                                                        <span>影片ID </span>
                                                                        <input type="text" data-input="title" id="UpdateBoardTitle" placeholder="標題" value="" style="width: 48%;">
                                                                        <input type="button" id="btn-setting1" class="button" value="設定" style="width: 10%; height: 32px; margin-left: 1%;">
                                                                </li>
                                                                <li class="center">
                                                                        <nav>
                                                                                <img style="background-image: url('http://www.oort.com.tw/funbook19/data/adsense/5/icon2.jpg');width: 34%;" src="http://www.oort.com.tw/funbook19/data/adsense/5/icon2.jpg" class="remittances-img" id="pair_photo">
                                                                                <a class="zoom" href="http://www.oort.com.tw/funbook19/data/adsense/5/icon2.jpg" target="_blank"></a> 
                                                                        </nav>
                                                                        <a class="button" href="#" data-dismiss="modal" id="btn_update_board">上傳圖片</a>
                                                                        <a class="button delete" href="#" data-dismiss="modal">清除圖片</a>
                                                                </li>
                                                                <li>
                                                                        <span>發表時間 </span>
                                                                        <input type="text" value="" placeholder="標題" id="UpdateBoardTitle" data-input="title">
                                                                </li>
                                                                <li>
                                                                        <span>影片標題 </span>
                                                                        <input type="text" value="" placeholder="標題" id="UpdateBoardTitle" data-input="title">
                                                                </li>
                                                                <li>
                                                                        <span>影片類別 </span>
                                                                        <select>
                                                                                <option>工作</option>
                                                                        </select>
                                                                </li>
                                                                <li>
                                                                        <span>內容 </span>
                                                                        <textarea style="margin-left: 30%; width: 60%;" placeholder="內容" class="form-control limited" id="UpdateBoardContent"></textarea>

                                                                </li>
                                                                <li class="center">
                                                                        <a class="button" href="#" data-dismiss="modal" id="btn_update_board">儲存</a>
                                                                        <a class="button delete" href="#" data-dismiss="modal">取消</a>
                                                                </li>
                                                        </ul>
                                                </form>
                                        </div>
                                </dl>
                        </div>
                </div>
        </div>

        <!--notification-->
        <div class="modal fade" id="myModalnotification" aria-hidden="false">
                <div class="modal-dialog">
                        <div id="dialog-msg">
                                <dl>
                                        <div class="list">
                                                <h2>推播</h2>
                                                <form role="form">
                                                        <ul>
                                                                <li>
                                                                        <div style="vertical-align: middle; display: inline-block; width: 25%;">
                                                                                <div style="background-image: url('../icon/logo.png');height: 100px;width: 100px;background-size: 100% 100%;"></div>
                                                                        </div>
                                                                        <div style="vertical-align: middle; width: 70%; display: inline-block;">
                                                                                <input data-input="title" id="input_noti_title" placeholder="人生吶療癒果實APP" value="人生吶療癒果實APP" style="box-sizing: border-box; display: inline; margin: 0px; width: 100%;" disabled="" type="text">
                                                                                <input data-input="title" id="input_noti_message" placeholder="輸入推播內容" value="" style="box-sizing: border-box; display: inline; width: 100%; margin: 10px 0px 0px;" type="text">
                                                                        </div>
                                                                </li>
                                                                <li class="center">
                                                                        <a class="button" href="#" data-dismiss="modal" id="btn_notification_yes">傳送</a>
                                                                        <a class="button delete" href="#" data-dismiss="modal">取消</a>
                                                                </li>
                                                        </ul>
                                                </form>
                                        </div>
                                </dl>
                        </div>
                </div>
        </div>
        
        <script>
            
        function unconnected_callback() {
                loading_ajax_hide();
                show_remind( "請先登入" , "error"  );
        };
        function connected_callback( member ) {
                if( member.a_admin !== "true" ){
                        loading_ajax_hide();
                        show_remind( "不是管理者，三秒後轉跳到首頁。" , "error" );
                        setTimeout( function(){ location.href = "../index.php" }, 3000);
                }
                else{
                        init();
                }
        };
                
        </script>
</body>
</html>
