<?php header("X-Frame-Options: DENY");?>
<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale = 1.0, user-scalable = 0" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="app-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="apple-touch-fullscreen" content="yes" />
        <link rel='shortcut icon' href='template/images/favicon.ico' type='x-icon'>
        <title>AROFLY | javascirpt API</title>
        <meta name="description" content="What you see what you get Enjoy to Interactive with living objects">

        <link class="icon" href="../icon/logo.png" rel="apple-touch-icon-precomposed" />
        <link class="icon" href="../icon/logo.png" rel="SHORTCUT ICON" />
        
        <link rel="stylesheet" href="css/all_css.css">
        <link rel="stylesheet" href="css/global.css">
        
        <link href="css/plugins/codemirror/codemirror.css" rel="stylesheet">
        <link href="css/plugins/codemirror/ambiance.css" rel="stylesheet">

        <!-- CodeMirror -->
        <script src="js/plugins/codemirror/codemirror.js"></script>
        <script src="js/plugins/codemirror/mode/javascript/javascript.js"></script>
        
        <?php include( "js/all_js.php"); ?>
        
        <style>
            input {
                border: 2px solid rgb(163, 177, 178);
                border-radius: 3px;
                color: rgb(102, 102, 102);
                display: inline-block;
                font-size: 12px;
                height: 30px;
                margin: 0 10px;
                padding: 0 5px;
            }
            .function_title {
                margin: 20px 0px 10px; 
                color: rgb(68, 68, 68); 
                font-weight: 600;
            }
            .list {
                width: 100%;
                overflow-x: scroll;
            }
            #datatable1 {
                width: 90%;
                text-align: left;
            }
        </style>
        <!-- edit by abin for restAPI-->
		 <script src="js/mgm_index/restAPI.js"></script>
		 <script>
			$( document ).ready(function() {
				var cleanCallBack = function() {
					$("#sendCallBack").val('');
					$("#sendStatus").val('');	
				}
				
				var initView = function(selectPageEvent) {
					var selectPage = $("#selectPage");
					selectPage.html("");
					var apiData = $.RESTfulAPI.conf;
					$.each(apiData, function(key, val) {
						selectPage.append(
							'<option value="'+ key +'">'+ key +'</option>'
						);
					});
					selectPage.change(selectPageEvent);
				}
				
				var testSendEvent = function(e) {
					cleanCallBack();
					var data = {};
					try {
						data = JSON.parse($("#testData").val());
					}
					catch(e) {
						$("#sendCallBack").val("send error: " + e.message);
						return;
					}
					$.ajax({
						type: $("#testMethod").val(),
						dataType: "text",
						url: "slim2"+$("#testUrl").val(),
						data: data,
						success: function(msg, status){
							$("#sendCallBack").val(msg);
							if(status == "success") {
								$("#sendStatus").val(200);
							}
						},
						error:function(xhr, ajaxOptions, thrownError){ 
							$("#sendStatus").val(xhr.status);
							$("#sendCallBack").val(thrownError);
						}
					});
				}
				
				var testButtonEvent = function(e) {
					var data = $(this).data("conf");
					//清除callBack資料
					cleanCallBack();
					
					$("#testData").val(JSON.stringify(data['data']));
					$("#testDesc").html(data['desc']);
					$("#testMethod").val(data['type']);
					$("#testUrl").val(data['url']);
				}
					
				var selectPageEvent = function(e) {
					var testingButton = $("#testingButton");
					testingButton.html("");
					$("#testUrl").val('');
					$("#testData").val('');
					cleanCallBack();
					
					var key = $(this).val();
					var apiData = $.RESTfulAPI.conf[key];
					var button = '';
					for(var i=0; i<apiData.length; i++) {
						for(var j=0; j<apiData[i]['test'].length; j++) {
							var test = apiData[i]['test'][j];
							button = '<input class="button" value="'+ test['buttonTitle'] +'" type="button">';
							button = $(button).appendTo(testingButton);
							button.data( "conf",  {
								type: apiData[i]['type']
								,url: apiData[i]['url']
								,data: test['data']
								,desc: test['desc']
							});
							button.click(testButtonEvent);
						}
					}
				}
				
				initView(selectPageEvent);
				cleanCallBack();
				$("#selectPage").trigger("change");
				$("#testSend").click(testSendEvent);
			});
		 </script>
</head>

<body>
        <div id="all">

                <?php include( "html/loading.php"); ?>
                <?php include( "html/header.php"); ?>

                <div class="content">
                        <?php include( "html/sidebar_setting.php"); ?>

                        <div class="main-content container">                        
                                <div class="path">
                                        <a href="#">Console</a> > <a href="#">javascirpt API</a>
                                </div>

                                <div class="list">

                                        <h2>Everything about your Pets</h2>
                                        
                                        
                                        <table class="display select dataTable" id="datatable1">
                                                <thead>
                                                        <tr>
                                                                <th style="width:50%;">Input json data</th>
                                                                <th>Description</th>
                                                        </tr>
                                                </thead>
                                                <tbody>
                                                        <tr class="odd child-middle">
                                                                <td colspan="2"> 
                                                                        <p>Incloud Function</p>
                                                                        <textarea id="code3" style="height: 200px;">
<script src="js/jquery.1.6.js"></script>

<!-- Google Maps and Places API -->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&libraries=visualization"></script>
<script src="js/jquery.map.klokantech.js"></script>

<!-- Google Maps and Places API -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places&callback=initAutocomplete" async defer></script>

                                                                        </textarea>
                                                                </td>
                                                        </tr>
                                                        
                                                        <tr class="odd child-middle">
                                                                <td colspan="2"> 
                                                                        <p> Ajax</p>
                                                                        <textarea id="code4" style="height: 2000px;">

// init
function googlemap_Map() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -34.397, lng: 150.644},
    zoom: 8
  });
}

// evnet
google.maps.event.addListener(map, 'click', function(event) {
    console.log( event.latLng );
});

// search
// https://developers.google.com/maps/documentation/javascript/examples/places-searchbox?hl=zh-tw
function search(places ){

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
}


// panTo
// https://developers.google.com/maps/documentation/javascript/examples/event-simple?hl=zh-tw
function googlemap_panTo( getPosition ) {
  map.panTo( getPosition );
}
example : 
var position = { lat: 12.97, lng: 77.59 };
googlemap_panTo( position );

// info windows
// https://developers.google.com/maps/documentation/javascript/examples/infowindow-simple?hl=zh-tw
function googlemap_infowindow( getPosition ) {
    infowindow.open(map, marker);
}

example : 
var contentString = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h1 id="firstHeading" class="firstHeading">Uluru</h1>'+
    '<div id="bodyContent">'+
    '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
    'sandstone rock formation in the southern part of the '+
    'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
    'south west of the nearest large town, Alice Springs; 450&#160;km '+
    '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+
    'features of the Uluru - Kata Tjuta National Park. Uluru is '+
    'sacred to the Pitjantjatjara and Yankunytjatjara, the '+
    'Aboriginal people of the area. It has many springs, waterholes, '+
    'rock caves and ancient paintings. Uluru is listed as a World '+
    'Heritage Site.</p>'+
    '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
    'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
    '(last visited June 22, 2009).</p>'+
    '</div>'+
    '</div>';

var infowindow = new google.maps.InfoWindow({
  content: contentString
});

var marker = new google.maps.Marker({
  position: uluru,
  map: map,
  title: 'Uluru (Ayers Rock)'
});
googlemap_infowindow( position );



                                                                        </textarea>
                                                                </td>
                                                        </tr>
                                                </tbody>
                                        </table>
                                        
                                </div>
                        </div>

                </div>

                <?php include( "html/footer.php"); ?>

        </div>
    


    <script>
         $(document).ready(function(){

             /*var editor_one = CodeMirror.fromTextArea(document.getElementById("code1"), {
                 lineNumbers: true,
                 matchBrackets: true,
                 styleActiveLine: true,
                 theme:"ambiance"
             });*/

             var editor_two = CodeMirror.fromTextArea(document.getElementById("code1"), {
                 lineNumbers: true,
                 matchBrackets: true,
                 styleActiveLine: true
             });
             var editor_two = CodeMirror.fromTextArea(document.getElementById("code2"), {
                 lineNumbers: true,
                 matchBrackets: true,
                 styleActiveLine: true
             });
             var editor_two = CodeMirror.fromTextArea(document.getElementById("code3"), {
                 lineNumbers: true,
                 matchBrackets: true,
                 styleActiveLine: true
             });
             var editor_two = CodeMirror.fromTextArea(document.getElementById("code4"), {
                 lineNumbers: true,
                 matchBrackets: true,
                 styleActiveLine: true
             });
             var editor_two = CodeMirror.fromTextArea(document.getElementById("code5"), {
                 lineNumbers: true,
                 matchBrackets: true,
                 styleActiveLine: true
             });

        });
            
        function init() {
                loading_ajax_hide();
                show_remind( "已登入，填入 User Token" , "success"  );
                $( "#user_token" ).val( getCookie( "scs_cookie" ) )
        };
        
        function unconnected_callback() {
                loading_ajax_hide();
                show_remind( "請先登入" , "error"  );
        };
        function connected_callback( member ) {
                if( member.a_admin !== "true" ){
                        loading_ajax_hide();
                        show_remind( "不是管理者，三秒後轉跳到首頁。" , "error" );
                        setTimeout( function(){ location.href = "../index.php" }, 3000);
                }
                else{
                        init();
                }
        };
        
        </script>
</body>
</html>
