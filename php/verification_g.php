<?php 

include 'config.php';
include 'global.php';

$callback = array();
//// 開啟 session  
//if (!isset($_SESSION)) { session_start(); }
//  
//// 將驗證碼記錄在 session 中  
//echo $_SESSION["verification__session"] ;

$DB_CON = DB_CON( DB_NAME );
if( !$DB_CON["success"] ){
        return $DB_CON;
}
$con = $DB_CON["data"];

$hf_account_data = get_sql($con, "account", "WHERE a_id='3G15VSC9F0'");
if( !$hf_account_data ){
        $callback['msg'] = "確認您輸入的信箱或密碼是否正確";
        $callback['success'] = false;
        mysqli_close($con);
        echo json_encode($callback);
}

if( $hf_account_data[0]["a_state"] === "block" ){
        $callback = set_random_token( $con , $hf_account_data[0] );
}
else if( $hf_account_data[0]["a_state"] === "blockade" ){
        $callback['msg'] = "帳號被停權，無法登入。";
        $callback['success'] = false;
}

mysqli_close($con);
echo json_encode($callback);

function set_random_token( $con , $hf_account_data ) {
        
        $callback = array();
        $user_rememberme_token = $hf_account_data["a_token"];
        while( 1 ) {
            $token = getRandom(20);
            $set_token = md5($token);
            $result = mysqli_query($con, "SELECT * FROM account WHERE a_token LIKE '%\\\"$set_token\\\"%'");
            if (mysqli_num_rows($result) == 0) {break;}
        }

        if( $user_rememberme_token === '' || $user_rememberme_token === '[]' ) {
            $set_token = array( $set_token );
            $set_token = json_encode( $set_token );
        }
        else {
            $user_rememberme_token = json_decode( $user_rememberme_token );
            if( count( $user_rememberme_token ) >= (int)10 )//MAX_NUM
            {
                $splice_num = count( $user_rememberme_token ) - (int)10 + 1;
                array_splice($user_rememberme_token, 0, $splice_num);
            }
            
            $user_rememberme_token[] = $set_token ;
            $set_token = $user_rememberme_token;
            $set_token = json_encode( $set_token );
        }

        $sql = "UPDATE account SET a_token='$set_token' WHERE a_id='".$hf_account_data['a_id']."'";
        if( mysqli_query( $con , $sql ) ) {
             $callback['data'] = $token;
             $callback['success'] = true;
        }
        else {
             $callback['msg'] = "save token error";
             $callback['success'] = false;
        }
        return $callback;
}
?>