<?php

include 'config.php';
include 'global.php';

$func = $_REQUEST["func"];

switch ($func) {
    case "add":
        if (!isset($_SESSION)) { session_start(); }  
        $echo = add();
        break;
    case "fb_signup_login": 
        $echo = fb_signup_login();
        break;
    case "loginbytoken":
        $echo = loginbytoken();
        break;
    case "logout":
        $echo = logout();
        break;
    case "login":
        if (!isset($_SESSION)) { session_start(); }  
        $echo = login();
        break;
    case "login_test":
        if (!isset($_SESSION)) { session_start(); }  
        $echo = login_test();
        break;
    case "set_icon":
        $echo = set_icon();
        break;
    case "read_info":
        $echo = read_info();
        break;
    case "edit_info":
        $echo = edit_info();
        break;
    case "set_payment":
        $echo = set_payment();
        break;
    case "set_limit":
        $echo = set_limit();
        break;
    case "set_limit_for":
        $echo = set_limit_for();
        break;
    case "change_pwd":
        $echo = change_pwd();
        break;
    case "change_pwd_by_authenticate":
        if (!isset($_SESSION)) { session_start(); }  
        $echo = change_pwd_by_authenticate();
        break;
    case "send_forget":
        if (!isset($_SESSION)) { session_start(); }  
        $echo = send_forget();
        break;
    case "check_forget_token":
        $echo = check_forget_token();
        break;
    case "authenticate":
        $echo = authenticate();
        break;
    case "send_authenticate":
        if (!isset($_SESSION)) { session_start(); }  
        $echo = send_authenticate();
        break;
    case "send_authenticate_fb":
        if (!isset($_SESSION)) { session_start(); }  
        $echo = send_authenticate_fb();
        break;
    case "member_send_authenticate":
        if (!isset($_SESSION)) { session_start(); }  
        $echo = member_send_authenticate();
        break;
    case "check_authenticate":
        if (!isset($_SESSION)) { session_start(); }  
        $echo = check_authenticate();
        break;
    case "pause_member":
        $echo = pause_member();
        break;
    case "check_bank":
        $echo = check_bank();
        break;
    case "check_info":
        $echo = check_info();
        break;
}

echo json_encode($echo);

function add(){
        
        $callback = array();
        try{
                if( check_empty( array( 'Signup_email','Signup_nickname','Signup_age','Signup_sex','Signup_password','Signup_job','Signup_city' ) ) ) {
                    
                    $Signup_email = $_REQUEST['Signup_email'];
                    $Signup_nickname = $_REQUEST['Signup_nickname'];
                    $Signup_age = $_REQUEST['Signup_age'];
                    $Signup_sex = $_REQUEST['Signup_sex'];
                    $Signup_password = $_REQUEST['Signup_password'];
                    $Signup_job = $_REQUEST['Signup_job']+0;
                    $Signup_city = $_REQUEST['Signup_city']+0;
                    date_default_timezone_set('Asia/Taipei');
                    
                    if( !in_array($Signup_sex, array("male","female")) ){
                            $callback['msg'] = "輸入資料錯誤";
                            $callback['success'] = false;
                            return $callback;
                    }
                    if( intval($Signup_age) != $Signup_age ){
                            $callback['msg'] = "輸入資料錯誤";
                            $callback['success'] = false;
                            return $callback;
                    }
//                    if( !in_array($Signup_job, array("ele","none")) ){
//                            $callback['msg'] = "輸入資料錯誤";
//                            $callback['success'] = false;
//                            return $callback;
//                    }arod 20160920
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    $account = Select_Mysql($con, "account", array( "a_email" => $Signup_email ));
                    if ( $account ) {
                        $callback['msg'] = "email exist";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                    }
                    
                    while( 1 ) {
                        $id = getRandom( 10 );
                        $check = Select_Mysql($con, "account", array( "a_id" => $id ));
                        if (!$check) {
                                break;
                        }
                    }
                    
                    if( !file_exists(account_path.$id) ){
                        mkdir(account_path.$id, 0777, true);
                        mkdir(account_path.$id."\\Original", 0777, true);
                        mkdir(account_path.$id."\\Preview", 0777, true);
                        mkdir(account_path.$id."\\ThumbnailM", 0777, true);
                        mkdir(account_path.$id."\\ThumbnailS", 0777, true);
                    }
                    
                    //set token
                    while( 1 ) {
                        $token = getRandom(20);
                        $set_token = md5($token);
                        $result = mysqli_query($con, "SELECT * FROM account WHERE a_token LIKE '%\\\"$set_token\\\"%'");
                        if (mysqli_num_rows($result) == 0) {break;}
                    }
                    $set_token = array( $set_token );
                    $set_token = json_encode( $set_token );
                    
                    $insert_array = array( "a_id" => $id ,
                                           "a_nickname" => $Signup_nickname ,
                                           "a_sex" => $Signup_sex ,
                                           "a_email" => $Signup_email ,
                                           "a_password" => $Signup_password ,
                                           "a_age" => date('Y') - $Signup_age ,
                                           "aj_id" => $Signup_job ,
                                           "ac_id" => $Signup_city ,
                                           "a_token" => $set_token ,
                                           "a_registration_time" => date('Y-m-d H:i:s') ,
                                           "a_last_login_time" => date('Y-m-d H:i:s') );
                    
                    if( insert_sql($con, "account", $insert_array) && insert_sql( $con , "page_history" , array( "a_id" => $insert_array['a_id'], "ph_json" => "[]" ) ) && insert_sql( $con , "page_favorite" , array( "a_id" => $insert_array['a_id'], "pf_json" => "[]" ) )) {
                                $callback['data'] = $token;
                                $callback['success'] = true;
                    }
                    else {
                                $callback['success'] = false;
                                $callback['msg'] = "add fail";
                    }
                    
                    mysqli_close($con);

                }
                else {
                    
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                    
                }
                return $callback;
                
        }
        catch (Exception $e)
        {
                echo "false";
        }
}

function fb_signup_login(){
        
        $callback = array();
        try{
                if( check_empty( array( 'FB_email','FB_id','FB_name' ) ) ) {
                    
                    $FB_email = $_REQUEST['FB_email'];
                    $FB_id = $_REQUEST['FB_id'];
                    $FB_name = $_REQUEST['FB_name'];
                    date_default_timezone_set('Asia/Taipei');
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    $account = Select_Mysql($con, "account", array( "a_email" => $FB_email ));
                    if ( $account ) {
                        
                        if( $account[0]["a_state"] === "block" ){
                                $callback = set_random_token( $con , $account[0] );
                                $callback['msg'] = "登入成功。";
                        }
                        else if( $account[0]["a_state"] === "blockade" ){
                                $callback['msg'] = "帳號被停權，無法登入。";
                                $callback['success'] = false;
                        }
                        
                    }
                    else{
                        
                        while( 1 ) {
                            $id = getRandom( 10 );
                            $check = Select_Mysql($con, "account", array( "a_id" => $id ));
                            if (!$check) {
                                    break;
                            }
                        }

                        if( !file_exists(account_path.$id) ){
                            mkdir(account_path.$id, 0777, true);
                            mkdir(account_path.$id."\\Original", 0777, true);
                            mkdir(account_path.$id."\\Preview", 0777, true);
                            mkdir(account_path.$id."\\ThumbnailM", 0777, true);
                            mkdir(account_path.$id."\\ThumbnailS", 0777, true);
                        }

                        //set token
                        while( 1 ) {
                            $token = getRandom(20);
                            $set_token = md5($token);
                            $result = mysqli_query($con, "SELECT * FROM account WHERE a_token LIKE '%\\\"$set_token\\\"%'");
                            if (mysqli_num_rows($result) == 0) {break;}
                        }
                        $set_token = array( $set_token );
                        $set_token = json_encode( $set_token );

                        $insert_array = array( "a_id" => $id ,
                                               "a_fb_id" => $FB_id ,
                                               "a_nickname" => $FB_name ,
                                               "a_email" => $FB_email ,
                                               "a_token" => $set_token ,
                                               "a_registration_time" => date('Y-m-d H:i:s') ,
                                               "a_last_login_time" => date('Y-m-d H:i:s') );

                        if( insert_sql($con, "account", $insert_array) ) {
                            $callback['data'] = $token;
                            $callback['msg'] = "註冊成功並登入。";
                            $callback['success'] = true;
                        }
                        else {
                            $callback['success'] = false;
                            $callback['msg'] = "add fail";
                        }
                        
                    }
                    
                    
                    mysqli_close($con);

                }
                else {
                    
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                    
                }
                return $callback;
                
        }
        catch (Exception $e)
        {
                echo "false";
        }
}

function loginbytoken(){
        
        $callback = array();
        try{
                if( check_empty( array( "token" ) ) ) {
                    
                    $token = md5( $_REQUEST[ "token" ] );
                    $cart = array();
                    date_default_timezone_set('Asia/Taipei');
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    $Check_Member = Check_Member( $con , $token );
                    if( !$Check_Member["success"] ){
                            return $Check_Member;
                    }
                    $account = $Check_Member["data"];
                    
                    
                    if( $account[0]["a_state"] === "Y" ){
                            $callback['msg'] = "帳號被停權，無法登入。";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    $account_job = get_sql($con, "account_job", "WHERE aj_id = ".$account[0]['aj_id']);
                    $account_city = get_sql($con, "account_city", "WHERE ac_id = ".$account[0]['ac_id']);
                    
                    $cart = array( "a_id" => $account[0]["a_id"] , 
                        "a_admin" => $account[0]["a_admin"] , 
                        "a_nickname" => $account[0]["a_nickname"] , 
                        "a_email" => $account[0]["a_email"] , 
                        "a_sex" => $account[0]["a_sex"] , 
                        "a_age" => $account[0]["a_age"] , 
                        "a_job" => $account_job[0]["aj_name"]  , 
                        "a_city" => $account_city[0]["ac_name"]);

                    $callback['data'] = $cart;
                    $callback['success'] = true;

                    mysqli_close($con);

                }
                else {
                    
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                    
                }
                
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function logout(){
        
        $callback = array();
        try{
                if( check_empty( array( "token" ) ) ) {
                    
                    $token = md5( $_REQUEST['token'] );
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    $account = get_sql($con, "account", "WHERE a_token LIKE '%\\\"".mysqli_real_escape_string($con,$token)."\\\"%'");
                    if( $account ) {

                        $user_rememberme_token = $account[0]['a_token'];
                        $user_rememberme_token = json_decode( $user_rememberme_token , true );
                        foreach ($user_rememberme_token as $key => $value) {
                            if( $value === $token ) {
                                array_splice($user_rememberme_token, $key, 1);
                                break;
                            }
                        }
                        $user_rememberme_token = json_encode( $user_rememberme_token );

                        $cmd = "UPDATE account SET a_token='$user_rememberme_token' WHERE a_id='" . $account[0]["a_id"] . "'";
                        if( mysqli_query( $con , $cmd ) ) {
                                $callback['success'] = true;
                        }
                        else {
                                $callback['msg'] = "delete token error";
                                $callback['success'] = false;
                        }
                    }
                    else {
                        $callback['success'] = true;
                    }
                    mysqli_close($con);

                }
                else {
                    
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                    
                }
                
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
}

function login(){
        
        $callback = array();
        try{
                if( check_empty( array( "account" , "password" ) ) ) {
                    
                    date_default_timezone_set('Asia/Taipei');
                    $email = $_REQUEST["account"];
                    $password = $_REQUEST["password"];
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    
                    $account = Select_Mysql($con, "account", array( "a_email" => $email , "a_password" => $password ));
                    if( !$account ){
                            $callback['msg'] = "確認您輸入的信箱或密碼是否正確";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    if( $account[0]["a_state"] === "block" ){
                            $callback = set_random_token( $con , $account[0] );
                    }
                    else if( $account[0]["a_state"] === "blockade" ){
                            $callback['msg'] = "帳號被停權，無法登入。";
                            $callback['success'] = false;
                    }
                            
                    mysqli_close($con);
                    
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function login_test(){
        
    
        $callback = array();
        try{
                    
                date_default_timezone_set('Asia/Taipei');
                    
                $DB_CON = DB_CON( DB_NAME );
                if( !$DB_CON["success"] ){
                        return $DB_CON;
                }
                $con = $DB_CON["data"];
                
                $account = Select_Mysql($con, "account", array( "a_id" => "3G15VSC9F0" ));
                if( !$account ){
                        $callback['msg'] = "確認您輸入的信箱或密碼是否正確";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                
                if( $account[0]["a_state"] === "block" ){
                        $callback = set_random_token( $con , $account[0] );
                }
                else if( $account[0]["a_state"] === "blockade" ){
                        $callback['msg'] = "帳號被停權，無法登入。";
                        $callback['success'] = false;
                }

                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function set_icon(){
        
        $callback = array();
        try{
                if( check_empty( array( "token" ) ) ) {
                    
                    if( !check_empty( array( "account_icon" ) ) ) {
                            $callback['success'] = true;
                            return $callback;
                    }
                    date_default_timezone_set('Asia/Taipei');
                    $token = md5( $_REQUEST[ "token" ] );
                    $account_icon = isset($_REQUEST["account_icon"]) ? $_REQUEST["account_icon"] : "";
                    $update_json = array();
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    $Check_Member = Check_Member( $con , $token );
                    if( !$Check_Member["success"] ){
                            return $Check_Member;
                    }
                    $account = $Check_Member["data"];
                    
                    if( $account_icon !== "" ) {
//                        if( $account_icon === "CLEAR" ){
//                                if( $channel && $channel[0]["ch_icon"] !== "" ){
//                                    if( file_exists(channel_path.$ch_id."\\".$channel[0]["ch_icon"]) ){
//                                        unlink(channel_path.$ch_id."\\".$channel[0]["ch_icon"]);
//                                    }
//                                    $json["ch_icon"] = "";
//                                    $r_icon = "";
//                                }
//                        }
//                        else{
                                if( !file_exists(upload_transient_file.$account_icon) ){
                                    $callback['msg'] = "icon not exist";
                                    $callback['success'] = false;
                                    mysqli_close($con);
                                    return $callback;
                                }
                                $account_icon_sub = explode( "." , $account_icon );
                                $account_icon_sub = $account_icon_sub[count($account_icon_sub)-1];
                                copy( upload_transient_file.$account_icon , account_path.$account[0]["a_id"]."\\icon.".$account_icon_sub );
                                $add_a_icon = "icon." . $account_icon_sub;
                                if( !file_exists( account_path.$account[0]["a_id"]."\\icon.".$account_icon_sub ) ){
                                    $callback['success'] = false;
                                    $callback['msg'] = "Upload icon fail";
                                    mysqli_close($con);
                                    return $callback;
                                }
                                $json["a_icon"] = $add_a_icon;
//                        }
                    }
                    
                    if (update_sql( $con , "account" , $json , array( "a_id" => $account[0]["a_id"] ) )) {
                        $callback['success'] = true;
                    }
                    else {
                        $callback['msg'] = "update fail";
                        $callback['success'] = false;
                    }
                            
                    mysqli_close($con);
                    
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function read_info(){
        $callback = array();
        try{
                
//                if( !check_empty( array( "token"  ) ) ) {
//                        $callback['msg'] = "輸入資料不完整";
//                        $callback['success'] = false;
//                        return $callback;
//                }
                
                date_default_timezone_set('Asia/Taipei');
                $token = md5( $_REQUEST[ "token" ] );
                $update_json = array();
                

                $DB_CON = DB_CON( DB_NAME );
                if( !$DB_CON["success"] ){
                        return $DB_CON;
                }
                $con = $DB_CON["data"];
                
                $Check_Member = Check_Member( $con , $token );
                if( !$Check_Member["success"] ){
                        return $Check_Member;
                }
                $Check_Member["data"][0]["a_age"] = date('Y') - $Check_Member["data"][0]["a_age"];
                $callback['data'] = $Check_Member["data"][0];
                $callback['success'] = true;

                mysqli_close($con);
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function edit_info(){
        $callback = array();
        try{
                
                if( !check_empty( array( "token" , "Mem_nickname" , 
                                        "Mem_age" , "Mem_job" , 
                                        "Mem_city" , "Mem_sex" ) ) ) {
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
                
                date_default_timezone_set('Asia/Taipei');
                $token = md5( $_REQUEST[ "token" ] );
                $Mem_nickname = $_REQUEST["Mem_nickname"];
                $Mem_age = $_REQUEST["Mem_age"];
                $Mem_job = $_REQUEST["Mem_job"];
                $Mem_city = $_REQUEST["Mem_city"];
                $Mem_habit = $_REQUEST["Mem_habit"];
                $Mem_sex = $_REQUEST["Mem_sex"];
                $update_json = array();
                
                if( !in_array($Mem_sex, array("male","female")) ){
                        $callback['msg'] = "輸入資料錯誤";
                        $callback['success'] = false;
                        return $callback;
                }
                if( intval($Mem_age) != $Mem_age ){
                        $callback['msg'] = "輸入資料錯誤2";
                        $callback['success'] = false;
                        return $callback;
                }

                $DB_CON = DB_CON( DB_NAME );
                if( !$DB_CON["success"] ){
                        return $DB_CON;
                }
                $con = $DB_CON["data"];
                
                $Check_Member = Check_Member( $con , $token );
                if( !$Check_Member["success"] ){
                        return $Check_Member;
                }
                $account = $Check_Member["data"];
                
                
//                if( $account[0]["a_email"] !== $a_email ) {
//                        if( get_sql($con, "account", "WHERE a_email='$a_email'") ){
//                                $callback['msg'] = "信箱已有人使用";
//                                $callback['success'] = false;
//                                mysqli_close($con);
//                                return $callback;
//                        }
//                        $callback = send_authenticate_letter( $con , $a_email , $account[0]["a_nickname"] );
//                        if( !$callback['success'] ){
//                                mysqli_close($con);
//                                return $callback;
//                        }
//                        $update_json["a_email_Confirm"] = $callback["a_email_Confirm"];
//                        $a_email_Confirm = false;
//                }
                
                $update_json["a_nickname"] = $Mem_nickname;
                $update_json["a_sex"] = $Mem_sex;
                $update_json["a_age"] = date('Y') - $Mem_age;
                $update_json["aj_id"] = $Mem_job;
                $update_json["ac_id"] = $Mem_city;
                $update_json["a_habit"] = $Mem_habit;
                
                if (update_sql( $con , "account" , $update_json , array( "a_id" => $account[0]["a_id"] ) )) {
//                    $callback["data"] = array( "a_email_Confirm" => $a_email_Confirm , "a_email" => $a_email , "a_icon" => $r_icon );
                    $callback['success'] = true;
                }
                else {
                    $callback['msg'] = "update fail";
                    $callback['success'] = false;
                }

                mysqli_close($con);
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function set_payment(){
        $callback = array();
        try{
                if( check_empty( array( "token" ,"a_payment_method" ) ) ) {
                    
                    date_default_timezone_set('Asia/Taipei');
                    $update_array = array();
                    $token = md5( $_REQUEST[ "token" ] );
                    $a_payment_method = $_REQUEST["a_payment_method"];
                    if( !in_array($a_payment_method, array("POST","PAYPAL")) ){
                            $callback['msg'] = "輸入資料不完整";
                            $callback['success'] = false;
                            return $callback;
                    }
                    $update_array["a_payment_method"] = $a_payment_method;
                    
                    if( $a_payment_method === "POST" ){
                         if( !check_empty( array( "account_payment_name","account_payment_bank","account_payment_account" ) ) ){
                                $callback['msg'] = "輸入資料不完整";
                                $callback['success'] = false;
                                return $callback;
                         }
                         $update_array["a_payment_name"] = $_REQUEST[ "account_payment_name" ];
                         $update_array["a_payment_bank"] = $_REQUEST[ "account_payment_bank" ];
                         $update_array["a_payment_account"] = $_REQUEST[ "account_payment_account" ];
                         $update_array["a_paypal_email"] = isset($_REQUEST["paypal_email"]) ? $_REQUEST["paypal_email"] : "";
                         
                    }
                    else if( $a_payment_method === "PAYPAL" ){
                        if( !check_empty( array( "paypal_email" ) ) ){
                                $callback['msg'] = "輸入資料不完整";
                                $callback['success'] = false;
                                return $callback;
                         }
                         $update_array["a_paypal_email"] = $_REQUEST[ "paypal_email" ];
                         $update_array["a_payment_name"] = isset($_REQUEST["account_payment_name"]) ? $_REQUEST["account_payment_name"] : "";
                         $update_array["a_payment_bank"] = isset($_REQUEST["account_payment_bank"]) ? $_REQUEST["account_payment_bank"] : "";
                         $update_array["a_payment_account"] = isset($_REQUEST["account_payment_account"]) ? $_REQUEST["account_payment_account"] : "";
                    }
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    $Check_Member = Check_Member( $con , $token );
                    if( !$Check_Member["success"] ){
                            return $Check_Member;
                    }
                    $account = $Check_Member["data"];
                    
                    if (update_sql( $con , "account" , $update_array , array( "a_id" => $account[0]["a_id"] ) )) {
                        $callback['success'] = true;
                    }
                    else {
                        $callback['msg'] = "update fail";
                        $callback['success'] = false;
                    }
                            
                    mysqli_close($con);
                    
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function set_limit(){
        $callback = array();
        try{
                if( check_empty( array( "token" ,
                                        "a_limit" ) ) ) {
                    
                    $token = md5( $_REQUEST[ "token" ] );
                    $a_limit = $_REQUEST["a_limit"];
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    $Check_Admin = Check_Admin( $con , $token );
                    if( !$Check_Admin["success"] ){
                            return $Check_Admin;
                    }
                    $account = $Check_Admin["data"];
                    
                    $update_json = array();
                    $update_json["a_limit_token"] = $a_limit;
                    
                    if (update_sql( $con , "account" , $update_json , array( "a_id" => $account[0]["a_id"] ) )) {
                        $callback['success'] = true;
                    }
                    else {
                        $callback['msg'] = "update fail";
                        $callback['success'] = false;
                    }
                            
                    mysqli_close($con);
                    
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function set_limit_for(){
        $callback = array();
        try{
                if( check_empty( array( "token" , "a_limit" , "to" ) ) ) {
                    
                    $token = md5( $_REQUEST[ "token" ] );
                    $a_limit = $_REQUEST["a_limit"];
                    $to = $_REQUEST["to"];
                    
                    if( (int)$a_limit <= 0 ){
                            $callback['msg'] = "人數設定錯誤";
                            $callback['success'] = false;
                            return $callback;
                    }
                    
                    if( !in_array($to, array("common","admin")) ){
                            $callback['msg'] = "輸入資料不完整";
                            $callback['success'] = false;
                            return $callback;
                    }
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];

                    $Check_Admin = Check_Admin( $con , $token );
                    if( !$Check_Admin["success"] ){
                            return $Check_Admin;
                    }
                    $account = $Check_Admin["data"];
                    
                    $a_admin = $to === "common" ? "false" : "true";
                    $system_column = $to === "common" ? "s_common_login_num" : "s_admin_login_num";
                    $update_json = array();
                    $update_json["a_limit_token"] = (int)$a_limit;
                    $update_json2 = array();
                    $update_json2[$system_column] = (int)$a_limit;
                    
                    if (update_sql( $con , "account" , $update_json , array( "a_admin" => $a_admin ) )) {
                        if( update_sql( $con , "system" , $update_json2 , array( "s_id" => 1 ) ) ){
                            $callback['success'] = true;
                        }
                        else {
                            $callback['msg'] = "update fail";
                            $callback['success'] = false;
                        }
                    }
                    else {
                        $callback['msg'] = "update fail";
                        $callback['success'] = false;
                    }
                            
                    mysqli_close($con);
                    
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function change_pwd(){
        $callback = array();
        try{
                if( check_empty( array( "token" ,
                                        "account_oldpassword" , 
                                        "account_newpassword" ) ) ) {
                    
                    $token = md5( $_REQUEST[ "token" ] );
                    $account_oldpassword = $_REQUEST["account_oldpassword"];
                    $account_newpassword = $_REQUEST["account_newpassword"];
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];

                    $Check_Member = Check_Member( $con , $token );
                    if( !$Check_Member["success"] ){
                            return $Check_Member;
                    }
                    $account = $Check_Member["data"];
                    if( $account_oldpassword === $account_newpassword ) {
                            $callback['msg'] = "舊密碼與新密碼相同";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    if( $account[0]["a_password"] !== $account_oldpassword ) {
                            $callback['msg'] = "舊密碼錯誤";
                            $callback['action'] = "email";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    $update_json = array();
                    $update_json["a_password"] = $account_newpassword;
                    $update_json["a_token"] = "[]";
                    
                    if (update_sql( $con , "account" , $update_json , array( "a_id" => $account[0]["a_id"] ) )) {
                        $callback['success'] = true;
                    }
                    else {
                        $callback['msg'] = "update fail";
                        $callback['success'] = false;
                    }
                            
                    mysqli_close($con);
                    
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function change_pwd_by_authenticate(){
        $callback = array();
        try{
                if( check_empty( array( "authenticate" , "new_password" ) ) ) {
                    
                    $authenticate = $_REQUEST[ "authenticate" ];
                    $new_password = $_REQUEST["new_password"];
                    
                    if( $_SESSION["authenticate"] !== $authenticate ){
                            $callback['msg'] = "此信箱認證碼修改密碼時間到期";
                            $callback['success'] = false;
                            return $callback;
                    }
                    if( !isset($_SESSION["email"]) || empty($_SESSION["email"]) ){
                            $callback['msg'] = "修改錯誤";
                            $callback['success'] = false;
                            return $callback;
                    }
                    $email = $_SESSION["email"];
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    date_default_timezone_set('Asia/Taipei');
                    $account = Select_Mysql($con, "account", array( "a_email" => $email ));
                    if( !$account ){
                            $callback['msg'] = "此信箱不存在。";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    $update_json = array();
                    $update_json["a_password"] = $new_password;
                    $update_json["a_token"] = "[]";
                    
                    if (update_sql( $con , "account" , $update_json , array( "a_id" => $account[0]["a_id"] ) )) {
                        $callback['success'] = true;
                    }
                    else {
                        $callback['msg'] = "update fail";
                        $callback['success'] = false;
                    }
                    mysqli_close($con);
                    
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function send_forget(){
        $callback = array();
        try{
                if( check_empty( array( "email","authentication" ) ) ) {
                    
                    header("Access-Control-Allow-Origin:*");
                    header('Access-Control-Allow-Credentials:true');
                    header('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS');
                    header('Access-Control-Allow-Headers:Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With');
                    header('Content-Type:text/html; charset=utf-8');
                    require 'gmailsystem/gmail.php';
                    mb_internal_encoding('UTF-8');
                    
                    $email = $_REQUEST["email"];
                    $authentication = $_REQUEST["authentication"];
                    
                    if( $authentication !== $_SESSION["verification__session"] ) {
                            $callback['msg'] = "驗證碼錯誤，請重新輸入";
                            $callback['success'] = false;
                            return $callback;
                    }
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    $account = Select_Mysql($con, "account", array( "a_email" => $email ));
                    if( !$account ){
                            $callback['msg'] = "信箱填寫錯誤，請重新輸入";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    while( true ) {
                            $token = getRandom( 20 );
                            $result = mysqli_query($con, "SELECT * FROM account WHERE a_forget_token='$token'");
                            if (mysqli_num_rows($result) === 0) {
                                    break;
                            }
                    }
                    
                    date_default_timezone_set('Asia/Taipei');
                    $sql_cmd = "UPDATE account SET a_forget_token='$token',a_forget_token_time='" . date('Y-m-d H:i:s',(time()+24*3600)) . "' WHERE a_id='" . $account[0]["a_id"] . "'";
                    if( !mysqli_query($con, $sql_cmd) ){
                            $callback['msg'] = "寄送失敗";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    $html = '<div dir="ltr">
                                <div>
                                    <p class="MsoNormal"> <span style="font-size:10.5pt;font-family:新細明體,serif;background-image:initial;background-repeat:initial">取回密碼說明</span> <span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"><br><br></span> <span lang="EN-US" style="font-family:新細明體,serif"></span> </p>
                                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"> <span style="font-size:10.5pt;font-family:新細明體,serif;color:rgb(55,96,146)">'.$thc_customer_data[0]['a_nickname'].'</span> <span style="font-size:10.5pt;font-family:新細明體,serif">，</span><span style="font-size:10.5pt;font-family:Arial,sans-serif"> </span><span style="font-size:10.5pt;font-family:新細明體,serif">這封信是由</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif;color:rgb(55,96,146)">HF</span><span style="font-size:10.5pt;font-family:新細明體,serif">發送的。</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"></span> </p>
                                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10.5pt;font-family:新細明體,serif">您收到這封郵件，是由於這個郵箱地址在</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif;color:rgb(55,96,146)">HF</span><span style="font-size:10.5pt;font-family:新細明體,serif">被登記為用<wbr>戶</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif">Email</span><span style="font-size:10.5pt;font-family:新細明體,serif">，</span><span style="font-size:10.5pt;font-family:Arial,sans-serif"> </span><span style="font-size:10.5pt;font-family:新細明體,serif">且該用戶請求使用</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif">Email</span><span style="font-size:10.5pt;font-family:新細明體,serif">密碼重置功能所致。</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"></span> </p>
                                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif">------------------------------<wbr>------------------------------<wbr>----------<br>
                                     </span><b><span style="font-size:10.5pt;font-family:新細明體,serif">重要！</span></b><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"><br>
                                     ------------------------------<wbr>------------------------------<wbr>----------</span> </p>
                                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10.5pt;font-family:新細明體,serif">如果您沒有提交密碼重置的請求或不是</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif;color:rgb(55,96,146)">HF</span><span style="font-size:10.5pt;font-family:新細明體,serif">的註冊用戶，<wbr>請立即忽略</span><span style="font-size:10.5pt;font-family:Arial,sans-serif"> </span><span style="font-size:10.5pt;font-family:新細明體,serif">並刪除這封郵件。只有在您確認需要重置密碼的情況下，<wbr>才需要繼續閱讀下面的內容。</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"></span> </p>
                                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif">------------------------------<wbr>------------------------------<wbr>----------<br>
                                     </span><b><span style="font-size:10.5pt;font-family:新細明體,serif">密碼重置說明</span></b><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"><br>
                                     ------------------------------<wbr>------------------------------<wbr>----------</span> </p>
                                    <p class="MsoNormal"><span style="font-size:10.5pt;font-family:新細明體,serif;background-image:initial;background-repeat:initial">您只需在提交請求後的一天內，通過點擊下面的鏈接重置您的密碼：</span><span lang="EN-US" style="font-family:新細明體,serif"></span> </p>
                                    <p class="MsoNormal"><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif;background-image:initial;background-repeat:initial"><a href="' . http_email_send . '?t=' . $token . '" target="_blank">' . http_email_send . '?t=' . $token . '</a></span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"><br>
                                     <span style="background-image:initial;background-repeat:initial">(</span></span><span style="font-size:10.5pt;font-family:新細明體,serif;background-image:initial;background-repeat:initial">如果上面不是鏈接形式，<wbr>請將該地址手工粘貼到瀏覽器地址欄再訪問</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif;background-image:initial;background-repeat:initial">)</span><span lang="EN-US" style="font-family:新細明體,serif"></span> </p>
                                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10.5pt;font-family:新細明體,serif">在上面的鏈接所打開的頁面中輸入新的密碼後提交，<wbr>您即可使用新的密碼登入網站了。<wbr>您可以在用戶控制面板中隨時修改您的密碼。</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"></span> </p>
                                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10.5pt;font-family:新細明體,serif">本請求提交者的</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"> IP </span><span style="font-size:10.5pt;font-family:新細明體,serif">為</span><span style="font-size:10.5pt;font-family:Arial,sans-serif"> <span lang="EN-US">'.$_SERVER['REMOTE_ADDR'].'</span></span>
                                    </p>
                                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10.5pt;font-family:新細明體,serif">此致</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"></span> </p>
                                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif;color:rgb(55,96,146)">HF</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"> </span><span style="font-size:10.5pt;font-family:新細明體,serif">管理團隊</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif">.&nbsp;</span><u><span style="font-size:10.5pt;font-family:新細明體,serif;color:rgb(17,85,204)">'.http_default_path.'</span></u> </p>
                                </div>
                            </div>';
                    $title = '[HF] 取回密碼說明';
                    
                    $callback = mstart( $_REQUEST["email"] , $html , $title );
                    
                    mysqli_close($con);
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function check_forget_token(){
        $callback = array();
        try{
                if( check_empty( array( "token" ) ) ) {
                    
                    $token = $_REQUEST["token"];
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    date_default_timezone_set('Asia/Taipei');
                    $account = Select_Mysql($con, "account", array( "a_forget_token" => $token , "a_forget_token_time" => array( "type" => date('Y-m-d H:i:s') , "value" => ">=" ) ));
                    if( !$account ){
                            $callback['msg'] = "此認證信可能過期或不存在，請重新寄送一次認證信。";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    $callback['success'] = true;
                    mysqli_close($con);
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function authenticate()
{
        $callback = array();
        try{
                if( check_empty( array( "token" ) ) ) {
                    
                    $token = $_REQUEST["token"];
                    date_default_timezone_set('Asia/Taipei');
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    $account = Select_Mysql($con, "account", array( "a_email_Confirm" => $token ));
                    if( !$account ) {
                            $callback['msg'] = "此認證信可能過期或不存在，請重新寄送一次認證信。";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    if( update_sql($con, "account", array( "a_email_Confirm" => "1" ), array( "a_id" => $account[0]["a_id"] )) ){
                            
                            $ran_callback = set_random_token( $con , $account[0] );
                            if( $ran_callback["success"] ){
                                    $callback['data'] = array( "a_nickname" => $account[0]["a_nickname"] , "token" => $ran_callback["data"] );
                                    $callback['success'] = true;
                            }
                            else{
                                    $callback = $ran_callback;
                            }
                    }
                    else{
                            $callback['msg'] = "認證失敗";
                            $callback['success'] = false;
                    }
                    mysqli_close($con);
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function send_authenticate(){
        $callback = array();
        try{
                if( check_empty( array( "email" ) ) ) {
                    
                    $email = $_REQUEST[ "email" ];
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                            $callback['msg'] = "此信箱不正確。";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    $callback = send_authenticate_letter( $con , $email );
                    if( !$callback['success'] ){
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    $_SESSION["authenticate"] = $callback["rand"];
                    $_SESSION["email"] = $email;
                    unset($callback["rand"]);
                    
                    $callback['success'] = true;
                    mysqli_close($con);
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function send_authenticate_fb(){
        $callback = array();
        try{
                if( check_empty( array( "email" ) ) ) {
                    
                    $email = $_REQUEST[ "email" ];
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                            $callback['msg'] = "此信箱不正確。";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    $rand = getRandom( 6 );
                    $_SESSION["authenticate"] = $rand;
                    $_SESSION["email"] = $email;
                    
                    $callback['rand'] = $rand;
                    $callback['success'] = true;
                    mysqli_close($con);
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function member_send_authenticate(){
        $callback = array();
        try{
                if( check_empty( array( "email" ) ) ) {
                    
                    $email = $_REQUEST[ "email" ];
                    
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                            $callback['msg'] = "此信箱不正確。";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];
                    
                    $check = Select_Mysql($con, "account", array( "a_email" => $email ));
                    if( !$check ){
                        $callback['msg'] = "此信箱不是會員。";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                    }
                    
                    $callback = send_forgot_authenticate_letter( $con , $email );
                    if( !$callback['success'] ){
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    $_SESSION["authenticate"] = $callback["rand"];
                    $_SESSION["email"] = $email;
                    unset($callback["rand"]);
                    
                    $callback['success'] = true;
                    mysqli_close($con);
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function check_authenticate(){
        $callback = array();
        try{
                if( check_empty( array( "authenticate" ) ) ) {
                    
                    $authenticate = $_REQUEST[ "authenticate" ];
                    
                    if ( $_SESSION["authenticate"] !== $authenticate ) {
                            $callback['msg'] = "此認證碼不正確。";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    $callback['success'] = true;
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function pause_member(){
        $callback = array();
        try{
                if( check_empty( array( "token","a_id","display" ) ) ) {
                    
                    $token = md5( $_REQUEST[ "token" ] );
                    $a_id = $_REQUEST[ "a_id" ];
                    $display = $_REQUEST[ "display" ];
                    
                    if( !in_array($display, array("block","blockade")) ){
                            $callback['msg'] = "輸入資料不完整";
                            $callback['success'] = false;
                            return $callback;
                    }
                    $DB_CON = DB_CON( DB_NAME );
                    if( !$DB_CON["success"] ){
                            return $DB_CON;
                    }
                    $con = $DB_CON["data"];

                    $Check_Admin = Check_Admin( $con , $token );
                    if( !$Check_Admin["success"] ){
                            return $Check_Admin;
                    }
                    $account = $Check_Admin["data"];
                    
                    $member = Select_Mysql($con, "account", array( "a_id" => $a_id ));
                    if( !$member ){
                            $callback['msg'] = "member is not exist";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    
                    if( update_sql($con, "account", array( "a_state" => $display ), array( "a_id" => $member[0]["a_id"] )) ){
                            $callback['data'] = $member[0]["a_id"];
                            $callback['success'] = true;
                    }
                    else{
                            $callback['msg'] = "修改失敗";
                            $callback['success'] = false;
                    }
                    
                    mysqli_close($con);
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function check_bank(){
        $callback = array();
        try{
                
                if( !check_empty( array( "token" ) ) ) {
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $token = md5( $_REQUEST[ "token" ] );
                
                $DB_CON = DB_CON( DB_NAME );
                if( !$DB_CON["success"] ){
                        return $DB_CON;
                }
                $con = $DB_CON["data"];
                
                $Check_Member = Check_Member( $con , $token );
                if( !$Check_Member["success"] ){
                        return $Check_Member;
                }
                $account = $Check_Member["data"];
                
                if( $account[0]["a_payment_method"] !== "" ){
                        $callback['data'] = "Have";
                        $callback['success'] = true;
                }
                else {
                        $callback['data'] = "YET";
                        $callback['success'] = true;
                }


                mysqli_close($con);

                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function check_info(){
        $callback = array();
        try{
                
                if( !check_empty( array( "token" ) ) ) {
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $token = md5( $_REQUEST[ "token" ] );
                
                $DB_CON = DB_CON( DB_NAME );
                if( !$DB_CON["success"] ){
                        return $DB_CON;
                }
                $con = $DB_CON["data"];
                
                $Check_Member = Check_Member( $con , $token );
                if( !$Check_Member["success"] ){
                        return $Check_Member;
                }
                $account = $Check_Member["data"];
                
                if( $account[0]["a_nickname"] === "" || $account[0]["a_phone"] === "" || $account[0]["a_country"] === "" ){
                        $callback['data'] = "YET";
                        $callback['success'] = true;
                }
                else {
                        $callback['data'] = "Have";
                        $callback['success'] = true;
                }


                mysqli_close($con);

                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

function set_random_token( $con , $account ) {
        
        $callback = array();
        $user_rememberme_token = $account["a_token"];
        while( 1 ) {
            $token = getRandom(20);
            $set_token = md5($token);
            $result = mysqli_query($con, "SELECT * FROM account WHERE a_token LIKE '%\\\"$set_token\\\"%'");
            if (mysqli_num_rows($result) == 0) {break;}
        }

        if( $user_rememberme_token === '' || $user_rememberme_token === '[]' ) {
            $set_token = array( $set_token );
            $set_token = json_encode( $set_token );
        }
        else {
            $user_rememberme_token = json_decode( $user_rememberme_token );
            if( count( $user_rememberme_token ) >= (int)10 )//MAX_NUM
            {
                $splice_num = count( $user_rememberme_token ) - (int)10 + 1;
                array_splice($user_rememberme_token, 0, $splice_num);
            }
            
            $user_rememberme_token[] = $set_token ;
            $set_token = $user_rememberme_token;
            $set_token = json_encode( $set_token );
        }

        $sql = "UPDATE account SET a_token='$set_token' WHERE a_id='".$account['a_id']."'";
        if( mysqli_query( $con , $sql ) ) {
             $callback['data'] = $token;
             $callback['success'] = true;
        }
        else {
             $callback['msg'] = "save token error";
             $callback['success'] = false;
        }
        return $callback;
}

function send_authenticate_letter( $con , $a_email ){
        
        $rand = getRandom( 6 );

        $html = '<p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
         </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">這封信是由</span><span lang="EN-US" style="color:rgb(55,96,146)">HF</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">發送的。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
        <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您收到這封郵件，是由於在</span><span style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span lang="EN-US" style="color:rgb(55,96,146)">HF</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">進行了新用戶註冊，或用戶修改</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> Email </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">使用</span><span style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">了這個郵箱地址。如果您並沒有訪問過</span><span style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">卡提諾論壇，或沒有進行上述操作，請忽略這封郵件。<wbr>您不需要退訂或進行其他進一步的操作。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
        <p class="MsoNormal"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
         <span style="background-image:initial;background-repeat:initial">------------------------------<wbr>------------------------------<wbr>----------</span>
            <br> </span><b><span style="font-size:10pt;font-family:新細明體,serif;color:black;background-image:initial;background-repeat:initial">帳號激活說明</span></b><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
         <span style="background-image:initial;background-repeat:initial">------------------------------<wbr>------------------------------<wbr>----------</span>
            <br>
            <br> </span><span lang="EN-US" style="font-family:新細明體,serif"></span> </p>
        <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">如果您是</span><span lang="EN-US" style="color:rgb(55,96,146)">HF</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">的新用戶，或在修改您的註冊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> Email </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">時使用了本地址，<wbr>我們需要對您的地址有效性進行驗證以避免垃圾郵件或地址被濫用。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
        <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您的認證碼為：</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
        <a target="_blank">'.$rand.'</a>
        <br>
        </span> </p>
        <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">感謝您的訪問，祝您使用愉快！</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
        <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">此致</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
         </span><span lang="EN-US" style="color:rgb(55,96,146)">HF</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">管理團隊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">.&nbsp;
         </span><span style="font-family:新細明體,serif;color:rgb(55,96,146)">'.http_default_path.'</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
        <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>
        <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>';

        header("Access-Control-Allow-Origin:*");
        header('Access-Control-Allow-Credentials:true');
        header('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers:Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With');
        header('Content-Type:text/html; charset=utf-8');
        require 'gmailsystem/gmail.php';
        mb_internal_encoding('UTF-8');
        $title = '[HF] Email 地址驗證';
        $callback = mstart( $a_email , $html , $title );
        
        
        if( $callback['success'] ){
            $callback["rand"] = $rand;
        }
        return $callback;
        
}

function send_forgot_authenticate_letter( $con , $a_email ){
        
        $rand = getRandom( 6 );
        
        $html = '<p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
         </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">這封信是由</span><span lang="EN-US" style="color:rgb(55,96,146)">HF</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">發送的。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
        <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif">------------------------------<wbr>------------------------------<wbr>----------<br>
        </span><b><span style="font-size:10.5pt;font-family:新細明體,serif">重要！</span></b><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"><br>
        ------------------------------<wbr>------------------------------<wbr>----------</span> </p>
       <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10.5pt;font-family:新細明體,serif">如果您沒有提交密碼重置的請求或不是</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif;color:rgb(55,96,146)">HF</span><span style="font-size:10.5pt;font-family:新細明體,serif">的註冊用戶，<wbr>請立即忽略</span><span style="font-size:10.5pt;font-family:Arial,sans-serif"> </span><span style="font-size:10.5pt;font-family:新細明體,serif">並刪除這封郵件。只有在您確認需要重置密碼的情況下，<wbr>才需要繼續閱讀下面的內容。</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"></span> </p>
       <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif">------------------------------<wbr>------------------------------<wbr>----------<br>
        </span><b><span style="font-size:10.5pt;font-family:新細明體,serif">密碼重置說明</span></b><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"><br>
        ------------------------------<wbr>------------------------------<wbr>----------</span> </p>
        <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">如果您是</span><span lang="EN-US" style="color:rgb(55,96,146)">HF</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">的新用戶，或在修改您的註冊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> Email </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">時使用了本地址，<wbr>我們需要對您的地址有效性進行驗證以避免垃圾郵件或地址被濫用。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
        <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您的認證碼為：</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
        <a target="_blank">'.$rand.'</a>
        <br>
        </span> </p>
        <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">感謝您的訪問，祝您使用愉快！</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
        <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">此致</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
         </span><span lang="EN-US" style="color:rgb(55,96,146)">HF</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">管理團隊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">.&nbsp;
         </span><span style="font-family:新細明體,serif;color:rgb(55,96,146)">'.http_default_path.'</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
        <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>
        <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>';
        
        header("Access-Control-Allow-Origin:*");
        header('Access-Control-Allow-Credentials:true');
        header('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers:Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With');
        header('Content-Type:text/html; charset=utf-8');
        require 'gmailsystem/gmail.php';
        mb_internal_encoding('UTF-8');
        $title = '[HF] Email 地址驗證';
        $callback = mstart( $a_email , $html , $title );
        
        
        if( $callback['success'] ){
            $callback["rand"] = $rand;
        }
        return $callback;
        
}
?>
