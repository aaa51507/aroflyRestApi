<?php	
        /*
        * @file json_mgm_account.php
        * @brief TABLE:account

        * detail 

        * @author arod ( howareu520@gmail.com )
        * @date 2016-08-30 */

        include 'config.php';
        include 'global.php';
        
        $func = $_REQUEST["func"];

        switch ($func) {
            case "fn_btn_search_regex":
                $echo = fn_btn_search_regex();
                break;
            case "fn_delete_account":
                $echo = fn_delete_account();
                break;
            case "fn_read_account_log":
                $echo = fn_read_account_log();
                break;
            case "fn_update_account_log":
                $echo = fn_update_account_log();
                break;
            case "fn_btn_search_condition_query":
                $echo = fn_btn_search_condition_query();
                break;
            case "fn_btn_pause_account":
                $echo = fn_btn_pause_account();
                break;
            case "fn_btn_checkbox_pause_account":
                $echo = fn_btn_checkbox_pause_account();
                break;
            case "fn_btn_checkbox_continue_account":
                $echo = fn_btn_checkbox_continue_account();
                break;
            case "fn_btn_checkbox_send_account_letter": //寄email給某些特定會員
                $echo = fn_btn_checkbox_send_account_letter();
                break;
            case "fn_btn_all_send_account_letter": //寄email給全部會員
                $echo = fn_btn_all_send_account_letter();
                break;
        }
        echo json_encode($echo);
        
    
    function fn_btn_search_regex(){
        $callback = array();
        try{    
                $cart = array();
                
                $operation_html_block = empty($_REQUEST[ "operation_html_block" ]) ? "" : $_REQUEST["operation_html_block"];
                $operation_html_blockade = empty($_REQUEST[ "operation_html_blockade" ]) ? "" : $_REQUEST["operation_html_blockade"];
                
//                if( !check_empty( array("token" ) ) ) {
//                        $callback['msg'] = "輸入資料不完整";
//                        $callback['success'] = false;
//                        return $callback;
//                }
                
                $length = $_REQUEST['length'];
                $order = $_REQUEST['order'];
                $search = $_REQUEST['search'];
                $start = $_REQUEST['start'];
                
                $order_Arr = array("a.a_id", "a.a_email", "a.a_nickname", "a.a_sex", "a.a_age", "a.ac_id", "a.aj_id", "a.a_last_login_time", "a.a_admin", "a.a_state");
                $order_str = " ORDER BY ".$order_Arr[ (int)$order[0]["column"] ]." ".$order[0]["dir"];
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $token = md5( $_REQUEST[ "token" ] );
                
                $Check_Admin = Check_Admin( $con , $token );
                if( ! $Check_Admin['success'] ){
                        $callback['msg'] = "you dont have admin";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                
                //搜尋
                $condition = "WHERE a.a_delete != 'blockade' ";
                if( !empty($_REQUEST[ "email" ]) ){//信箱
                        $condition .= ($condition == "")? "WHERE a.a_email like '%".$_REQUEST[ "email" ]."%' " : " AND a.a_email like '%".$_REQUEST[ "email" ]."%' ";
                }
                if( !empty($_REQUEST[ "nickname" ]) ){//暱稱
                        $condition .= ($condition == "")? "WHERE a.a_nickname like '%".$_REQUEST[ "nickname" ]."%' " : " AND a.a_nickname like '%".$_REQUEST[ "nickname" ]."%' ";
                }
                if( !empty($_REQUEST[ "admin" ]) ){//暱稱
                        $condition .= ($condition == "")? "WHERE a.a_admin like '".$_REQUEST[ "admin" ]."' " : " AND a.a_admin like '".$_REQUEST[ "admin" ]."' ";
                }
                if( !empty($_REQUEST[ "state" ]) ){//狀態
                        $condition .= ($condition == "")? "WHERE a.a_state = '".$_REQUEST[ "state" ]."' " : " AND a.a_state = '".$_REQUEST[ "state" ]."' ";
                }
                if( !empty($_REQUEST[ "city" ]) ){//居住地
                        $condition .= ($condition == "")? "WHERE a.ac_id = ".$_REQUEST[ "city" ]." " : " AND a.ac_id = ".$_REQUEST[ "city" ]." " ;
                }
                if( !empty($_REQUEST[ "job" ]) ){//行業別
                        $condition .= ($condition == "")? "WHERE a.aj_id = ".$_REQUEST[ "job" ]." " : " AND a.aj_id = ".$_REQUEST[ "job" ]." " ;
                }
                
                $this_month = date('Y-m', strtotime('this month') );
                $res = mysqli_query($con, "SELECT a.a_id, a.a_email, a.a_nickname, a.a_sex, a.a_age, ac.ac_name, aj.aj_name, a.a_last_login_time, a.a_registration_time, a.a_admin, a.a_state "
                                        . "FROM account as a LEFT JOIN account_city as ac on ac.ac_id = a.ac_id LEFT JOIN account_job as aj on aj.aj_id = a.aj_id "
                                        . $condition
                                        . "$order_str LIMIT $start, $length");

                if (mysqli_num_rows($res) > 0) {

                        while($row = mysqli_fetch_array($res)) {
                                $a_admin = ($row['a_admin'] == 'true') ? '管理者':'會員';
                                $a_state = ($row['a_state'] == 'block') ? '啟用':'停用';

                                $a_registration_time = date('Y-m-d',  strtotime($row['a_registration_time']));
                                $lastlogin = date('Y-m-d',  strtotime($row['a_last_login_time']));
                                $a_last_login_time = $lastlogin == "1970-01-01" ? "無" : date('Y-m-d',  strtotime($row['a_last_login_time']));

                                $cart[] = array(
                                    "0" => $row['a_id'],
                                    "1" => $row['a_email']!==""? '<a href="javascript:;">'.$row['a_email'].'</a>' : '' ,
                                    "2" => $row['a_nickname'],
                                    "3" => $row['a_sex']=='male'?'男':'女',
                                    "4" => date('Y') - $row['a_age'],
                                    "5" => $row['ac_name'],
                                    "6" => $row['aj_name'],
                                    "7" => $a_last_login_time . "<br>" . $a_registration_time,
                                    "8" => $a_admin,
                                    "9" => $a_state,
                                    "10" => ($row['a_state']=='block'?stripslashes($operation_html_block):stripslashes($operation_html_blockade))
                                );
                        }


                        $callback['data'] =  $cart;

                        $total_num = mysqli_query($con, "SELECT FOUND_ROWS()");
                        $total_num = mysqli_fetch_array($total_num);
                        $callback["recordsTotal"] = $total_num["FOUND_ROWS()"];
                        $callback["recordsFiltered"] = $total_num["FOUND_ROWS()"];
                        
                        mysqli_close($con);
                }
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }
    
    function fn_delete_account(){
        $callback = array();
        try{
                if( check_empty( array( "a_id" ) ) ) {//"token",

//                    $token = md5( $_REQUEST[ "token" ] );
                    $a_id = $_REQUEST[ "a_id" ];

                    $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                    $con->query("SET NAMES utf8");
                    // Check connection
                    if (mysqli_connect_errno()) {
                            $callback['msg'] = "SQL connect fail";
                            $callback['success'] = false;
                            return $callback;
                    }

//                    $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
//                    if( !$account ) {
//                            $callback['msg'] = "Login fail";
//                            $callback['success'] = false;
//                            mysqli_close($con);
//                            return $callback;
//                    }
//                    if( $account[0]['a_admin'] !== "true" ){
//                            $callback['msg'] = "you dont have admin";
//                            $callback['success'] = false;
//                            mysqli_close($con);
//                            return $callback;
//                    }

                    $member = get_sql($con, "account" , "WHERE a_id = '$a_id'");
                    if( !$member ){
                            $callback['msg'] = "member is not exist";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }

                    if(mysqli_query($con, "UPDATE account SET a_delete='blockade', a_state='blockade' WHERE a_id='$a_id'") ){
                            $callback['success'] = true;
                    }
                    else{
                            $callback['msg'] = "修改失敗";
                            $callback['success'] = false;
                    }

                    mysqli_close($con);
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }

        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
    }

    function fn_read_account_log(){
        $callback = array();
        try{
                if( !check_empty( array( "a_id" ) ) ) {// "token" ,
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
                $token = md5( $_REQUEST[ "token" ] );
                $a_id = $_REQUEST[ 'a_id' ];

                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");

                $cart = array();

                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }

//                $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
//                if( !$account ) {
//                        $callback['msg'] = "Login fail";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }
//                if( $account[0]['a_admin'] !== "true" ){
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }
//                echo "SELECT a.a_id, a.a_age, a.a_email, a.a_nickname, a.a_sex, ac.ac_name, a.ac_name, aj.aj_name, a.a_last_login_time, a.a_registration_time, a.a_admin, a.a_state "
//                                        . "FROM account as a "
//                                        . "LEFT JOIN account_city as ac on ac.ac_id = a.ac_id "
//                                        . "LEFT JOIN account_job as aj on aj.aj_id = a.aj_id "
//                                        . "WHERE a.a_id='$a_id'";
                $res = mysqli_query($con, "SELECT a.a_id, a.a_age, a.a_email, a.a_nickname, a.a_sex, ac.ac_name, aj.aj_name, a.a_last_login_time, a.a_registration_time, a.a_admin, a.a_state "
                                        . "FROM account as a "
                                        . "LEFT JOIN account_city as ac on ac.ac_id = a.ac_id "
                                        . "LEFT JOIN account_job as aj on aj.aj_id = a.aj_id "
                                        . "WHERE a.a_id='$a_id'");

                while($row = mysqli_fetch_array($res,MYSQLI_ASSOC)) {
                        $row['a_age'] = date('Y') - date('Y',  strtotime($row['a_age']));
                        $cart = $row;
                }

                $callback['data'] =  $cart;
                $callback['success'] = true;

                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
    
    }
    
    function fn_update_account_log(){
        $callback = array();
        try{
                if( !check_empty( array( "a_id" , "a_admin" , "a_email" , "a_nickname" , "a_sex" , "a_age" , "a_city" , "a_job" ) ) ) {// "token" ,
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
                
//                $token = md5( $_REQUEST[ "token" ] );
                $a_id = $_REQUEST[ 'a_id' ];
		
		$a_admin = $_REQUEST[ 'a_admin' ];
                $a_email = $_REQUEST[ 'a_email' ];
                $a_nickname = $_REQUEST[ 'a_nickname' ];
                $a_sex = $_REQUEST[ 'a_sex' ];
                $a_age = date('Y')-(int)$_REQUEST[ 'a_age' ];
                $a_city = $_REQUEST[ 'a_city' ];
                $a_job = $_REQUEST[ 'a_job' ]+0;
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
//                $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
//                if( !$account ) {
//                        $callback['msg'] = "Login fail";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }
//                if( $account[0]['a_admin'] !== "true" ){
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }
                
                $sql_cmd = "UPDATE " ."account" . " SET 
                                    a_admin='$a_admin',
                                    a_email='$a_email',
                                    a_nickname='$a_nickname',
                                    a_sex='$a_sex',
                                    a_age=$a_age,
                                    ac_id=$a_city,
                                    aj_id=$a_job
                                    WHERE a_id='$a_id'";

                        if( mysqli_query($con, $sql_cmd) ) {
                                $callback['success'] = true;
                        }
                        else {
                                $callback['msg'] = "UPDATE fail";
                                $callback['success'] = false;
                        }
                        
                        mysqli_close($con);
                     
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
    
    }
    
    function fn_btn_update_account_kind_single_profile(){
        $callback = array();
        try{
                //date_default_timezone_set('Asia/Taipei');
                
                $token = md5( $_REQUEST[ "token" ] );
                $a_id = $_REQUEST[ 'a_id' ];

                $a_kind = $_REQUEST[ 'a_kind' ];
                
                //$b_date = date('Y-m-d H:i:s');
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                                
                $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
                if( !$account ) {
                        $callback['msg'] = "Login fail";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                    
                $sql_cmd = "UPDATE account_profile SET a_kind=$a_kind WHERE a_id='$a_id'";

                        if( mysqli_query($con, $sql_cmd) ) {
                                $callback['success'] = true;
                        }
                        else {
                                $callback['msg'] = "UPDATE fail";
                                $callback['success'] = false;
                        }
                        
                        mysqli_close($con);
                     
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
    
    }
    
    function fn_btn_search_condition_query(){
        $callback = array();
        try{       
                $search_cmd = $_REQUEST[ 'search_cmd' ];
                $search_cmd = json_decode($search_cmd,true);

                $i = 0;
                $cart = array();
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");

                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                /*點選 搜尋快捷鍵 進入SQL搜尋data*/
                foreach ($search_cmd as $key => $value) {
                        if( $key== 0){
                            foreach ($value as $key2 => $value2) {
                                if( $key2 == 'a_points' || $key2 == 'a_vitality' || $key2 == 'a_limit_token'){
                                    $condition = (string)$key2 .' LIKE '. '%'.$value2.'%';
                                } else {
                                    $condition = (string)$key2 .' LIKE '. '\'%'.$value2.'%\'';
                                }
                            }
                        } else {
                            foreach ($value as $key2 => $value2) {
                                if( $key2 == 'a_points' || $key2 == 'a_vitality' || $key2 == 'a_limit_token'){
                                    $condition .= ' AND ' . (string)$key2 .' LIKE '. '%'.$value2.'%';
                                } else {
                                    $condition .= ' AND ' . (string)$key2 .' LIKE '. '\'%'.$value2.'%\'';
                                }
                            }
                        }
                }

                $result = mysqli_query($con, "SELECT * FROM "."account" ." WHERE $condition");
                if ( mysqli_num_rows($result) > 0) {

                        while ($row = mysqli_fetch_assoc($result)) {
                                $cart[$i] = array();
                                foreach ($row as $key => $value) {

                                        $cart[$i][$key] = $value;

                                }
                                $i++;
                        }
                        $callback['data'] = $cart;
                        $callback['success'] = true;

                } else {
                        $callback['success'] = false;
                }

                mysqli_close($con);

        }
        catch (Exception $e){
                $callback['msg'] = $e;
                $callback['success'] = false;
        }

        return $callback;
}

    function fn_btn_pause_account(){
        $callback = array();
        try{
                if( !check_empty( array( "a_id","display" ) ) ) {//"token",
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
//                $token = md5( $_REQUEST[ "token" ] );
                $a_id = $_REQUEST[ "a_id" ];
                $display = $_REQUEST[ "display" ];

                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }

//                $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
//                if( !$account ) {
//                        $callback['msg'] = "Login fail";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }
//                if( $account[0]['a_admin'] !== "true" ){
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }

                $member = get_sql($con, "account" , "WHERE a_id = '$a_id'");
                if( !$member ){
                        $callback['msg'] = "member is not exist";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }

                if( update_sql($con, "account" , array( "a_state" => $display ), array( "a_id" => $member[0]["a_id"] )) ){
                        $callback['data'] = $member[0]["a_id"];
                        $callback['success'] = true;
                }
                else{
                        $callback['msg'] = "修改失敗";
                        $callback['success'] = false;
                }

                mysqli_close($con);
                    
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
        
}

    function fn_btn_checkbox_pause_account(){
        $callback = array();
        try{
                if( check_empty( array( "token","list" ) ) ) {

                    $token = md5( $_REQUEST[ "token" ] );
                    $list = json_decode($_REQUEST[ "list" ],true);

                    $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                    $con->query("SET NAMES utf8");
                    // Check connection
                    if (mysqli_connect_errno()) {
                            $callback['msg'] = "SQL connect fail";
                            $callback['success'] = false;
                            return $callback;
                    }

                    $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
                    if( !$account ) {
                            $callback['msg'] = "Login fail";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    if( $account[0]['a_admin'] !== "true" ){
                            $callback['msg'] = "you dont have admin";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }

                    $cond = "";
                    foreach ($list as $key => $value) {
                            $cond .= ( $key === 0 ) ? "a_id='$value'" : " OR a_id='$value'";
                    }

                    if( mysqli_query($con, "UPDATE " ."account" . " SET a_state='blockade' WHERE $cond") ){
                            $callback['data'] = $list;
                            $callback['success'] = true;
                    }
                    else{
                            $callback['msg'] = "修改失敗";
                            $callback['success'] = false;
                    }

                    mysqli_close($con);
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }

        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;

    }

    function fn_btn_checkbox_continue_account(){
        $callback = array();
        try{
                if( check_empty( array( "token","list" ) ) ) {

                    $token = md5( $_REQUEST[ "token" ] );
                    $list = json_decode($_REQUEST[ "list" ],true);

                    $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                    $con->query("SET NAMES utf8");
                    // Check connection
                    if (mysqli_connect_errno()) {
                            $callback['msg'] = "SQL connect fail";
                            $callback['success'] = false;
                            return $callback;
                    }

                    $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
                    if( !$account ) {
                            $callback['msg'] = "Login fail";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }
                    if( $account[0]['a_admin'] !== "true" ){
                            $callback['msg'] = "you dont have admin";
                            $callback['success'] = false;
                            mysqli_close($con);
                            return $callback;
                    }

                    $cond = "";
                    foreach ($list as $key => $value) {
                            $cond .= ( $key === 0 ) ? "a_id='$value'" : " OR a_id='$value'";
                    }

                    if( mysqli_query($con, "UPDATE " ."account" . " SET a_state='block' WHERE $cond") ){
                            $callback['data'] = $list;
                            $callback['success'] = true;
                    }
                    else{
                            $callback['msg'] = "修改失敗";
                            $callback['success'] = false;
                    }

                    mysqli_close($con);
                }
                else {
                    $callback['msg'] = "輸入資料不完整";
                    $callback['success'] = false;
                }

        }
        catch (Exception $e){
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;

    }

    function fn_hichart_click_list_search(){
        $callback = array();
        try{       
                $i = 0;
                $cart = array();

                if( !check_empty( array( "timepoint","token" ) ) ) {
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }

                $timepoint = $_REQUEST[ 'timepoint' ] /1000;
                $token = md5( $_REQUEST[ "token" ] );

                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");

                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }

                $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
                if( !$account ) {
                        $callback['msg'] = "Login fail";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                if( $account[0]['a_admin'] !== "true" ){
                        $callback['msg'] = "you dont have admin";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }

                $result = mysqli_query($con, "SELECT * FROM " ."account" . " WHERE DATE_FORMAT(a_registration_time, '%Y-%m-%d') = FROM_UNIXTIME('".$timepoint."')");
                if ( mysqli_num_rows($result) > 0) {

                        while ($row = mysqli_fetch_assoc($result)) {
                                $cart[$i] = array();
                                foreach ($row as $key => $value) {

                                        $cart[$i][$key] = $value;

                                }
                                $i++;
                        }
                        $callback['data'] = $cart;
                        $callback['success'] = true;

                } else {
                        $callback['success'] = false;
                }

                mysqli_close($con);

        }
        catch (Exception $e){
                $callback['msg'] = $e;
                $callback['success'] = false;
        }

        return $callback;
    }

    function fn_btn_checkbox_send_account_letter(){
        $callback = array();
        $cart = array();
        try{

                if( !check_empty( array( "token" , "a_id" , "title" , "content" ) ) ){
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }


                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                date_default_timezone_set('Asia/Taipei');
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }

                $token = md5( $_REQUEST[ "token" ] );
                $a_id = json_decode($_REQUEST[ "a_id" ],true);
                $title = mysqli_real_escape_string($con,$_REQUEST[ "title" ]);
                $content = mysqli_real_escape_string($con,$_REQUEST[ "content" ]);

                $account = get_sql($con, "account", "WHERE a_token LIKE '%\\\"$token\\\"%'");
                if( !$account ) {
                        $callback['msg'] = "Login fail";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                if( $account[0]['a_admin'] !== "true" ){
                        $callback['msg'] = "you dont have admin";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }

                foreach ($a_id as $key => $value) {
                        $a_id[$key] = "'".$value."'";
                }
                $a_id = implode(',', $a_id);

                $member = get_sql($con, "account", "WHERE a_id IN (".$a_id.")" , "a_email");

                if( $member ){
                        $callback = send_authenticate_letter( $con , $account[0]['a_email'] , $member , $title , $content );
                }

                mysqli_close($con);


        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
    }

    function fn_btn_all_send_account_letter(){
        $callback = array();
        $cart = array();
        try{

                if( !check_empty( array( "token" , "title" , "content" ) ) ){
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }


                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                date_default_timezone_set('Asia/Taipei');
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }

                $token = md5( $_REQUEST[ "token" ] );
                $title = !empty($_REQUEST[ "title" ]) ? mysqli_real_escape_string($con,$_REQUEST[ "title" ]) : '';
                $content = !empty($_REQUEST[ "content" ]) ? mysqli_real_escape_string($con,$_REQUEST[ "content" ]) : '';

                $account = get_sql($con, "account", "WHERE a_token LIKE '%\\\"$token\\\"%'");
                if( !$account ) {
                        $callback['msg'] = "Login fail";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                if( $account[0]['a_admin'] !== "true" ){
                        $callback['msg'] = "you dont have admin";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }

                $member = get_sql($con, "account", "WHERE a_email != ''", "a_email");
                if( $member ){
                        $callback = send_authenticate_letter( $con , $account[0]['a_email'] , $member , $title , $content );
                }

                mysqli_close($con);


        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;
    }

    function send_authenticate_letter( $con , $a_email_admin , $a_email , $title , $content ){

        while( true ) {
                $email_token = getRandom( 20 );
                $result = mysqli_query($con, "SELECT * FROM account WHERE a_email_confirm='$email_token'");
                if (mysqli_num_rows($result) === 0) {
                        break;
                }
        }

        $html = '<div class="ii gt adP adO" id=":lq">
                        <div class="a3s aXjCH m155062df580309d9" id=":17n">
                                <div bgcolor="#f3f1ee">
                                        <table width="660" cellspacing="1" cellpadding="10" border="0" bgcolor="#e0ddd5" align="center">
                                                <tbody>
                                                        <tr>
                                                                <td bgcolor="#FFFFFF">
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody>
                                                                                        <tr>
                                                                                                <td><img src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQPC_e3_-_Tx8A3Hp0XcmHCPxGQ0dI9AUTVmc13DLYDASWBeFe3vg" class="CToWUd"></td>
                                                                                        </tr>
                                                                                </tbody>
                                                                        </table>
                                                                        <table width="100%" cellspacing="0" cellpadding="20" border="0" style="font-family:Verdana,\'\0065b0\007d30\00660e\009ad4\';font-size:12px;line-height:20px;color:rgb(102,102,102)">
                                                                                <tbody>
                                                                                        <tr>
                                                                                                <td>
                                                                                                        <p style="color:rgb(193,73,72);font-weight:bold">此為系統自動通知信，請勿直接回信！</p>
                                                                                                        <div style="font-family:Verdana,\'\0065b0\007d30\00660e\009ad4\';font-size:12px">
                                                                                                                <p>親愛的會員您好：</p>
                                                                                                                <p>'.$content.'</p>
                                                                                                                <p>網路客服</p>
                                                                                                        </div>
                                                                                                </td>
                                                                                        </tr>
                                                                                </tbody>
                                                                        </table>
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Verdana,\'\0065b0\007d30\00660e\009ad4\';font-size:12px;line-height:20px;color:rgb(102,102,102)">
                                                                                <tbody>
                                                                                        <tr>
                                                                                                <td height="2" bgcolor="#ebe8e3"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                                <td height="30" align="center"><a>http://www.funbook19.com.tw</a>&nbsp;&nbsp;<wbr></wbr>funbook19網路客服</td>
                                                                                        </tr>
                                                                                </tbody>
                                                                        </table>
                                                                </td>
                                                        </tr>
                                                </tbody>
                                        </table>
                                </div>
                        </div>
                </div>';

        header("Access-Control-Allow-Origin:*");
        header('Access-Control-Allow-Credentials:true');
        header('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers:Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With');
        header('Content-Type:text/html; charset=utf-8');
        require './gmailsystem/gmail.php';
        mb_internal_encoding('UTF-8');
//        $title = '[Funbook19] 網路客服';

        $callback = mstart_scr( $a_email_admin , $a_email , $html , $title );


        if( $callback['success'] ){
            $callback["a_email_confirm"] = $email_token;
        }
        return $callback;

    }
?>
