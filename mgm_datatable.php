<?php header("X-Frame-Options: DENY");?>
<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale = 1.0, user-scalable = 0" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="app-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="apple-touch-fullscreen" content="yes" />
        <link rel='shortcut icon' href='template/images/favicon.ico' type='x-icon'>
        <title>AROFLY | Account</title>
        <meta name="description" content="What you see what you get Enjoy to Interactive with living objects">

        <link class="icon" href="../icon/logo.png" rel="apple-touch-icon-precomposed" />
        <link class="icon" href="../icon/logo.png" rel="SHORTCUT ICON" />
        
        <link rel="stylesheet" href="css/all_css.css">
        <link rel="stylesheet" href="css/global.css">
        
        <?php include( "js/all_js.php"); ?>
        <script src="js/jquery.pagination.js"></script>
        <script src="js/arod/management_account.jquery.pagination.js"></script>
        <script>
                $.global_date_statistics_column = "vip"; //table:date_statistics > col:vip
        </script>
        <!--script src="js/search.js"></script-->
        <script src="js/batch.js"></script>

        <!-- include HIGHCHARTS-->
        <script src="js/highstock.js"></script>
        <script src="js/mgm_highchart_jack.js"></script>
        <script src="js/mgm_hichart_click_jack.js"></script>
        <!-- for arod edit function (點擊折線圖的點會呼叫的fun)-->

        <!-- datePicker -->
        <script src="js/jquery-ui.js"></script>

        <!-- jquery.dataTables -->
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="js/dataTablesPlugin.js"></script>

        <script>
                
                $(document).ready(function() {

                });


                function init() {
                        
                        $.initDatatable_1();
                        
                        $("#btn_Datatable").unbind('click').bind('click', function() {
                                $.initDatatable_1();
                        });
                        
                        $("#btn_checkbox_block_new_account").unbind('click').bind('click', function() {
                                console.log($('#example').data("rows_selected"));
                                console.log( 'btn_checkbox_block_new_account' );
                                $( "#add_account_modal" ).modal( "show" );
                                
                        });
                        
                        $("#add_account_modal_Yes").unbind('click').bind('click', function() {
                                
                                $(".alert").hide();
                                var bool = true;
                                var msg = "",pos,input_type;
                                $.each( $("#container input.necessary") , function( index , value ){
                                        input_type = $( value ).attr( "type" );
                                        if( input_type === "email" ){
                                                if( !email_event( $( value ) ) ){
                                                        bool = false;
                                                        pos = $( value );
                                                }
                                        }
                                        else if( input_type === "password" ){
                                                if( !password_event( $( value ) ) ){
                                                        bool = false;
                                                        pos = $( value );
                                                        $( value ).val("");
                                                        $( "#account_pwd2" ).val("");
                                                }
                                        }
                                        else{
                                                if( !input_event( $( value ) ) ){
                                                        bool = false;
                                                        pos = $( value );
                                                }
                                        }

                                });

                                if( !check_password_event( $( "#account_pwd2" ) ) ){
                                        bool = false;
                                        pos = $( "#account_pwd2" );
                                        $( "#account_pwd" ).val("");
                                        $( "#account_pwd2" ).val("");
                                }

                                if( !select_event( $( "#account_country" ) ) ){
                                        bool = false;
                                        pos = $( "#account_country" );
                                }
                                
                                if( bool ) {
                                    loading_ajax_show();
                                    var data = {
                                                token:      getCookie( "scs_cookie" ),
                                                a_email:      $( "#account_email" ).val(),
                                                a_password:   md5( $( "#account_pwd" ).val() ),
                                                a_nickname:       $( "#account_nickname" ).val(),
                                                a_country:     $( "#account_country" ).val(),
                                                a_eighteen:     $( "#account_eighteen" ).is( ":checked" ),
                                                a_admin:     $( "#account_admin" ).is( ":checked" )
                                    };
                                    var success_back = function( data ) {

                                            data = JSON.parse( data );
                                            console.log(data);
                                            loading_ajax_hide();
                                            if( data.success ) {
                                                show_remind( "新增成功" );
                                                $.initDatatable_1();
                                                $( "#add_account_modal" ).modal( "hide" );
                                            }
                                            else {
                                                show_remind( data.msg , "error" );
                                                if( data.action === "email" ){
                                                        $( "#account_email" ).parent().find(".alert").hide();
                                                        $( "#account_email" ).parent().find(".alert.error2").show();
                                                        scrollto( $( "#account_email" ) );
                                                }
                                            }

                                    }
                                    var error_back = function( data ) {
                                            console.log(data);
                                    }
                                    $.Ajax( "POST" , "../php/member.php?func=add_by_admin" , data , "" , success_back , error_back);

                                }
                                else {
                                    re_captcha();
                                    show_remind( msg , "error" );
                                    scrollto( pos );
                                }
                                    
                        });
                        
                        $("#btn_checkbox_block_email_account").unbind('click').bind('click', function() {
                                    console.log($('#example').data("rows_selected"));
                                    console.log( 'btn_checkbox_block_email_account' );
                                    
                                    $( "#dialogue_email_account_user" ).html( '注意：確定送出此 ' + $('#example').data("rows_selected").length + ' 位使用者' );
                                    
                                    $("#dialogue_email_account").modal("show");
                                    
                                    $("#dialogue_email_account_main_Yes").unbind('click').bind('click', function() {
                                                loading_ajax_show();
                                                var data = {
                                                        a_id    : JSON.stringify( $('#example').data("rows_selected") ) ,
                                                        title   : $( "#dialogue_email_account_main" ).val() ,
                                                        content : $( "#dialogue_email_account_contnet" ).val() ,
                                                        token:   getCookie( "scs_cookie" )
                                                };
                                                var success_back = function( data ) {
                                                        
                                                        loading_ajax_hide();
                                                        data = JSON.parse( data );
                                                        console.log(data);
                                                        if( data.success ) {
                                                                show_remind("送出成功", "success");
                                                                $( "#dialogue_email_account" ).modal("hide");
                                                        }
                                                        else {
                                                                show_remind( data.msg , "error" );
                                                        }

                                                }
                                                var error_back = function( data ) {
                                                        console.log(data);
                                                }
                                                $.Ajax( "GET" , "php/json_mgm_account.php?func=fn_btn_checkbox_send_account_letter" , data , "" , success_back , error_back);

                                        
                                    });
                                    
                        });
                        
                        $("#btn_checkbox_block_emailall_account").unbind('click').bind('click', function() {
                                    console.log($('#example').data("rows_selected"));
                                    console.log( 'btn_checkbox_block_emailall_account' );
                                    
                                    $( "#dialogue_email_account_user" ).html( '注意：為防止會員名單外洩，將用密件副本傳送，確定送出全部會員' );
                                    
                                    $("#dialogue_email_account").modal("show");
                                    
                                    $("#dialogue_email_account_main_Yes").unbind('click').bind('click', function() {
                                                loading_ajax_show();
                                                var data = {
                                                        title   : $( "#dialogue_email_account_main" ).val() ,
                                                        content : $( "#dialogue_email_account_contnet" ).val() ,
                                                        token:   getCookie( "scs_cookie" )
                                                };
                                                var success_back = function( data ) {
                                                        loading_ajax_hide();
                                                        data = JSON.parse( data );
                                                        console.log(data);
                                                        if( data.success ) {
                                                                $( "#dialogue_email_account" ).modal("hide");
                                                                show_remind("送出成功", "success");
                                                        }
                                                        else {
                                                                show_remind( data.msg , "error" );
                                                        }

                                                }
                                                var error_back = function( data ) {
                                                        console.log(data);
                                                }
                                                $.Ajax( "GET" , "php/json_mgm_account.php?func=fn_btn_all_send_account_letter" , data , "" , success_back , error_back);

                                        
                                    });
                                    
                        });
                        
                        $("#myModalEditAccount").delegate("#myModalEditAccount_Yes", "click", function() {
                                $.a_id = $("#myModalEditAccount_Yes").attr('a_id');
                                fn_update_account_log();
                        });
                }
                
                $.initDatatable_1 = function initDatatable_1() {

                        //destory dataTable
                        $("#example").DataTable().destroy();
                        $("#datatable").html("");

                        var column = ["ID", "InExUser", "UserName", "Password", "SystemLocation", "Remarks", "CreateDate Time<br>UpdateDate Time", "CreateBy", "Status", ""];

                        createSearchTable('#search_Datatable', '#example', column);

                        var ajax = 'php/json_mgm_account.php?func=fn_btn_search_regex&' +
                                'token='+getCookie("scs_cookie")+'&' +
                                'email='+$("#email").val()+'&' +
                                'nickname='+$("#nickname").val()+'&' +
                                'admin='+$("#admin").val()+'&' +
                                /*'city='+$("#city").val()+'&' +
                                'job='+$("#job").val()+'&' +
                                'state='+$("#state").val()+'&' +*/
                                'operation_html_block=<div class="center"><a id="pause_member" class="mark msg_box">Pause</a><br><a class="mark msg_box page_delete" id="delete_member">Delete</a></div>&' +
                                'operation_html_blockade=<div class="center"><a id="pause_member" class="mark msg_box orange_btn">Resume</a><br><a class="mark msg_box page_delete" id="delete_member">Delete</a></div>';


                        var order = [
                                [1, 'desc']
                        ];
                        
                        createTable('#datatable', '#example', column);
                        createDataTablePipeline('#example', ajax, order, false, [1]);
                        $('#example').data("rows_selected", []);
                        addEvent('#datatable', '#example');
                        
                        //修改會員資料 click id=pencil 
                        $('#example tbody').on('click', ' tr td:nth-child(2) a', function(e) {
                                var $row = $(this).closest('tr');
                                // Get row data
                                var data = $('#example').DataTable().row($row).data();
                                // Get row ID
                                $.a_id = data[0];

                                fn_read_account_log();
                        });
                        //暫停/恢復會員 click id=pause_member
                        $('#example tbody').on('click', '[id=pause_member]', function(e) {
                                var $row = $(this).closest('tr');
                                // Get row data
                                var data = $('#example').DataTable().row($row).data();
                                // Get row ID
                                $.a_id = data[0];

                                $.display = ($(this).hasClass('orange_btn')) ? 'block' : 'blockade';
                                fn_btn_pause_account();
                        });
                        //刪除(隱藏)/停權會員 click id=delete_member
                        $('#example tbody').on('click', '[id=delete_member]', function(e) {
                                var $row = $(this).closest('tr');
                                // Get row data
                                var data = $('#example').DataTable().row($row).data();
                                // Get row ID
                                $.a_id = data[0];
                                $("#myModaldelete").modal('show');
                                $("#myModaldelete_yes").unbind('click').bind('click', function() {
                                        fn_delete_account();
                                });
                        });

                }



                //暫停/恢復會員權限(單一)
                function fn_delete_account() {
                        var data = {
                                token: getCookie("scs_cookie"),
                                a_id: $.a_id,
                                display: $.display
                        };
                        var success_back = function(data) {
                                data = JSON.parse(data);
                                console.log(data);
                                if (data.success) {
                                        show_remind("刪除成功");
                                        $.initDatatable_1();
                                        
                                } else {
                                        show_remind("更改失敗");
                                }
                        };
                        var error_back = function(data) {
                                console.log(data);
                        };
                        $.Ajax("POST", "php/json_mgm_account.php?func=fn_delete_account", data, "", success_back, error_back);
                }

                //暫停/恢復會員權限(單一)
                function fn_btn_pause_account() {
                        var data = {
                                token: getCookie("scs_cookie"),
                                a_id: $.a_id,
                                display: $.display
                        };
                        var success_back = function(data) {
                                data = JSON.parse(data);
                                console.log(data);
                                if (data.success) {
                                        console.log(this);
                                        show_remind("更改成功");
                                        $.initDatatable_1();
                                } else {
                                        show_remind("更改失敗");
                                }
                        };
                        var error_back = function(data) {
                                console.log(data);
                        };
                        $.Ajax("POST", "php/json_mgm_account.php?func=fn_btn_pause_account", data, "", success_back, error_back);
                }
                
                //會員資訊Dialog
                function fn_read_account_log() {

                        var data = {
                                token: getCookie("scs_cookie"),
                                a_id: $.a_id
                        };
                        var success_back = function(data) {

                                console.log(data);
                                var tmp_franchisee = "";
                                var tmp_kind = "";
                                data = JSON.parse(data);
                                console.log(data);
                                if (data.success) {

                                        $("#myModalEditAccount_Yes").attr('a_id', data.data.a_id);

                                        $("#Edit_a_admin").val(data.data.a_admin);
                                        
                                        $("#Edit_a_email").val(data.data.a_email);
                                        $("#Edit_a_nickname").val(data.data.a_nickname);
                                        $("#Edit_a_sex").val(data.data.a_sex);
                                        $("#Edit_a_age").val(data.data.a_age);
                                        $("#Edit_a_city").val(data.data.a_city);
                                        $("#Edit_a_job").val(data.data.a_job);
                                        $("#Edit_a_last_login_time").val(data.data.a_last_login_time);
                                        $("#Edit_a_registration_time").val(data.data.a_registration_time);
                                
                                        $("#myModalEditAccount").modal("show");

                                } else {
                                        $("#myModalEditAccount").modal("hide");
                                }

                        };
                        var error_back = function(data) {
                                console.log(data);
                        };
                        $.Ajax("POST", "php/json_mgm_account.php?func=fn_read_account_log", data, "", success_back, error_back);

                }
                
                //修改會員資料
                function fn_update_account_log() {

                        var data = {
                                token: getCookie("scs_cookie"),
                                a_id: $.a_id,
                                a_admin: $("#Edit_a_admin").val(),

                                a_email: $("#Edit_a_email").val(),
                                a_nickname: $("#Edit_a_nickname").val(),
                                a_sex: $("#Edit_a_sex").val(),
                                a_age: $("#Edit_a_age").val(),
                                a_city: $("#Edit_a_city").val(),
                                a_job: $("#Edit_a_job").val(),
                        };
                        var success_back = function(data) {
                                data = JSON.parse(data);
                                console.log(data);
                                if (data.success) {
                                        show_remind("更改成功");
                                        $.initDatatable_1();
                                        $("#myModalEditAccount").modal('hide');
                                } else {
                                        alert("更新失敗");
                                }

                        };
                        var error_back = function(data) {
                                console.log(data);
                        };
                        $.Ajax("POST", "php/json_mgm_account.php?func=fn_update_account_log", data, "", success_back, error_back);
                }

                
                function email_event( pos ){
                        
                        var bool = true;
                        pos.parent().find(".alert").hide();
                        if( pos.val() === "" ) {
                                bool = false;
                                pos.parent().find(".alert.warning").show();
                        }
                        else if( !pos[0].validity.valid ){
                                bool = false;
                                pos.parent().find(".alert.error:not(.error2)").show();
                        }
                        else {
                                pos.parent().find(".alert.success").show();
                        }
                        return bool;
                }
                function password_event( pos ){
                        
                        var bool = true;
                        pos.parent().find(".alert").hide();       
                        if( pos.val() === "" || !/^(?=.*\d)(?=.*[a-z]).{8,}$/.test( pos.val() ) ){
                                bool = false;
                                pos.parent().find(".alert.warning").show();
                        }
                        else {
                                pos.parent().find(".alert.success").show();
                        }
                        return bool;
                }
                function check_password_event( pos ){
                        
                        var bool = true;
                        pos.parent().find(".alert").hide();   
                        if( pos.val() === "" ) {
                            bool = false;
                            pos.parent().find(".alert.error").show();
                        }
                        else if( pos.val() !== $( "#account_pwd" ).val() ) {
                            bool = false;
                            pos.parent().find(".alert.error").show();
                        }
                        else {
                            pos.parent().find(".alert.success").show();
                        }
                        return bool;
                }
                function input_event( pos ){
                        
                        var bool = true;
                        pos.parent().find(".alert").hide();
                        if( pos.val() === "" ) {
                                bool = false;
                                pos.parent().find(".alert.warning").show();
                        }
                        else {
                                pos.parent().find(".alert.success").show();
                        }
                        return bool;
                }
                function select_event( pos ){
                        
                        var bool = true;
                        pos.parent().find(".alert").hide();
                        if( pos.val() === null || pos.val() === "" ) {
                            bool = false;
                            pos.parent().find(".alert.warning").show();
                        }
                        else{
                            pos.parent().find(".alert.success").show();
                        }
                        return bool;
                }
                
                function clear_event( pos ){
                        pos.parent().find(".alert").hide();
                }

        </script>
</head>

<body>
        <div id="all">

                <?php include( "html/loading.php"); ?>
                <?php include( "html/header.php"); ?>

                <div class="content">
                        <?php include( "html/sidebar_setting.php"); ?>

                        <div class="main-content container">                        
                                <div class="path">
                                        <a href="#">Console</a> > <a href="#">Account</a>
                                </div>

                                <div class="list">

                                        <h2>Account</h2>

                                        <!-- search box -->
                                        <div class="dataTable_search_box">
                                                <div id="condition_Datatable">
                                                        <table>
                                                                <tr class="condition_4" data-column="2">
                                                                        <td align="center">
                                                                                ID:<input type="text" id="email" class="column_filter hasDatepicker">
                                                                        </td>
                                                                        <td align="center">
                                                                                UserName:<input type="text" id="nickname" class="column_filter hasDatepicker">
                                                                        </td>
                                                                        <td class="center">Status:
                                                                                <select style=" width: 50%;" id="admin">
                                                                                        <option value="">All</option>
                                                                                        <option value="true">Admin</option>
                                                                                        <option value="false">Pause</option>
                                                                                </select>
                                                                        </td>
                                                                </tr>
                                                        </table>
                                                </div>

                                                <button id="btn_Datatable" class="submit">Search</button>
                                        </div>
                                        <!-- function btn box -->
<!--                                    <div class="dataTable_function_btn_box">
                                                <div class="float-left">操作結果訊息顯示</div>
                                                <button id="btn_checkbox_block_new_account" onclick="$('#add_account_modal').modal('show');" class="float-right" type="button">+新增會員</button>
                                        </div>-->

                                        <div id="datatable"></div>
                                </div>
                        </div>

                </div>

                <?php include( "html/footer.php"); ?>
            
        </div>

        <!--add-->
        <div class="modal fade" id="add_account_modal" aria-hidden="false">
                <div class="modal-dialog">
                        <div id="dialog-msg">
                                <dl>
                                        <div class="list">
                                                <h2>新增帳號</h2>
                                                <form role="form" id="container">
                                                        <ul>
                                                                <li>
                                                                        <span>信箱</span>
                                                                        <input type="email" onfocus="clear_event($(this))" onblur="email_event($(this))" class="necessary" placeholder="帳號或 E-mail" id="account_email">
                                                                        <div style="display:none;" class="alert success"></div>
                                                                        <div style="display:none;" class="alert warning">必填欄位</div>
                                                                        <div style="display:none;" class="alert error">請輸入有效的電子郵箱</div>
                                                                        <div style="display:none;" class="alert error error2">電子郵件重複</div>
                                                                </li>
                                                                <li>
                                                                        <span>輸入密碼</span>
                                                                        <input type="password" oninput="$( '#account_pwd2' ).val('');" onfocus="clear_event($(this))" onblur="password_event($(this))" class="necessary" id="account_pwd">
                                                                        <div style="display:none;" class="alert success"></div>
                                                                        <div style="display:none;" class="alert warning">至少八個字元組以上，需要包括小寫字母和數字。</div>
                                                                </li>
                                                                <li>
                                                                        <span>確認密碼</span>
                                                                        <input type="password" onfocus="clear_event($(this))" onblur="check_password_event($(this))" id="account_pwd2">
                                                                        <div style="display:none;" class="alert success"></div>
                                                                        <div style="display:none;" class="alert error">密碼不一致</div>
                                                                </li>
                                                                <li>
                                                                        <span>公開暱稱</span>
                                                                        <input type="text" onfocus="clear_event($(this))" onblur="input_event($(this))" class="necessary" id="account_nickname">
                                                                        <div style="display:none;" class="alert success"></div>
                                                                        <div style="display:none;" class="alert warning">必填欄位</div>
                                                                </li>
                                                                <li class="check">
                                                                        <input type="checkbox" id="account_admin">是否是管理員
                                                                </li>
                                                                <li class="center">
                                                                        <a class="button" href="#" data-dismiss="modal" id="add_account_modal_Yes">確定</a>
                                                                        <a class="button delete" href="#" data-dismiss="modal">取消</a>
                                                                </li>
                                                        </ul>
                                                </form>
                                        </div>
                                </dl>
                        </div>
                </div>
        </div>

        <!--修改會員資料 dialog-->
        <div aria-hidden="false" id="myModalEditAccount" class="modal fade" style="display:none;">
                <div class="modal-dialog">
                        <div id="dialog-msg">
                                <dl>
                                        <div class="list">
                                                <h2>Edit</h2>
                                                <nav class="set">
                                                        <form>
                                                                <ul>
                                                                        <li>
                                                                                <span>Admin </span>
                                                                                <select id="Edit_a_admin">
                                                                                    <option value="true">Yes</option>
                                                                                    <option value="false">No</option>
                                                                                </select>
                                                                        </li>
                                                                        <hr>
                                                                        <li>
                                                                                <span>ReferenceId</span>
                                                                                <input type="text" class="necessary long" id="Edit_a_email" placeholder="信箱" value="">
                                                                        </li>
                                                                        <li>
                                                                                <span>UserName</span>
                                                                                <input type="text" class="necessary long" id="Edit_a_nickname" placeholder="暱稱" value="">
                                                                        </li>
                                                                        <li>
                                                                                <span>Password</span>
                                                                                <input type="text" class="necessary long" id="Edit_a_nickname" placeholder="暱稱" value="">
                                                                        </li>
                                                                        <li>
                                                                                <span>CreateDate Time</span>
                                                                                <input type="text" class="necessary long" id="Edit_a_last_login_time" placeholder="上次登入時間" value="" readonly="readonly">
                                                                        </li>
                                                                        <li>
                                                                                <span>UpdateDate Time</span>
                                                                                <input type="text" class="necessary long" id="Edit_a_registration_time" placeholder="註冊時間" value="" readonly="readonly">
                                                                        </li>
                                                                        <li>
                                                                                <span>SystemLocation</span>
                                                                                <input type="text" class="necessary long" id="Edit_a_registration_time" placeholder="註冊時間" value="" readonly="readonly">
                                                                        </li>
                                                                        <li>
                                                                                <span>Remarks</span>
                                                                                <input type="text" class="necessary long" id="Edit_a_registration_time" placeholder="註冊時間" value="" readonly="readonly">
                                                                        </li>
                                                                        <li>
                                                                                <span>CreateBy</span>
                                                                                <input type="text" class="necessary long" id="Edit_a_registration_time" placeholder="註冊時間" value="" readonly="readonly">
                                                                        </li>
                                                                        <li>
                                                                                <span>CreateBy ID</span>
                                                                                <input type="text" class="necessary long" id="Edit_a_registration_time" placeholder="註冊時間" value="" readonly="readonly">
                                                                        </li>
                                                                        <li>
                                                                                <span>UpdateBy</span>
                                                                                <input type="text" class="necessary long" id="Edit_a_registration_time" placeholder="註冊時間" value="" readonly="readonly">
                                                                        </li>
                                                                        <li>
                                                                                <span>UpdateBy ID</span>
                                                                                <input type="text" class="necessary long" id="Edit_a_registration_time" placeholder="註冊時間" value="" readonly="readonly">
                                                                        </li>
                                                                </ul>
                                                        </form>
                                                </nav>

                                                <form method="get" action=""  class="center">
                                                        <a id="myModalEditAccount_Yes" href="#" class="button">Yes</a>
                                                        <a data-dismiss="modal" href="#" class="button delet">Cancel</a>
                                                </form>
                                        </div>
                                </dl>
                        </div>
                </div>
        </div>

        <!--delete-->
        <div class="modal fade" id="myModaldelete" aria-hidden="false">
                <div class="modal-dialog">
                        <div id="dialog-msg">
                                <dl>
                                        <div class="list">
                                                <h2>Delete</h2>
                                                <p class="words"><strong>Your will not be able to recover this account!</strong></p>
                                                <form method="get">
                                                        <nav>
                                                                <a data-dismiss="modal" id="myModaldelete_yes" class="button" type="button">Yes</a>
                                                                <a data-dismiss="modal" class="button delete" type="button">Cancel</a>
                                                        </nav>
                                                </form>
                                        </div>
                                </dl>
                        </div>
                </div>
        </div>
        
        <script>
            
        function unconnected_callback() {
                loading_ajax_hide();
                show_remind( "請先登入" , "error"  );
        };
        function connected_callback( member ) {
                if( member.a_admin !== "true" ){
                        loading_ajax_hide();
                        show_remind( "不是管理者，三秒後轉跳到首頁。" , "error" );
                        setTimeout( function(){ location.href = "../index.php" }, 3000);
                }
                else{
                        init();
                }
        };
        
        </script>
</body>
</html>
